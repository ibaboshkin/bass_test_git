﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using NLog;
using TagUtilities.TagsLibrary;

namespace TagUtilities.TagsLibraryWrapper.v._2._0.Tests
{
    [TestClass]
    public class ID3v1TagTests
    {
        private static Logger _logger = LogManager.GetLogger("TagUtilities.ID3v1TagTests");

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            TagsUtilities.Initialize();
        }

        [TestMethod]
        public void CreateTest()
        {
            _logger.Trace("============CreateTest Start=============");
            _logger.Trace("Create an instance of ID3v1Tag");
            ID3v1Tag tag = new ID3v1Tag();

            _logger.Trace("Expected TypeString: {0}, Actual: {1}", tag.TypeString, "TagsLibrary.ID3v1Tag");
            Assert.AreEqual(tag.TypeString, "TagsLibrary.ID3v1Tag");
            _logger.Trace("Dispose");
            tag.Dispose();
        }

        [TestMethod]
        public void LoadFromFileTest()
        {
            _logger.Trace("============LoadFromFileTest Start=============");
            _logger.Trace("Create an instance of ID3v1Tag");
            ID3v1Tag tag = new ID3v1Tag();
            string fileName = Path.Combine(Environment.CurrentDirectory, Constants.MP3_TEST_FILE);
            _logger.Trace("Load tags from \"{0}\"", fileName);
            tag.LoadFromFile(fileName);
            _logger.Trace("Expected TypeString: {0}, Actual: {1}", tag.TypeString, "TagsLibrary.ID3v1Tag");
            Assert.AreEqual(tag.TypeString, "TagsLibrary.ID3v1Tag");
            _logger.Trace("Loaded: {0}", tag.ToString());
            _logger.Trace("Dispose");
            tag.Dispose();
        }

        [TestMethod]
        public void TypeConvertionTest()
        {
            _logger.Trace("============LoadFromFileTest Start=============");
            _logger.Trace("Create an instance of ID3v1Tag");
            ID3v1Tag tag = new ID3v1Tag();
            string fileName = Path.Combine(Environment.CurrentDirectory, Constants.MP3_TEST_FILE);
            _logger.Trace("Load tags from \"{0}\"", fileName);
            tag.LoadFromFile(fileName);
            _logger.Trace("Expected TypeString: {0}, Actual: {1}", tag.TypeString, "TagsLibrary.ID3v1Tag");
            Assert.AreEqual(tag.TypeString, "TagsLibrary.ID3v1Tag");
            _logger.Trace("Loaded: {0}", tag.ToString());
            _logger.Trace("Get First Tag");
            var first = tag.TagsList.First();
            var parent = first.Parent;
            Assert.IsTrue(parent == tag);
            _logger.Trace("Dispose");
            tag.Dispose();
        }

        [TestMethod]
        public void AssignTest()
        {
            _logger.Trace("============LoadFromFileTest Start=============");
            _logger.Trace("Create an instance of ID3v1Tag");
            ID3v1Tag tag = new ID3v1Tag();
            string fileName = Path.Combine(Environment.CurrentDirectory, Constants.MP3_TEST_FILE);
            _logger.Trace("Load tags from \"{0}\"", fileName);
            tag.LoadFromFile(fileName);
            _logger.Trace("Expected TypeString: {0}, Actual: {1}", tag.TypeString, "TagsLibrary.ID3v1Tag");
            Assert.AreEqual(tag.TypeString, "TagsLibrary.ID3v1Tag");
            _logger.Trace("Loaded: {0}", tag.ToString());
            _logger.Trace("Create anew instance of ID3v1Tag");
            ID3v1Tag tag2 = new ID3v1Tag();
            _logger.Trace("Assign tag to tag2");
            tag2.Assign(tag);
            _logger.Trace("Assigned: {0}", tag2.ToString());
            Assert.AreEqual(tag.ToString(), tag2.ToString());
            _logger.Trace("Dispose");
            tag.Dispose();
        }
    }
}
