﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Un4seen.Bass;
using System.IO;
using TagUtilities.TagsLibrary;

namespace TagUtilities.TagsLibraryWrapper.Tests
{
    [TestClass]
    public class TagsLibraryWrapper_Tests
    {
        private int[] _plugins;

        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
        }

        [TestInitialize]
        public void TestInitialize()
        {
            Assert.IsTrue(Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero));
            LoadPlugins();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            FreePlugins();
            Assert.IsTrue(Bass.BASS_Free());
        }


        [TestMethod]
        public void TestLoadFromFileLoadFromBassAreEqual()
        {
            string[] testFiles = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "TestFiles"));

            Assert.IsTrue(testFiles != null && testFiles.Length > 0);

            foreach (var file in testFiles)
            {
                if (Path.GetExtension(file) == ".ape")
                {
                    continue;
                }

                using (Tags tagsFromFile = new Tags())
                {
                    tagsFromFile.LoadFromFile(file);

                    using (Tags tagsFromBass = new Tags())
                    {
                        tagsFromBass.LoadFromBASS(file);

                        CheckAreTagsEqual(tagsFromFile, tagsFromBass);
                    }
                }

            }
        }

        private void LoadPlugins()
        {
            string[] plugins = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Plugins"));
            _plugins = new int[plugins.Length];
            int i = 0;

            foreach (var plugin in plugins)
            {
                _plugins[i++] = Bass.BASS_PluginLoad(plugin);
            }
        }

        private void FreePlugins()
        {
            foreach (var plugin in _plugins)
            {
                Bass.BASS_PluginFree(plugin);
            }
        }

        private void CheckAreTagsEqual(Tags first, Tags second)
        {
            Assert.AreEqual(first.TagsList.Length, second.TagsList.Length);

            for (int i = 0; i < first.TagsList.Length; i++)
            {
                Assert.IsTrue(string.Compare(first.TagsList[i].Value, second[first.TagsList[i].Name]) == 0);
            }
        }
    }
}
