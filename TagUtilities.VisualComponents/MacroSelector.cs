﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TagUtilities.VisualComponents.Models;
using BrightIdeasSoftware;

namespace TagUtilities.VisualComponents
{
    public partial class MacroSelector : UserControl
    {
        #region Constants
        public const int DESCRIPTION_COLUMN_DEFAULT_WIDTH = 250;
        public const int MACRO_COLUMN_DEFAULT_WIDTH = 100;
        #endregion

        #region Fields
        private bool _showToolbar = true;
        private int _descriptionColumnWidth;
        private int _macroColumnWidth;
        #endregion

        #region Events
        public event EventHandler<InsertMacroEventArgs> InsertMacro;
        #endregion

        #region Properties
        [Browsable(true), DefaultValue(true)]
        public bool ShowToolbar
        {
            get
            {
                return _showToolbar;
            }
            set
            {
                if (_showToolbar != value)
                {
                    _showToolbar = value;
                    panel1.Visible = value;
                }
            }
        }

        [Browsable(true), DefaultValue(DESCRIPTION_COLUMN_DEFAULT_WIDTH)]
        public int DescriptionColumnWidth
        {
            get
            {
                return _descriptionColumnWidth;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Value should be > 0", "DescriptionColumnWidth");
                }

                if (_descriptionColumnWidth != value)
                {
                    _descriptionColumnWidth = value;
                    descriptionColumn.Width = value;
                }
            }
        }

        [Browsable(true), DefaultValue(MACRO_COLUMN_DEFAULT_WIDTH)]
        public int MacroColumnWidth
        {
            get
            {
                return _macroColumnWidth;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Value should be > 0", "MacroColumnWidth");
                }

                if (_macroColumnWidth != value)
                {
                    _macroColumnWidth = value;
                    valueColumn.Width = value;
                }
            }
        }
        #endregion

        #region Constructors
        public MacroSelector()
        {
            InitializeComponent();
            objectListView1.AlwaysGroupByColumn = categoryColumn;
            DescriptionColumnWidth = DESCRIPTION_COLUMN_DEFAULT_WIDTH;
            MacroColumnWidth = MACRO_COLUMN_DEFAULT_WIDTH;
            SetLocalization();
        }
        #endregion

        #region Methods
        private void Filter(string txt, int matchKind)
        {
            TextMatchFilter filter = null;
            if (!String.IsNullOrEmpty(txt))
            {
                switch (matchKind)
                {
                    case 0:
                    default:
                        filter = TextMatchFilter.Contains(objectListView1, txt);
                        break;
                    case 1:
                        filter = TextMatchFilter.Prefix(objectListView1, txt);
                        break;
                    case 2:
                        filter = TextMatchFilter.Regex(objectListView1, txt);
                        break;
                }
            }
            // Setup a default renderer to draw the filter matches
            if (filter == null)
                objectListView1.DefaultRenderer = null;
            else
            {
                objectListView1.DefaultRenderer = new HighlightTextRenderer(filter);

                // Uncomment this line to see how the GDI+ rendering looks
                //objectListView1.DefaultRenderer = new HighlightTextRenderer { Filter = filter, UseGdiTextRendering = false };
            }

            // Some lists have renderers already installed
            HighlightTextRenderer highlightingRenderer = objectListView1.GetColumn(0).Renderer as HighlightTextRenderer;
            if (highlightingRenderer != null)
                highlightingRenderer.Filter = filter;

            objectListView1.AdditionalFilter = filter;
        }

        /// <summary>
        /// Fills the table with given macro list
        /// </summary>
        /// <param name="source">Macro list. Tuple.Item1 - macro name
        /// Tuple.Item2 - description
        /// Tuple.Item3 - Category
        /// </param>
        public void Fill(IEnumerable<Tuple<string, string, string>> source)
        {
            List<DescriptedValue> list = new List<DescriptedValue>(source.Count());

            foreach (var item in source)
            {
                DescriptedValue descriptedValue = new DescriptedValue(item.Item1, item.Item3, item.Item2);
                list.Add(descriptedValue);
            }

            objectListView1.SetObjects(list);
        }

        /// <summary>
        /// Fills the table with given macro list
        /// </summary>
        /// <param name="source">Macro list</param>
        public void Fill(IEnumerable<DescriptedValue> source)
        {
            objectListView1.SetObjects(source);
        }

        private void SetLocalization()
        {
            valueColumn.Text = Resources.MACRO_COLUMN_NAME;
            descriptionColumn.Text = Resources.DESCRIPTION_COLUMN_NAME;
            button1.Text = Resources.INSERT_BUTTON_NAME;
            label1.Text = Resources.FILTER_LABEL_NAME;
        }
        #endregion

        #region Event raisers
        protected void OnInsertMacro(string macroName)
        {
            var temp = InsertMacro;

            if (!string.IsNullOrEmpty(macroName) && temp != null)
            {
                InsertMacro(this, new InsertMacroEventArgs(macroName));
            }
        }
        #endregion

        #region Event handlers
        private void objectListView1_DoubleClick(object sender, EventArgs e)
        {
            if (objectListView1.SelectedItem != null)
            {
                OnInsertMacro(objectListView1.SelectedItem.Text);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Filter(tbFilter.Text, 0);
        }
        #endregion
    }
}
