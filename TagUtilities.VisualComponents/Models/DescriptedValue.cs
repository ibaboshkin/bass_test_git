﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagUtilities.VisualComponents.Models
{
    [Serializable]
    public class DescriptedValue
    {
        private string _description;
        private string _value;
        private string _category;

        public string Description 
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }

        public DescriptedValue(string value, string category, string description = null)
        {
            Value = value;
            Description = description ?? string.Empty;
            Category = category;
        }
    }
}
