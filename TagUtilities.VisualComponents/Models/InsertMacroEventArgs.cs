﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TagUtilities.VisualComponents.Models
{
    public class InsertMacroEventArgs: EventArgs
    {
        public string Macro { get; set; }

        public InsertMacroEventArgs()
        {

        }

        public InsertMacroEventArgs(string macroName)
        {
            Macro = macroName;
        }
    }
}
