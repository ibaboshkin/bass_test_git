﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TracklistCreator
{
    public class FileInfoCollection : IList<FileInfo>
    {
        #region Fields
        private List<FileInfo> _files = new List<FileInfo>();
        private FolderInfo _parent;
        #endregion

        #region Constructors
        internal FileInfoCollection(FolderInfo parent)
        {
            _parent = parent;
        }
        #endregion

        #region IList implementation
        public int IndexOf(FileInfo item)
        {
            return _files.IndexOf(item);
        }

        public void Insert(int index, FileInfo item)
        {
            item.Parent = _parent;
            _files.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _files.RemoveAt(index);
            _parent.UpdateAlbumInfo();
        }

        public FileInfo this[int index]
        {
            get
            {
                return _files[index];
            }
            set
            {
                value.Parent = _parent;
                _files[index] = value;
            }
        }

        public void Add(FileInfo item)
        {
            item.Parent = _parent;
            _files.Add(item);
        }

        public void Clear()
        {
            _files.Clear();
            _parent.UpdateAlbumInfo();
        }

        public bool Contains(FileInfo item)
        {
            return _files.Contains(item);
        }

        public void CopyTo(FileInfo[] array, int arrayIndex)
        {
            _files.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _files.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(FileInfo item)
        {
            bool removed = _files.Remove(item);

            if (removed)
            {
                _parent.UpdateAlbumInfo();
            }

            return removed;
        }

        public IEnumerator<FileInfo> GetEnumerator()
        {
            return _files.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _files.GetEnumerator();
        }
        #endregion
    }
}
