﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using TagUtilities.TagsLibrary;
using TagUtilities.VisualComponents.Models;
using System.Threading.Tasks;
using TagUtilities;
using TagUtilities.TracklistCreator.Properties;
using System.Diagnostics;
using System.Runtime.ExceptionServices;
using Un4seen.Bass;
using NLog;

namespace TagUtilities.TracklistCreator
{
    public partial class TracklistCreatorDialog : Form
    {
        #region Constants
        private const int MACRO_SECTION_WIDTH = 400;
        private const string SEARCH_PATTERN = @"$(?<=\.(mp3|flac|ogg|ape))"; // ape
        #endregion

        #region Fields
        private List<TagInfo> _replaceInfo;
        private bool _isMacroSelectorShown = false;
        private TextBox _lastActiveTextBox = null;
        private int[] _plugins;
        private static Logger _logger = LogManager.GetLogger("TagUtilities.TracklistCreator.TracklistCreatorDialog");
        #endregion

        #region Properties
        public bool IsMacroSelectorShown
        {
            get
            {
                return _isMacroSelectorShown;
            }
            set
            {
                if (_isMacroSelectorShown != value)
                {
                    _isMacroSelectorShown = value;

                    if (value)
                    {
                        this.Width += MACRO_SECTION_WIDTH;
                        btnMacros.Text = Resources.MACROS_BUTTON_SHOWN_TITLE;
                    }
                    else
                    {
                        this.Width -= MACRO_SECTION_WIDTH;
                        btnMacros.Text = Resources.MACROS_BUTTON_HIDDEN_TITLE;
                    }
                }
            }
        }
        #endregion

        #region Constructor
        public TracklistCreatorDialog()
        {
            InitBass();
            _replaceInfo = Tags.GetTagNames();
            InitializeComponent();
            LoadSettings();
            SetLocalization();
            macroSelector1.Fill(MapTagInfo(_replaceInfo));
        }
        #endregion

        #region Methods
        private void InitBass()
        {
            bool ok = Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);

            string[] plugins = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Plugins"));
            _plugins = new int[plugins.Length];
            int i = 0;

            foreach (var plugin in plugins)
            {
                _plugins[i++] = Bass.BASS_PluginLoad(plugin);
            }
        }

        private void FreeBass()
        {
            foreach (var plugin in _plugins)
            {
                Bass.BASS_PluginFree(plugin);
            }

            Bass.BASS_Free();
        }

        private IEnumerable<DescriptedValue> MapTagInfo(IEnumerable<TagInfo> info)
        {
            List<DescriptedValue> result = new List<DescriptedValue>(info.Count());

            foreach (var item in info)
            {
                result.Add(new DescriptedValue("%" + item.ProperyName + "%", item.Category, item.Description));
            }

            result.Add(new DescriptedValue("%FolderName%", "System Info", "Folder Name"));
            //result.Add(new DescriptedValue("%FileName%", "System Info", "Folder Name"));
            return result;
        }

        private Tags GetTags(string fileName)
        {
            Tags tags = new Tags();

            int max = 10;
            int i = 0;
            bool ok = false;

            do
            {
                try
                {
                    tags.LoadFromBASS(fileName);
                    ok = true;
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(string.Format("LoadFromBASS try {0} faild. Error opening file \"{1}\".", i + 1, fileName), ex);
                } 
            } while (!ok && i++<max);


            if (!ok)
            {
                _logger.Trace("Could not open \"{0}\" with BASS", fileName);

                try
                {
                    tags.LoadFromFile(fileName);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(string.Format("An unknown exception has occured during opening file \"{0}\"", fileName), ex);
                    MessageBox.Show(string.Format("An unknown exception has occured during opening file \"{0}\"", fileName));
                }
            }

            return tags;
        }

        private int ScanFolders(string folder)
        {
            if (!Directory.Exists(folder))
            {
                throw new FileNotFoundException("Could not find directory \"" + folder + "\"");
            }

            int result = 0;

            var directories = Directory.GetDirectories(folder);

            foreach (var directory in directories)
            {
                var count = ScanFolders(directory);

                if (count > 0)
                {
                    result += count + 1;
                }
            }

            var searchPattern = new Regex(
                SEARCH_PATTERN,
                RegexOptions.IgnoreCase);

            var files = Directory.GetFiles(folder).Where(f => searchPattern.IsMatch(f));

            if (files != null)
            {
                foreach (var fileName in files)
                {
                    result++;
                }
            }

            return result;
        }

        [HandleProcessCorruptedStateExceptions]
        private FolderInfo CreateTree(string folder)
        {
            if (!Directory.Exists(folder))
            {
                throw new FileNotFoundException("Could not find directory \"" + folder + "\"");
            }

            FolderInfo result = new FolderInfo(folder);

            var directories = Directory.GetDirectories(folder);

            foreach (var directory in directories)
            {
                var subfolder = CreateTree(directory);

                if (subfolder != null)
                {
                    result.Subfolders.Add(subfolder);
                    this.Invoke(new Action(() => progressBar1.Increment(1)));
                }
            }

            var searchPattern = new Regex(
                SEARCH_PATTERN,
                RegexOptions.IgnoreCase);

            var files = Directory.GetFiles(folder).Where(f => searchPattern.IsMatch(f));

            if ((files == null || files.Count() == 0) && result.Subfolders.Count == 0)
            {
                return null;
            }

            foreach (var fileName in files)
            {
                var tags = GetTags(fileName);
                result.Files.Add(new FileInfo(fileName, tags));
                this.Invoke(new Action(() => progressBar1.Increment(1)));
            }

            return result;
        }

        private void WriteTree(FolderInfo tree, string fileName, string headerFormat, string tracksFormat, string footerFormat)
        {
            FileStream file;

            if (File.Exists(fileName))
            {
                file = new FileStream(fileName, FileMode.Truncate, FileAccess.Write);
            }
            else
            {
                file = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write);
            }

            StreamWriter sw = new StreamWriter(file);

            WriteTree(tree, sw, headerFormat, tracksFormat, footerFormat);

            sw.Flush();
            file.Close();
        }

        private void WriteTree(FolderInfo tree, StreamWriter writer, string headerFormat, string tracksFormat, string footerFormat)
        {
            if (tree == null)
            {
                return;
            }

            WriteElement(headerFormat, tree, writer);

            foreach (var folder in tree.Subfolders)
            {
                WriteTree(folder, writer, headerFormat, tracksFormat, footerFormat);
            }

            foreach (var file in tree.Files)
            {
                WriteElement(tracksFormat, file, writer);
                this.Invoke(new Action(() => progressBar1.Increment(1)));
            }

            WriteElement(footerFormat, tree, writer);
            this.Invoke(new Action(() => progressBar1.Increment(1)));
        }

        private void WriteElement(string format, object element, StreamWriter writer)
        {
            if (string.IsNullOrEmpty(format))
            {
                return;
            }

            var parameters = format.ParseParameters();

            if (parameters == null || parameters.Count() == 0)
            {
                writer.WriteLine(format);
                return;
            }

            StringBuilder sb = new StringBuilder(format);
            Tags tags = null;

            if (element is FolderInfo)
            {
                tags = (element as FolderInfo).AlbumInfo;
                sb.Replace("%FolderName%", Path.GetFileName((element as FolderInfo).Path));
            }

            if (element is FileInfo)
            {
                tags = (element as FileInfo).Tags;
                //sb.Replace("%FileName%", Path.GetFileName((element as FileInfo).Path));
            }

            if (tags == null)
            {
                return;
            }

            foreach (var parameter in parameters)
            {
                string key = parameter.Substring(1, parameter.Length - 2);

                TagInfo info = _replaceInfo.Where(i => i.ProperyName == key || i.SystemName == key || i.FriendlyName == key).FirstOrDefault();

                if (info == null)
                {
                    continue;
                }

                if (!string.IsNullOrEmpty(info.SystemName))
                {
                    sb.ReplaceParameter(parameter, tags[info.SystemName]);
                }
                else
                {
                    var property = typeof(Tags).GetProperty(info.ProperyName, BindingFlags.Instance | BindingFlags.Public);

                    if (property != null)
                    {
                        sb.ReplaceParameter(parameter, property.GetValue(tags, null).ToString());
                    }
                    else
                    {
                        sb.ReplaceParameter(parameter, string.Empty);
                    }
                }
            }

            sb.Replace(@"\%", "");
            //foreach (var item in _replaceInfo)
            //{
            //    string key = "%" + item.ProperyName + "%";

            //    if (format.Contains(key))
            //    {
            //        if (!string.IsNullOrEmpty(item.SystemName))
            //        {
            //            sb.Replace(key, tags[item.SystemName]); 
            //        }
            //        else
            //        {
            //            var property = typeof(Tags).GetProperty(item.ProperyName, BindingFlags.Instance | BindingFlags.Public);

            //            if (property != null)
            //            {
            //                sb.Replace(key, (string)property.GetValue(tags, null));
            //            }
            //            else
            //            {
            //                sb.Replace(key, string.Empty);
            //            } 
            //        }
            //    }
            //}

            writer.WriteLine(sb.ToString());
        }

        private void CreateTracklist(string path, string fileName, string headerFormat, string tracksFormat, string footerFormat)
        {
            lock (this)
            {
                progressBar1.Value = 0;
                progressBar1.Style = ProgressBarStyle.Marquee;
                btnStart.Enabled = false;

                Task.Factory.StartNew(() =>
                    {
                        int count = ScanFolders(path);
                        this.Invoke(new Action(() =>
                                {
                                    progressBar1.Style = ProgressBarStyle.Continuous;
                                    progressBar1.Maximum = count;
                                    progressBar1.Value = 0;
                                }
                            )
                        );

                        FolderInfo tree = CreateTree(path);
                        this.Invoke(new Action(() =>
                                {
                                    progressBar1.Value = 0;
                                    progressBar1.Maximum = tree.GetItemsCount(true);
                                }
                            )
                        );

                        WriteTree(tree, fileName, tbHeader.Text, tbTracklist.Text, tbFooter.Text);

                        this.Invoke(new Action(() => btnStart.Enabled = true));
                    }
                );
            }
        }

        private void SaveSettings()
        {
            if (!string.IsNullOrEmpty(tbDirPath.Text))
            {
                Settings.Default["LastFolderPath"] = tbDirPath.Text;
            }

            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                Settings.Default["LastFilePath"] = Path.GetDirectoryName(saveFileDialog1.FileName);
            }

            Settings.Default.Save();
        }

        private void LoadSettings()
        {
            if (Settings.Default["LastFolderPath"] != null)
            {
                string value = Settings.Default["LastFolderPath"].ToString();

                if (!string.IsNullOrEmpty(value))
                {
                    folderBrowserDialog1.SelectedPath = value;
                    tbDirPath.Text = value;
                }
            }

            if (Settings.Default["LastFilePath"] != null)
            {
                string value = Settings.Default["LastFilePath"].ToString();

                if (!string.IsNullOrEmpty(value))
                {
                    saveFileDialog1.InitialDirectory = value;
                }
            }
        }

        private void SetLocalization()
        {
            this.Text = Resources.FORM_TITLE;
            label4.Text = Resources.FOLDER_LABEL_TITLE;
            btnStart.Text = Resources.START_BUTTON_TITLE;
            btnMacros.Text = Resources.MACROS_BUTTON_HIDDEN_TITLE;
            gbFormat.Text = Resources.FORMAT_GROUPBOX_TITLE;
            label1.Text = Resources.HEADER_FORMAT_TITLE;
            label2.Text = Resources.TRACKLIST_FORMAT_TITLE;
            label3.Text = Resources.FOOTER_FORMAT_TITLE;
        }
        #endregion

        #region Event Handlers
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbDirPath.Text) && Directory.Exists(tbDirPath.Text))
            {
                folderBrowserDialog1.SelectedPath = tbDirPath.Text;
            }

            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbDirPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnMacros_Click(object sender, EventArgs e)
        {
            IsMacroSelectorShown = !IsMacroSelectorShown;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbDirPath.Text) && saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                CreateTracklist(tbDirPath.Text, saveFileDialog1.FileName, tbHeader.Text, tbTracklist.Text, tbFooter.Text);
            }
        }

        private void macroSelector1_InsertMacro(object sender, InsertMacroEventArgs e)
        {
            var selectionIndex = _lastActiveTextBox.SelectionStart;
            _lastActiveTextBox.Text = _lastActiveTextBox.Text.Insert(selectionIndex, e.Macro);
            _lastActiveTextBox.SelectionStart = selectionIndex + e.Macro.Length;
        }

        private void tbHeader_Enter(object sender, EventArgs e)
        {
            if (sender is TextBox)
            {
                _lastActiveTextBox = sender as TextBox;
            }
        }

        private void TracklistCreatorDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveSettings();
            FreeBass();
        }
        #endregion
    }
}

