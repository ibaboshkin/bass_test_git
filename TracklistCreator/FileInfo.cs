﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TagUtilities.TagsLibrary;

namespace TagUtilities.TracklistCreator
{
    public class FileInfo
    {
        private readonly Tags _tags;
        private FolderInfo _parent;

        public Tags Tags { get { return _tags; } }
        public string Path { get; private set; }

        public FolderInfo Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;

                if (_parent != null)
                {
                    _parent.UpdateAlbumInfo(Tags);
                }
            }
        }

        public FileInfo(string path, Tags tags)
        {
            Path = path;
            _tags = tags;
        }

        public FileInfo(string path, Tags tags, FolderInfo parent)
        {
            Path = path;
            _tags = tags;
            Parent = parent;
        }
    }
}
