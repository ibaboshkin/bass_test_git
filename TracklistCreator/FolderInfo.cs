﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TagUtilities.TagsLibrary;

namespace TagUtilities.TracklistCreator
{
    public class FolderInfo
    {
        private FolderInfo _parent;

        #region Properties
        public FolderInfo Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;

                if (_parent != null)
                {
                    _parent.UpdateAlbumInfo(AlbumInfo);
                }
            }
        }

        public FolderInfoCollection Subfolders { get; private set; }
        public FileInfoCollection Files { get; private set; }
        public Tags AlbumInfo { get; private set; }
        public string Path { get; private set; } 
        #endregion

        #region Constructors
        public FolderInfo(string path)
        {
            Path = path;
            Subfolders = new FolderInfoCollection(this);
            Files = new FileInfoCollection(this);
            AlbumInfo = new Tags();
        }

        public FolderInfo(string path, FolderInfo parent)
            : this(path)
        {
            Parent = parent;
        } 
        #endregion

        #region Methods
        public void UpdateAlbumInfo()
        {
            AlbumInfo.DeleteAllTags();
            //AlbumInfo = new Tags();

            foreach (var subfolder in Subfolders)
            {
                UpdateAlbumInfo(subfolder.AlbumInfo, false);
            }

            foreach (var file in Files)
            {
                UpdateAlbumInfo(file.Tags, false);
            }

            if (Parent != null)
            {
                Parent.UpdateAlbumInfo();
            }
        }

        public void UpdateAlbumInfo(Tags tags)
        {
            UpdateAlbumInfo(tags, true);
        }

        internal void UpdateAlbumInfo(Tags tags, bool updateParent)
        {
            //return;
            if (tags == null || tags.TagsList == null || tags.TagsList.Count() == 0)
            {
                return;
            }

            foreach (var tag in tags.TagsList)
            {
                if (string.IsNullOrEmpty(tag.Value))
                {
                    continue;
                }

                if (string.IsNullOrEmpty(AlbumInfo[tag.Name]))
                {
                    AlbumInfo[tag.Name] = tag.Value;
                }
                else
                {
                    if (AlbumInfo[tag.Name] != "Various")
                    {
                        if (string.Compare(AlbumInfo[tag.Name], tag.Value, true) != 0)
                        {
                            if (AlbumInfo[tag.Name].Contains(tag.Value))
                            {
                                AlbumInfo[tag.Name] = tag.Value;
                            }
                            else
                                if (!tag.Value.Contains(AlbumInfo[tag.Name]))
                                {
                                    AlbumInfo[tag.Name] = "Various";
                                }
                        }
                    }
                }
            }

            if (updateParent && Parent != null)
            {
                Parent.UpdateAlbumInfo();
            }
        } 

        public int GetItemsCount(bool searchSubfolders)
        {
            int count = Subfolders.Count + Files.Count;

            if (searchSubfolders)
            {
                foreach (var folder in Subfolders)
                {
                    count += folder.GetItemsCount(true);
                }
            }

            return count;
        }
        #endregion
    }
}
