﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TracklistCreator
{
    public class FolderInfoCollection : IList<FolderInfo>
    {
        #region Fields
        private List<FolderInfo> _folders = new List<FolderInfo>();
        private FolderInfo _parent;
        #endregion

        #region Constructors
        internal FolderInfoCollection(FolderInfo parent)
        {
            _parent = parent;
        }
        #endregion

        #region IList implementation
        public int IndexOf(FolderInfo item)
        {
            return _folders.IndexOf(item);
        }

        public void Insert(int index, FolderInfo item)
        {
            item.Parent = _parent;
            _folders.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _folders.RemoveAt(index);
            _parent.UpdateAlbumInfo();
        }

        public FolderInfo this[int index]
        {
            get
            {
                return _folders[index];
            }
            set
            {
                value.Parent = _parent;
                _folders[index] = value;
            }
        }

        public void Add(FolderInfo item)
        {
            item.Parent = _parent;
            _folders.Add(item);
        }

        public void Clear()
        {
            _folders.Clear();
            _parent.UpdateAlbumInfo();
        }

        public bool Contains(FolderInfo item)
        {
            return _folders.Contains(item);
        }

        public void CopyTo(FolderInfo[] array, int arrayIndex)
        {
            _folders.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _folders.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(FolderInfo item)
        {
            bool removed = _folders.Remove(item);

            if (removed)
            {
                _parent.UpdateAlbumInfo();
            }

            return removed;
        }

        public IEnumerator<FolderInfo> GetEnumerator()
        {
            return _folders.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _folders.GetEnumerator();
        } 
        #endregion
    }
}
