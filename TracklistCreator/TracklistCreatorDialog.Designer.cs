﻿namespace TagUtilities.TracklistCreator
{
    partial class TracklistCreatorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBrowse = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tbDirPath = new System.Windows.Forms.TextBox();
            this.gbFormat = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbFooter = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbTracklist = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbHeader = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnMacros = new System.Windows.Forms.Button();
            this.macroSelector1 = new TagUtilities.VisualComponents.MacroSelector();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.gbFormat.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(368, 9);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(26, 23);
            this.btnBrowse.TabIndex = 0;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tbDirPath
            // 
            this.tbDirPath.Location = new System.Drawing.Point(52, 12);
            this.tbDirPath.Name = "tbDirPath";
            this.tbDirPath.Size = new System.Drawing.Size(310, 20);
            this.tbDirPath.TabIndex = 1;
            // 
            // gbFormat
            // 
            this.gbFormat.Controls.Add(this.label3);
            this.gbFormat.Controls.Add(this.tbFooter);
            this.gbFormat.Controls.Add(this.label2);
            this.gbFormat.Controls.Add(this.tbTracklist);
            this.gbFormat.Controls.Add(this.label1);
            this.gbFormat.Controls.Add(this.tbHeader);
            this.gbFormat.Location = new System.Drawing.Point(13, 67);
            this.gbFormat.Name = "gbFormat";
            this.gbFormat.Size = new System.Drawing.Size(381, 254);
            this.gbFormat.TabIndex = 5;
            this.gbFormat.TabStop = false;
            this.gbFormat.Text = "Format";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Footer";
            // 
            // tbFooter
            // 
            this.tbFooter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFooter.Location = new System.Drawing.Point(11, 190);
            this.tbFooter.Multiline = true;
            this.tbFooter.Name = "tbFooter";
            this.tbFooter.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbFooter.Size = new System.Drawing.Size(360, 51);
            this.tbFooter.TabIndex = 9;
            this.tbFooter.Text = "[/spoiler]";
            this.tbFooter.Enter += new System.EventHandler(this.tbHeader_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Tracklist";
            // 
            // tbTracklist
            // 
            this.tbTracklist.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTracklist.Location = new System.Drawing.Point(11, 114);
            this.tbTracklist.Multiline = true;
            this.tbTracklist.Name = "tbTracklist";
            this.tbTracklist.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbTracklist.Size = new System.Drawing.Size(360, 51);
            this.tbTracklist.TabIndex = 7;
            this.tbTracklist.Text = "[b]%TrackNumber%. %Title%[/b] [size=10][i]- %Artist%[/i][/size]";
            this.tbTracklist.Enter += new System.EventHandler(this.tbHeader_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Header";
            // 
            // tbHeader
            // 
            this.tbHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbHeader.Location = new System.Drawing.Point(11, 38);
            this.tbHeader.Multiline = true;
            this.tbHeader.Name = "tbHeader";
            this.tbHeader.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbHeader.Size = new System.Drawing.Size(360, 51);
            this.tbHeader.TabIndex = 5;
            this.tbHeader.Text = "[spoiler=\"%FolderName%\"]";
            this.tbHeader.Enter += new System.EventHandler(this.tbHeader_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Folder";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(238, 38);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = global::TagUtilities.TracklistCreator.Resources.START_BUTTON_TITLE;
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnMacros
            // 
            this.btnMacros.Location = new System.Drawing.Point(319, 38);
            this.btnMacros.Name = "btnMacros";
            this.btnMacros.Size = new System.Drawing.Size(75, 23);
            this.btnMacros.TabIndex = 7;
            this.btnMacros.Text = global::TagUtilities.TracklistCreator.Resources.MACROS_BUTTON_HIDDEN_TITLE;
            this.btnMacros.UseVisualStyleBackColor = true;
            this.btnMacros.Click += new System.EventHandler(this.btnMacros_Click);
            // 
            // macroSelector1
            // 
            this.macroSelector1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.macroSelector1.DescriptionColumnWidth = 210;
            this.macroSelector1.Location = new System.Drawing.Point(415, 9);
            this.macroSelector1.MacroColumnWidth = 150;
            this.macroSelector1.Name = "macroSelector1";
            this.macroSelector1.Size = new System.Drawing.Size(380, 312);
            this.macroSelector1.TabIndex = 9;
            this.macroSelector1.InsertMacro += new System.EventHandler<TagUtilities.VisualComponents.Models.InsertMacroEventArgs>(this.macroSelector1_InsertMacro);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(52, 38);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(180, 23);
            this.progressBar1.TabIndex = 10;
            // 
            // TracklistCreatorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 329);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.macroSelector1);
            this.Controls.Add(this.btnMacros);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbFormat);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tbDirPath);
            this.Controls.Add(this.btnBrowse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TracklistCreatorDialog";
            this.Text = "Tracklist Creator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TracklistCreatorDialog_FormClosed);
            this.gbFormat.ResumeLayout(false);
            this.gbFormat.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox tbDirPath;
        private System.Windows.Forms.GroupBox gbFormat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbFooter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbTracklist;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbHeader;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnMacros;
        private VisualComponents.MacroSelector macroSelector1;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

