unit TagsLibExports;

interface

Uses SysUtils,
  Classes,
  TagsLibrary;

type
  TTagArray = Array of TTag;
  PTagArray = ^TTagArray;
  TCoverArtArray = Array of TCoverArt;
  PCoverArt = ^TCoverArtArray;

{$REGION 'Tag Declarations'}
function Tag_get_Name(Tag: TTag): PChar; export; stdcall;
function Tag_get_Value(Tag: TTag): PChar; export; stdcall;
function Tag_get_Language(Tag: TTag): PChar; export; stdcall;
function Tag_get_Description(Tag: TTag): PChar; export; stdcall;
function Tag_get_ExtTagType(Tag: TTag): TExtTagType; export; stdcall;
function Tag_get_Parent(Tag: TTag): TTags; export; stdcall;
function Tag_get_Index(Tag: TTag): Integer; export; stdcall;

procedure Tag_set_Name(Tag: TTag; Name: PChar); export; stdcall;
procedure Tag_set_Value(Tag: TTag; Value: PChar); export; stdcall;
procedure Tag_set_Language(Tag: TTag; Language: PChar); export; stdcall;
procedure Tag_set_Description(Tag: TTag; Description: PChar); export; stdcall;
procedure Tag_set_ExtTagType(Tag: TTag; ExtTagType: TExtTagType);
  export; stdcall;
procedure Tag_set_Parent(Tag: TTag; Parent: TTags); export; stdcall;
procedure Tag_set_Index(Tag: TTag; Index: Integer); export; stdcall;

function Tag_Create(Parent: TTags): TTag; export; stdcall;
procedure Tag_Destroy(Tag: TTag); export; stdcall;
procedure Tag_Clear(Tag: TTag); export; stdcall;
function Tag_Delete(Tag: TTag): Boolean; export; stdcall;
procedure Tag_Remove(Tag: TTag); export; stdcall;
function Tag_Assign(this: TTag; Tag: TTag): Boolean; export; stdcall;
{$ENDREGION}
{$REGION 'CoverArt Declarations'}
function CoverArt_get_Name(CoverArt: TCoverArt): PChar; export; stdcall;
procedure CoverArt_get_Stream(CoverArt: TCoverArt; var buffer: PByte;
  var size: Int64); export; stdcall;
function CoverArt_get_Description(CoverArt: TCoverArt): PChar; export; stdcall;
function CoverArt_get_CoverType(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_MIMEType(CoverArt: TCoverArt): PChar; export; stdcall;
function CoverArt_get_PictureFormat(CoverArt: TCoverArt): TTagPictureFormat;
  export; stdcall;
function CoverArt_get_Width(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_Height(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_ColorDepth(CoverArt: TCoverArt): Cardinal;
  export; stdcall;
function CoverArt_get_NoOfColors(CoverArt: TCoverArt): Cardinal;
  export; stdcall;
function CoverArt_get_Parent(CoverArt: TCoverArt): TTags; export; stdcall;
function CoverArt_get_Index(CoverArt: TCoverArt): Integer; export; stdcall;

procedure CoverArt_set_Name(CoverArt: TCoverArt; Name: PChar); export; stdcall;
procedure CoverArt_set_Stream(CoverArt: TCoverArt; var buffer: PByte;
  size: Integer); export; stdcall;
procedure CoverArt_set_Description(CoverArt: TCoverArt; Description: PChar);
  export; stdcall;
procedure CoverArt_set_CoverType(CoverArt: TCoverArt; CoverType: Cardinal);
  export; stdcall;
procedure CoverArt_set_MIMEType(CoverArt: TCoverArt; MIMEType: PChar);
  export; stdcall;
procedure CoverArt_set_PictureFormat(CoverArt: TCoverArt;
  PictureFormat: TTagPictureFormat); export; stdcall;
procedure CoverArt_set_Width(CoverArt: TCoverArt; Width: Cardinal);
  export; stdcall;
procedure CoverArt_set_Height(CoverArt: TCoverArt; Height: Cardinal);
  export; stdcall;
procedure CoverArt_set_ColorDepth(CoverArt: TCoverArt; ColorDepth: Cardinal);
  export; stdcall;
procedure CoverArt_set_NoOfColors(CoverArt: TCoverArt; NoOfColors: Cardinal);
  export; stdcall;
procedure CoverArt_set_Parent(CoverArt: TCoverArt; Parent: TTags);
  export; stdcall;
procedure CoverArt_set_Index(CoverArt: TCoverArt; Index: Integer);
  export; stdcall;

function CoverArt_Create(Parent: TTags): TCoverArt; export; stdcall;
procedure CoverArt_Destroy(CoverArt: TCoverArt); export; stdcall;
procedure CoverArt_Clear(CoverArt: TCoverArt); export; stdcall;
procedure CoverArt_Delete(CoverArt: TCoverArt); export; stdcall;
function CoverArt_Assign(thisCover: TCoverArt; CoverArt: TCoverArt): Boolean;
  export; stdcall;
{$ENDREGION}
{$REGION 'SourceAudioAttributes Declarations'}
function SourceAudioAttributes_Create(Parent: TTags): TSourceAudioAttributes;
  export; stdcall;
procedure SourceAudioAttributes_Destroy(SourceAudioAttributes
  : TSourceAudioAttributes); export; stdcall;
function SourceAudioAttributes_get_Channels(SourceAudioAttributes
  : TSourceAudioAttributes): Word; export; stdcall;
function SourceAudioAttributes_get_SamplesPerSec(SourceAudioAttributes
  : TSourceAudioAttributes): Cardinal; export; stdcall;
function SourceAudioAttributes_get_BitsPerSample(SourceAudioAttributes
  : TSourceAudioAttributes): Word; export; stdcall;
function SourceAudioAttributes_get_PlayTime(SourceAudioAttributes
  : TSourceAudioAttributes): Double; export; stdcall;
function SourceAudioAttributes_get_SampleCount(SourceAudioAttributes
  : TSourceAudioAttributes): Int64; export; stdcall;
function SourceAudioAttributes_get_BitRate(SourceAudioAttributes
  : TSourceAudioAttributes): Integer; export; stdcall;
function SourceAudioAttributes_get_Parent(SourceAudioAttributes
  : TSourceAudioAttributes): TTags; export; stdcall;
procedure SourceAudioAttributes_set_Parent(SourceAudioAttributes
  : TSourceAudioAttributes; Parent: TTags); export; stdcall;
{$ENDREGION}
{$REGION 'Tags Declaration'}
function Tags_get_FileName(Tags: TTags): PChar; export; stdcall;
function Tags_get_Loaded(Tags: TTags): Boolean; export; stdcall;
procedure Tags_get_Tags(Tags: TTags; var pointer: PTagArray; var size: Integer);
  export; stdcall;
procedure Tags_get_CoverArts(Tags: TTags; var pointer: PCoverArt;
  var size: Integer); export; stdcall;
function Tags_get_SourceAudioAttributes(Tags: TTags): TSourceAudioAttributes;
  export; stdcall;
function Tags_get_UpperCaseFieldNamesToWrite(Tags: TTags): Boolean;
  export; stdcall;
function Tags_get_Size(Tags: TTags): Int64; export; stdcall;

procedure Tags_set_FileName(Tags: TTags; FileName: PChar); export; stdcall;
procedure Tags_set_Loaded(Tags: TTags; Loaded: Boolean); export; stdcall;
procedure Tags_set_Tags(thisTags: TTags; Tags: TTagArray); export; stdcall;
procedure Tags_set_CoverArts(Tags: TTags; CoverArts: TCoverArtArray);
  export; stdcall;
procedure Tags_set_SourceAudioAttributes(Tags: TTags;
  SourceAudioAttributes: TSourceAudioAttributes); export; stdcall;
procedure Tags_set_UpperCaseFieldNamesToWrite(Tags: TTags;
  UpperCaseFieldNamesToWrite: Boolean); export; stdcall;

function Tags_Create: TTags; export; stdcall;
procedure Tags_Destroy(Tags: TTags); export; stdcall;
function Tags_LoadFromFile(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_LoadFromBASS(Tags: TTags; Channel: Cardinal): Integer;
  export; stdcall;
function Tags_SaveToFile(Tags: TTags; FileName: PChar; TagType: TTagType)
  : Integer; export; stdcall;
procedure Tags_LoadTags(Tags: TTags); export; stdcall;
function Tags_Add(Tags: TTags; Name: PChar): TTag; export; stdcall;
function Tags_Tag(Tags: TTags; Name: PChar): TTag; export; stdcall;
function Tags_GetTag(Tags: TTags; Name: PChar): PChar; export; stdcall;
function Tags_SetTag(Tags: TTags; Name: PChar; Text: PChar): Integer;
  export; stdcall;
// TStrings
function Tags_SetList(Tags: TTags; Name: PChar; List: TStrings): Integer;
  export; stdcall;
function Tags_Delete(Tags: TTags; Index: Integer): Boolean; export; stdcall;
function Tags_Delete2(Tags: TTags; Name: PChar): Boolean; export; stdcall;
function Tags_Remove(Tags: TTags; Index: Integer): Boolean; export; stdcall;
procedure Tags_DeleteAllTags(Tags: TTags); export; stdcall;
procedure Tags_Clear(Tags: TTags); export; stdcall;
function Tags_Count(Tags: TTags): Integer; export; stdcall;
function Tags_Exists(Tags: TTags; Name: PChar): Integer; export; stdcall;
function Tags_TypeCount(Tags: TTags; Name: PChar): Integer; export; stdcall;
procedure Tags_RemoveEmptyTags(Tags: TTags); export; stdcall;
function Tags_CoverArtCount(Tags: TTags): Integer; export; stdcall;
function Tags_CoverArt(Tags: TTags; Name: PChar): TCoverArt; export; stdcall;
function Tags_AddCoverArt(Tags: TTags; Name: PChar): TCoverArt; export; stdcall;
function Tags_AddCoverArt2(Tags: TTags; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt; export; stdcall;
function Tags_AddCoverArt3(Tags: TTags; Name: PChar; FileName: PChar)
  : TCoverArt; export; stdcall;
function Tags_DeleteCoverArt(Tags: TTags; Index: Integer): Boolean;
  export; stdcall;
procedure Tags_DeleteAllCoverArts(Tags: TTags); export; stdcall;
function Tags_Assign(Tags: TTags; Source: TTags): Boolean; export; stdcall;
function Tags_SaveAPEv2Tag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveFlacTag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveID3v1Tag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveID3v2Tag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveMP4Tag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveOggVorbisAndOpusTag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
function Tags_SaveWAVTag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
{$IFDEF MSWINDOWS}
function Tags_SaveWMATag(Tags: TTags; FileName: PChar): Integer;
  export; stdcall;
{$ENDIF}
procedure Tags_LoadAPEv2Tags(Tags: TTags); export; stdcall;
procedure Tags_LoadFlacTags(Tags: TTags); export; stdcall;
procedure Tags_LoadOggVorbisAndOpusTags(Tags: TTags); export; stdcall;
procedure Tags_LoadID3v1Tags(Tags: TTags); export; stdcall;
procedure Tags_LoadID3v2Tags(Tags: TTags); export; stdcall;
procedure Tags_LoadMP4Tags(Tags: TTags); export; stdcall;
procedure Tags_LoadWAVTags(Tags: TTags); export; stdcall;
{$IFDEF MSWINDOWS}
procedure Tags_LoadWMATags(Tags: TTags); export; stdcall;
{$ENDIF}
// TStrings
procedure Tags_LoadNullTerminatedStrings(Tags: TTags; TagType: PChar;
  TagList: TStrings); export; stdcall;
// TStrings
procedure Tags_LoadNullTerminatedWAVRIFFINFOStrings(Tags: TTags;
  TagList: TStrings); export; stdcall;
function Tags_PictureStreamType(Tags: TTags; var buffer: PByte; size: Int64)
  : TTagPictureFormat; export; stdcall;
function Tags_PicturePointerType(Tags: TTags; Picture: pointer)
  : TTagPictureFormat; export; stdcall;

{$ENDREGION}
function RemoveTagsFromFile2(FileName: PChar; TagType: TTagType = ttAutomatic)
  : Integer; export; stdcall;
function TagsLibraryVersion: Cardinal; export; stdcall;
function TagsLibraryErrorCode2String2(ErrorCode: Integer): PChar;
  export; stdcall;

implementation

{$REGION 'Tag'}

function Tag_Create(Parent: TTags): TTag;
begin
  Result := TTag.Create(Parent);
end;

procedure Tag_Destroy(Tag: TTag);
begin
  Tag.Destroy;
end;

function Tag_get_Name(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Name);
end;

function Tag_get_Value(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Value);
end;

function Tag_get_Language(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Language);
end;

function Tag_get_Description(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Description);
end;

function Tag_get_ExtTagType(Tag: TTag): TExtTagType;
begin
  Result := Tag.ExtTagType;
end;

function Tag_get_Parent(Tag: TTag): TTags;
begin
  Result := Tag.Parent;
end;

function Tag_get_Index(Tag: TTag): Integer;
begin
  Result := Tag.Index;
end;

procedure Tag_set_Name(Tag: TTag; Name: PChar);
begin
  Tag.Name := Name;
end;

procedure Tag_set_Value(Tag: TTag; Value: PChar);
begin
  Tag.Value := Value;
end;

procedure Tag_set_Language(Tag: TTag; Language: PChar);
begin
  Tag.Language := Language;
end;

procedure Tag_set_Description(Tag: TTag; Description: PChar);
begin
  Tag.Description := Description;
end;

procedure Tag_set_ExtTagType(Tag: TTag; ExtTagType: TExtTagType);
begin
  Tag.ExtTagType := ExtTagType;
end;

procedure Tag_set_Parent(Tag: TTag; Parent: TTags);
begin
  Tag.Parent := Parent;
end;

procedure Tag_set_Index(Tag: TTag; Index: Integer);
begin
  Tag.Index := Index;
end;

procedure Tag_Clear(Tag: TTag);
begin
  Tag.Clear;
end;

function Tag_Delete(Tag: TTag): Boolean;
begin
  Result := Tag.Delete;
end;

procedure Tag_Remove(Tag: TTag);
begin
  Tag.Remove;
end;

function Tag_Assign(this: TTag; Tag: TTag): Boolean;
begin
  Result := this.Assign(Tag);
end;
{$ENDREGION}
{$REGION 'CoverArt'}

function CoverArt_get_Name(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.Name);
end;

procedure CoverArt_get_Stream(CoverArt: TCoverArt; var buffer: PByte;
  var size: Int64);
var
  bytes: array of byte;
  i: Integer;
begin
  if CoverArt.Stream = nil then
  begin
    exit;
  end;

  SetLength(bytes, CoverArt.Stream.size);
  CoverArt.Stream.Seek(0, soBeginning);
  CoverArt.Stream.Read(bytes[0], CoverArt.Stream.size);
  buffer := PByte(bytes);
  size := CoverArt.Stream.size;
end;

function CoverArt_get_Description(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.Description);
end;

function CoverArt_get_CoverType(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.CoverType;
end;

function CoverArt_get_MIMEType(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.MIMEType);
end;

function CoverArt_get_PictureFormat(CoverArt: TCoverArt): TTagPictureFormat;
begin
  Result := CoverArt.PictureFormat;
end;

function CoverArt_get_Width(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.Width;
end;

function CoverArt_get_Height(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.Height;
end;

function CoverArt_get_ColorDepth(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.ColorDepth;
end;

function CoverArt_get_NoOfColors(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.NoOfColors;
end;

function CoverArt_get_Parent(CoverArt: TCoverArt): TTags;
begin
  Result := CoverArt.Parent;
end;

function CoverArt_get_Index(CoverArt: TCoverArt): Integer;
begin
  Result := CoverArt.Index;
end;

procedure CoverArt_set_Name(CoverArt: TCoverArt; Name: PChar);
begin
  CoverArt.Name := Name;
end;

procedure CoverArt_set_Stream(CoverArt: TCoverArt; var buffer: PByte;
  size: Integer);
var
  i: Integer;
begin
  if CoverArt.Stream <> nil then
    CoverArt.Stream.Destroy;

  CoverArt.Stream := TMemoryStream.Create;
  CoverArt.Stream.Write(buffer[0], size);
end;

procedure CoverArt_set_Description(CoverArt: TCoverArt; Description: PChar);
begin
  CoverArt.Description := Description;
end;

procedure CoverArt_set_CoverType(CoverArt: TCoverArt; CoverType: Cardinal);
begin
  CoverArt.CoverType := CoverType;
end;

procedure CoverArt_set_MIMEType(CoverArt: TCoverArt; MIMEType: PChar);
begin
  CoverArt.MIMEType := MIMEType;
end;

procedure CoverArt_set_PictureFormat(CoverArt: TCoverArt;
  PictureFormat: TTagPictureFormat);
begin
  CoverArt.PictureFormat := PictureFormat;
end;

procedure CoverArt_set_Width(CoverArt: TCoverArt; Width: Cardinal);
begin
  CoverArt.Width := Width;
end;

procedure CoverArt_set_Height(CoverArt: TCoverArt; Height: Cardinal);
begin
  CoverArt.Height := Height;
end;

procedure CoverArt_set_ColorDepth(CoverArt: TCoverArt; ColorDepth: Cardinal);
begin
  CoverArt.ColorDepth := ColorDepth;
end;

procedure CoverArt_set_NoOfColors(CoverArt: TCoverArt; NoOfColors: Cardinal);
begin
  CoverArt.NoOfColors := NoOfColors;
end;

procedure CoverArt_set_Parent(CoverArt: TCoverArt; Parent: TTags);
begin
  CoverArt.Parent := Parent;
end;

procedure CoverArt_set_Index(CoverArt: TCoverArt; Index: Integer);
begin
  CoverArt.Index := Index;
end;

function CoverArt_Create(Parent: TTags): TCoverArt;
begin
  Result := TCoverArt.Create(Parent);
end;

procedure CoverArt_Destroy(CoverArt: TCoverArt);
begin
  CoverArt.Destroy;
end;

procedure CoverArt_Clear(CoverArt: TCoverArt);
begin
  CoverArt.Clear;
end;

procedure CoverArt_Delete(CoverArt: TCoverArt);
begin
  CoverArt.Delete;
end;

function CoverArt_Assign(thisCover: TCoverArt; CoverArt: TCoverArt): Boolean;
begin
  thisCover.Assign(CoverArt);
end;

{$ENDREGION}
{$REGION 'SourceAudioAttributes'}

function SourceAudioAttributes_Create(Parent: TTags): TSourceAudioAttributes;
begin
  Result := TSourceAudioAttributes.Create(Parent);
end;

procedure SourceAudioAttributes_Destroy(SourceAudioAttributes
  : TSourceAudioAttributes);
begin
  SourceAudioAttributes.Destroy;
end;

function SourceAudioAttributes_get_Channels(SourceAudioAttributes
  : TSourceAudioAttributes): Word;
begin
  Result := SourceAudioAttributes.Channels;
end;

function SourceAudioAttributes_get_SamplesPerSec(SourceAudioAttributes
  : TSourceAudioAttributes): Cardinal;
begin
  Result := SourceAudioAttributes.SamplesPerSec;
end;

function SourceAudioAttributes_get_BitsPerSample(SourceAudioAttributes
  : TSourceAudioAttributes): Word;
begin
  Result := SourceAudioAttributes.BitsPerSample;
end;

function SourceAudioAttributes_get_PlayTime(SourceAudioAttributes
  : TSourceAudioAttributes): Double;
begin
  Result := SourceAudioAttributes.PlayTime;
end;

function SourceAudioAttributes_get_SampleCount(SourceAudioAttributes
  : TSourceAudioAttributes): Int64;
begin
  Result := SourceAudioAttributes.SampleCount;
end;

function SourceAudioAttributes_get_BitRate(SourceAudioAttributes
  : TSourceAudioAttributes): Integer;
begin
  Result := SourceAudioAttributes.BitRate;
end;

function SourceAudioAttributes_get_Parent(SourceAudioAttributes
  : TSourceAudioAttributes): TTags;
begin
  Result := SourceAudioAttributes.Parent;
end;

procedure SourceAudioAttributes_set_Parent(SourceAudioAttributes
  : TSourceAudioAttributes; Parent: TTags);
begin
  SourceAudioAttributes.Parent := Parent;
end;
{$ENDREGION}
{$REGION 'Tags'}

function Tags_get_FileName(Tags: TTags): PChar;
begin
  Result := PChar(Tags.FileName);
end;

function Tags_get_Loaded(Tags: TTags): Boolean;
begin
  Result := Tags.Loaded;
end;

procedure Tags_get_Tags(Tags: TTags; var pointer: PTagArray; var size: Integer);
begin
  pointer := PTagArray(Tags.Tags);
  size := Length(Tags.Tags);
end;

procedure Tags_get_CoverArts(Tags: TTags; var pointer: PCoverArt;
  var size: Integer);
begin
  pointer := PCoverArt(Tags.CoverArts);
  size := Length(Tags.CoverArts);
end;

function Tags_get_SourceAudioAttributes(Tags: TTags): TSourceAudioAttributes;
begin
  Result := Tags.SourceAudioAttributes;
end;

function Tags_get_UpperCaseFieldNamesToWrite(Tags: TTags): Boolean;
begin
  Result := Tags.UpperCaseFieldNamesToWrite;
end;

function Tags_get_Size(Tags: TTags): Int64;
begin
  Result := Tags.size;
end;

procedure Tags_set_FileName(Tags: TTags; FileName: PChar);
begin
  Tags.FileName := FileName;
end;

procedure Tags_set_Loaded(Tags: TTags; Loaded: Boolean);
begin
  Tags.Loaded := Loaded;
end;

procedure Tags_set_Tags(thisTags: TTags; Tags: TTagArray);
begin
  TTagArray(thisTags.Tags) := Tags;
end;

procedure Tags_set_CoverArts(Tags: TTags; CoverArts: TCoverArtArray);
begin
  TCoverArtArray(Tags.CoverArts) := CoverArts;
end;

procedure Tags_set_SourceAudioAttributes(Tags: TTags;
  SourceAudioAttributes: TSourceAudioAttributes);
begin
  Tags.SourceAudioAttributes := SourceAudioAttributes;
end;

procedure Tags_set_UpperCaseFieldNamesToWrite(Tags: TTags;
  UpperCaseFieldNamesToWrite: Boolean);
begin
  Tags.UpperCaseFieldNamesToWrite := UpperCaseFieldNamesToWrite;
end;

function Tags_Create: TTags;
begin
  Result := TTags.Create;
end;

procedure Tags_Destroy(Tags: TTags);
begin
  Tags.Destroy;
end;

function Tags_LoadFromFile(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.LoadFromFile(FileName);
end;

function Tags_LoadFromBASS(Tags: TTags; Channel: Cardinal): Integer;
begin
  Result := Tags.LoadFromBASS(Channel);
end;

function Tags_SaveToFile(Tags: TTags; FileName: PChar;
  TagType: TTagType): Integer;
begin
  Result := Tags.SaveToFile(FileName, TagType);
end;

procedure Tags_LoadTags(Tags: TTags);
begin
  Tags.LoadTags;
end;

function Tags_Add(Tags: TTags; Name: PChar): TTag;
begin
  Result := Tags.Add(Name);
end;

function Tags_Tag(Tags: TTags; Name: PChar): TTag;
begin
  Result := Tags.Tag(Name);
end;

function Tags_GetTag(Tags: TTags; Name: PChar): PChar;
begin
  Result := PChar(Tags.GetTag(Name));
end;

function Tags_SetTag(Tags: TTags; Name: PChar; Text: PChar): Integer;
begin
  Result := Tags.SetTag(Name, Text);
end;

// TStrings
function Tags_SetList(Tags: TTags; Name: PChar; List: TStrings): Integer;
begin
  Result := Tags.SetList(Name, List);
end;

function Tags_Delete(Tags: TTags; Index: Integer): Boolean;
begin
  Result := Tags.Delete(Index);
end;

function Tags_Delete2(Tags: TTags; Name: PChar): Boolean;
begin
  Result := Tags.Delete(Name);
end;

function Tags_Remove(Tags: TTags; Index: Integer): Boolean;
begin
  Result := Tags.Remove(Index);
end;

procedure Tags_DeleteAllTags(Tags: TTags);
begin
  Tags.DeleteAllTags;
end;

procedure Tags_Clear(Tags: TTags);
begin
  Tags.Clear;
end;

function Tags_Count(Tags: TTags): Integer;
begin
  Result := Tags.Count;
end;

function Tags_Exists(Tags: TTags; Name: PChar): Integer;
begin
  Result := Tags.Exists(Name);
end;

function Tags_TypeCount(Tags: TTags; Name: PChar): Integer;
begin
  Result := Tags.TypeCount(Name);
end;

procedure Tags_RemoveEmptyTags(Tags: TTags);
begin
  Tags.RemoveEmptyTags;
end;

function Tags_CoverArtCount(Tags: TTags): Integer;
begin
  Result := Tags.CoverArtCount;
end;

function Tags_CoverArt(Tags: TTags; Name: PChar): TCoverArt;
begin
  Result := Tags.CoverArt(Name);
end;

function Tags_AddCoverArt(Tags: TTags; Name: PChar): TCoverArt;
begin
  Result := Tags.AddCoverArt(Name);
end;

function Tags_AddCoverArt2(Tags: TTags; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt;
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.AddCoverArt(Name, Stream, MIMEType);
end;

function Tags_AddCoverArt3(Tags: TTags; Name: PChar; FileName: PChar)
  : TCoverArt;
begin
  Result := Tags.AddCoverArt(Name, FileName);
end;

function Tags_DeleteCoverArt(Tags: TTags; Index: Integer): Boolean;
begin
  Result := Tags.DeleteCoverArt(Index);
end;

procedure Tags_DeleteAllCoverArts(Tags: TTags);
begin
  Tags.DeleteAllCoverArts;
end;

function Tags_Assign(Tags: TTags; Source: TTags): Boolean;
begin
  Result := Tags.Assign(Source);
end;

function Tags_SaveAPEv2Tag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveAPEv2Tag(FileName);
end;

function Tags_SaveFlacTag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveFlacTag(FileName);
end;

function Tags_SaveID3v1Tag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveID3v1Tag(FileName);
end;

function Tags_SaveID3v2Tag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveID3v2Tag(FileName);
end;

function Tags_SaveMP4Tag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveMP4Tag(FileName);
end;

function Tags_SaveOggVorbisAndOpusTag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveOggVorbisAndOpusTag(FileName);
end;

function Tags_SaveWAVTag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveWAVTag(FileName);
end;

{$IFDEF MSWINDOWS}

function Tags_SaveWMATag(Tags: TTags; FileName: PChar): Integer;
begin
  Result := Tags.SaveWMATag(FileName);
end;

{$ENDIF}

procedure Tags_LoadAPEv2Tags(Tags: TTags);
begin
  Tags.LoadAPEv2Tags;
end;

procedure Tags_LoadFlacTags(Tags: TTags);
begin
  Tags.LoadFlacTags;
end;

procedure Tags_LoadOggVorbisAndOpusTags(Tags: TTags);
begin
  Tags.LoadOggVorbisAndOpusTags;
end;

procedure Tags_LoadID3v1Tags(Tags: TTags);
begin
  Tags.LoadID3v1Tags;
end;

procedure Tags_LoadID3v2Tags(Tags: TTags);
begin
  Tags.LoadID3v2Tags;
end;

procedure Tags_LoadMP4Tags(Tags: TTags);
begin
  Tags.LoadMP4Tags;
end;

procedure Tags_LoadWAVTags(Tags: TTags);
begin
  Tags.LoadWAVTags;
end;

{$IFDEF MSWINDOWS}

procedure Tags_LoadWMATags(Tags: TTags);
begin
  Tags.LoadWMATags;
end;

{$ENDIF}

// TStrings
procedure Tags_LoadNullTerminatedStrings(Tags: TTags; TagType: PChar;
  TagList: TStrings);
begin
  Tags.LoadNullTerminatedStrings(TagType, TagList);
end;

// TStrings
procedure Tags_LoadNullTerminatedWAVRIFFINFOStrings(Tags: TTags;
  TagList: TStrings);
begin
  Tags.LoadNullTerminatedWAVRIFFINFOStrings(TagList);
end;

function Tags_PictureStreamType(Tags: TTags; var buffer: PByte; size: Int64)
  : TTagPictureFormat;
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.PictureStreamType(Stream);
end;

function Tags_PicturePointerType(Tags: TTags; Picture: pointer)
  : TTagPictureFormat;
begin
  Result := Tags.PicturePointerType(Picture);
end;

{$ENDREGION}

function RemoveTagsFromFile2(FileName: PChar;
  TagType: TTagType = ttAutomatic): Integer;
begin
  RemoveTagsFromFile(FileName, TagType);
end;

function TagsLibraryVersion: Cardinal;
begin
  Result := TAGSLIBRARY_VERSION;
end;

function TagsLibraryErrorCode2String2(ErrorCode: Integer): PChar;
begin
  Result := PChar(TagsLibraryErrorCode2String(ErrorCode));
end;

end.
