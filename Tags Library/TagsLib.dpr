library TagsLib;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  TagsLibExports in 'TagsLibExports.pas',
  APEv2Library in 'Modules\APEv2Library.pas',
  FlacTagLibrary in 'Modules\FlacTagLibrary.pas',
  ID3v1Library in 'Modules\ID3v1Library.pas',
  ID3v2Library in 'Modules\ID3v2Library.pas',
  MP4TagLibrary in 'Modules\MP4TagLibrary.pas',
  OggVorbisAndOpusTagLibrary in 'Modules\OggVorbisAndOpusTagLibrary.pas',
  TagsLibrary in 'Modules\TagsLibrary.pas',
  TagsLibraryDefs in 'Modules\TagsLibraryDefs.pas',
  uTExtendedX87 in 'Modules\uTExtendedX87.pas',
  WAVTagLibrary in 'Modules\WAVTagLibrary.pas',
  WMATagLibrary in 'Modules\WMATagLibrary.pas';

{$R *.res}
function DllMessage(text: PChar): PWideChar; export; stdcall;
  var str: String;
begin
  str := text;
  WriteLn(str);
  Result := PWideChar(str + ' World!');
end;

exports DllMessage;
exports Tag_get_Name;
exports Tag_get_Value;
exports Tag_get_Language;
exports Tag_get_Description;
exports Tag_get_ExtTagType;
exports Tag_get_Parent;
exports Tag_get_Index;
exports Tag_set_Name;
exports Tag_set_Value;
exports Tag_set_Language;
exports Tag_set_Description;
exports Tag_set_ExtTagType;
exports Tag_set_Parent;
exports Tag_set_Index;
exports Tag_Create;
exports Tag_Destroy;
exports Tag_Clear;
exports Tag_Delete;
exports Tag_Remove;
exports Tag_Assign;

exports CoverArt_get_Name;
exports CoverArt_get_Stream;
exports CoverArt_get_Description;
exports CoverArt_get_CoverType;
exports CoverArt_get_MIMEType;
exports CoverArt_get_PictureFormat;
exports CoverArt_get_Width;
exports CoverArt_get_Height;
exports CoverArt_get_ColorDepth;
exports CoverArt_get_NoOfColors;
exports CoverArt_get_Parent;
exports CoverArt_get_Index;

exports CoverArt_set_Name;
exports CoverArt_set_Stream;
exports CoverArt_set_Description;
exports CoverArt_set_CoverType;
exports CoverArt_set_MIMEType;
exports CoverArt_set_PictureFormat;
exports CoverArt_set_Width;
exports CoverArt_set_Height;
exports CoverArt_set_ColorDepth;
exports CoverArt_set_NoOfColors;
exports CoverArt_set_Parent;
exports CoverArt_set_Index;

exports CoverArt_Create;
exports CoverArt_Destroy;
exports CoverArt_Clear;
exports CoverArt_Delete;
exports CoverArt_Assign;

exports SourceAudioAttributes_Create;
exports SourceAudioAttributes_Destroy;
exports SourceAudioAttributes_get_Channels;
exports SourceAudioAttributes_get_SamplesPerSec;
exports SourceAudioAttributes_get_BitsPerSample;
exports SourceAudioAttributes_get_PlayTime;
exports SourceAudioAttributes_get_SampleCount;
exports SourceAudioAttributes_get_BitRate;
exports SourceAudioAttributes_get_Parent;
exports SourceAudioAttributes_set_Parent;

exports Tags_get_FileName;
exports Tags_get_Loaded;
exports Tags_get_Tags;
exports Tags_get_CoverArts;
exports Tags_get_SourceAudioAttributes;
exports Tags_get_UpperCaseFieldNamesToWrite;
exports Tags_get_Size;

exports Tags_set_FileName;
exports Tags_set_Loaded;
exports Tags_set_Tags;
exports Tags_set_CoverArts;
exports Tags_set_SourceAudioAttributes;
exports Tags_set_UpperCaseFieldNamesToWrite;

exports Tags_Create;
exports Tags_Destroy;
exports Tags_LoadFromFile;
exports Tags_LoadFromBASS;
exports Tags_SaveToFile;
exports Tags_LoadTags;
exports Tags_Add;
exports Tags_Tag;
exports Tags_GetTag;
exports Tags_SetTag;
exports Tags_SetList;
exports Tags_Delete;
exports Tags_Delete2;
exports Tags_Remove;
exports Tags_DeleteAllTags;
exports Tags_Clear;
exports Tags_Count;
exports Tags_Exists;
exports Tags_TypeCount;
exports Tags_RemoveEmptyTags;
exports Tags_CoverArtCount;
exports Tags_CoverArt;
exports Tags_AddCoverArt;
exports Tags_AddCoverArt2;
exports Tags_AddCoverArt3;
exports Tags_DeleteCoverArt;
exports Tags_DeleteAllCoverArts;
exports Tags_Assign;
exports Tags_SaveAPEv2Tag;
exports Tags_SaveFlacTag;
exports Tags_SaveID3v1Tag;
exports Tags_SaveID3v2Tag;
exports Tags_SaveMP4Tag;
exports Tags_SaveOggVorbisAndOpusTag;
exports Tags_SaveWAVTag;
{$IFDEF MSWINDOWS}
exports Tags_SaveWMATag;
{$ENDIF}
exports Tags_LoadAPEv2Tags;
exports Tags_LoadFlacTags;
exports Tags_LoadOggVorbisAndOpusTags;
exports Tags_LoadID3v1Tags;
exports Tags_LoadID3v2Tags;
exports Tags_LoadMP4Tags;
exports Tags_LoadWAVTags;
{$IFDEF MSWINDOWS}
exports Tags_LoadWMATags;
{$ENDIF}
exports Tags_LoadNullTerminatedStrings;
exports Tags_LoadNullTerminatedWAVRIFFINFOStrings;
exports Tags_PictureStreamType;
exports Tags_PicturePointerType;

exports TagsLibraryErrorCode2String;
exports RemoveTagsFromFile2;
exports TagsLibraryErrorCode2String2;

exports ID3v1TagLibraryErrorCodeToTagsLibraryErrorCode;
exports ID3v2TagLibraryErrorCodeToTagsLibraryErrorCode;
exports FlacTagLibraryErrorCodeToTagsLibraryErrorCode;
exports MP4TagLibraryErrorCodeToTagsLibraryErrorCode;
exports APEv2TagLibraryErrorCodeToTagsLibraryErrorCode;
exports OpusVorbisTagLibraryErrorCodeToTagsLibraryErrorCode;
exports WAVTagLibraryErrorCodeToTagsLibraryErrorCode;
{$IFDEF MSWINDOWS}
exports WMATagLibraryErrorCodeToTagsLibraryErrorCode;
{$ENDIF}

exports TagsLibraryVersion;
begin
end.
