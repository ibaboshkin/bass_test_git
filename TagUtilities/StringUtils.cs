﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TagUtilities
{
    public static class StringUtils
    {
        /// <summary>
        /// String similarity parameters
        /// </summary>
        public class SimilarityParameters
        {
            /// <summary>
            /// Minimum acceptable similarity percentage for long strings. Value should be between 0 to 100.
            /// </summary>
            public double MinSimilarityLong { get; set; }

            /// <summary>
            /// Minimum acceptable similarity percentage for short strings. Value should be between 0 to 100.
            /// </summary>
            public double MinSimilarityShort { get; set; }

            /// <summary>
            /// Maximum acceptable tolerance percentage for long strings. Value should be between 0 to 100.
            /// </summary>
            public double MaxToleranceLong { get; set; }

            /// <summary>
            /// Maximum acceptable tolerance percentage for short strings. Value should be between 0 to 100.
            /// </summary>
            public double MaxToleranceShort { get; set; }

            public SimilarityParameters()
            {
                MinSimilarityLong = 80;
                MinSimilarityShort = 60;
                MaxToleranceLong = 20;
                MaxToleranceShort = 10;
            }
        }

        /// <summary>
        /// Removes punctuation from text
        /// </summary>
        /// <param name="text">Text to remove punctuation from</param>
        /// <returns>Text without punctuation</returns>
        public static string RemovePunctuation(this string text)
        {
            string pattern = @"[\"".,:;!?()\[\]-]";

            Regex reg = new Regex(pattern);
            return reg.Replace(text, string.Empty);
        }

        /// <summary>
        /// Remove accents from text
        /// </summary>
        /// <param name="input">Text to remove accents from</param>
        /// <returns>Text without accents</returns>
        public static string RemoveAccents(this string input)
        {
            string beforeConversion = "ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖØÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöøùúûüýÿĀāĂăĆćĈĉČčĎĒēĔĕĚěĜĝĞğĤĥĨĩĪīĬĭİŁłŃńŇňŌōŎŏŔŕŘřŚśŜŝŠšŨũŪūŬŭŮů";
            string afterConversion = "AAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyyAaAaCcCcCcDEeEeEeGgGgHhIiIiIiILlNnNnOoOoRrRrSsSsSsUuUuUuUu";

            Regex pattern = new Regex("[" + beforeConversion + "]");

            if (pattern.IsMatch(input))
            {
                return input;
            }

            StringBuilder builder = new StringBuilder(input);
            int length = input.Length;

            for (int i = 0; i < beforeConversion.Length; i++)
            {
                builder.Replace(beforeConversion[i], afterConversion[i]);
            }

            builder.Replace("Æ", "Ae");
            builder.Replace("æ", "ae");
            builder.Replace("ß", "ss");
            builder.Replace("Œ", "Oe");
            builder.Replace("œ", "oe");

            return builder.ToString();
#if false

            for (int i = 0; i < length; i++)
            {
                char c = input[i];

                // Quick test: if it's not in range then just keep
                // current character
                if (c < '\u00c0' || c > '\uFB06')
                    builder.Append(c);
                else
                {
                    switch (c)
                    {

                        case '\u00C0':
                        // Ã€
                        case '\u00C1':
                        // ï¿½?
                        case '\u00C2':
                        // Ã‚
                        case '\u00C3':
                        // Ãƒ
                        case '\u00C4':
                        // Ã„
                        case '\u00C5':  // Ã…
                            builder.Append('A');
                            break;

                        case '\u00C6':  // Ã†
                            builder.Append('A');
                            builder.Append('E');
                            break;

                        case '\u00C7':  // Ã‡
                            builder.Append('C');
                            break;

                        case '\u00C8':
                        // Ãˆ
                        case '\u00C9':
                        // Ã‰
                        case '\u00CA':
                        // ÃŠ
                        case '\u00CB':  // Ã‹
                            builder.Append('E');
                            break;

                        case '\u00CC':
                        // ÃŒ
                        case '\u00CD':
                        // ï¿½?
                        case '\u00CE':
                        // ÃŽ
                        case '\u00CF':  // ï¿½?
                            builder.Append('I');
                            break;

                        case '\u0132':  // Ä²
                            builder.Append('I');
                            builder.Append('J');
                            break;

                        case '\u00D0':  // ï¿½?
                            builder.Append('D');
                            break;

                        case '\u00D1':  // Ã‘
                            builder.Append('N');
                            break;

                        case '\u00D2':
                        // Ã’
                        case '\u00D3':
                        // Ã“
                        case '\u00D4':
                        // Ã”
                        case '\u00D5':
                        // Ã•
                        case '\u00D6':
                        // Ã–
                        case '\u00D8':  // Ã˜
                            builder.Append('O');
                            break;

                        case '\u0152':  // Å’
                            builder.Append('O');
                            builder.Append('E');
                            break;

                        case '\u00DE':  // Ãž
                            builder.Append('T');
                            builder.Append('H');
                            break;

                        case '\u00D9':
                        // Ã™
                        case '\u00DA':
                        // Ãš
                        case '\u00DB':
                        // Ã›
                        case '\u00DC':  // Ãœ
                            builder.Append('U');
                            break;

                        case '\u00DD':
                        // ï¿½?
                        case '\u0178':  // Å¸
                            builder.Append('Y');
                            break;

                        case '\u00E0':
                        // Ã 
                        case '\u00E1':
                        // Ã¡
                        case '\u00E2':
                        // Ã¢
                        case '\u00E3':
                        // Ã£
                        case '\u00E4':
                        // Ã¤
                        case '\u00E5':  // Ã¥
                            builder.Append('a');
                            break;

                        case '\u00E6':  // Ã¦
                            builder.Append('a');
                            builder.Append('e');
                            break;

                        case '\u00E7':  // Ã§
                            builder.Append('c');
                            break;

                        case '\u00E8':
                        // Ã¨
                        case '\u00E9':
                        // Ã©
                        case '\u00EA':
                        // Ãª
                        case '\u00EB':  // Ã«
                            builder.Append('e');
                            break;

                        case '\u00EC':
                        // Ã¬
                        case '\u00ED':
                        // Ã­
                        case '\u00EE':
                        // Ã®
                        case '\u00EF':  // Ã¯
                            builder.Append('i');
                            break;

                        case '\u0133':  // Ä³
                            builder.Append('i');
                            builder.Append('j');
                            break;

                        case '\u00F0':  // Ã°
                            builder.Append('d');
                            break;

                        case '\u00F1':  // Ã±
                            builder.Append('n');
                            break;

                        case '\u00F2':
                        // Ã²
                        case '\u00F3':
                        // Ã³
                        case '\u00F4':
                        // Ã´
                        case '\u00F5':
                        // Ãµ
                        case '\u00F6':
                        // Ã¶
                        case '\u00F8':  // Ã¸
                            builder.Append('o');
                            break;

                        case '\u0153':  // Å“
                            builder.Append('o');
                            builder.Append('e');
                            break;

                        case '\u00DF':  // ÃŸ
                            builder.Append('s');
                            builder.Append('s');
                            break;

                        case '\u00FE':  // Ã¾
                            builder.Append('t');
                            builder.Append('h');
                            break;

                        case '\u00F9':
                        // Ã¹
                        case '\u00FA':
                        // Ãº
                        case '\u00FB':
                        // Ã»
                        case '\u00FC':  // Ã¼
                            builder.Append('u');
                            break;

                        case '\u00FD':
                        // Ã½
                        case '\u00FF':  // Ã¿
                            builder.Append('y');
                            break;

                        case '\uFB00':  // ï¬€
                            builder.Append('f');
                            builder.Append('f');
                            break;

                        case '\uFB01':  // ï¿½?
                            builder.Append('f');
                            builder.Append('i');
                            break;

                        case '\uFB02':  // ï¬‚
                            builder.Append('f');
                            builder.Append('l');
                            break;
                        // following 2 are commented as they can break the maxSizeNeeded (and doing *3 could be expensive)
                        //        case '\uFB03': // ï¬ƒ
                        //            builder.Append('f');
                        //            builder.Append('f');
                        //            builder.Append('i');
                        //            break;
                        //        case '\uFB04': // ï¬„
                        //            builder.Append('f');
                        //            builder.Append('f');
                        //            builder.Append('l');
                        //            break;

                        case '\uFB05':  // ï¬…
                            builder.Append('f');
                            builder.Append('t');
                            break;

                        case '\uFB06':  // ï¬†
                            builder.Append('s');
                            builder.Append('t');
                            break;

                        default:
                            builder.Append(c);
                            break;

                    }
                }
            }

            return builder.ToString(); 
#endif
        }

        /// <summary>
        /// Checks two strings for similarity
        /// </summary>
        /// <param name="text1">This string</param>
        /// <param name="text2">String to compare with</param>
        /// <param name="ignoreCase">Ignore case</param>
        /// <param name="ignorePunctuation">Ignore punctuation</param>
        /// <param name="removeaccents">Compare strings with accents replaced by english letters</param>
        /// <param name="exactMatchOnly">Exact match check. It means that strings will be compared with their original case and with punctuation.
        /// ignoreCase and ignorePunctuation parameters are ignored. Though you can still compare strings without accents by setting removeaccents to true.</param>
        /// <param name="parameters">String similarity parameters</param>
        /// <returns>True is strings are similar</returns>
        public static bool AreSimilar(string text1, string text2, bool ignoreCase = true, bool ignorePunctuation = true, bool removeaccents = true, bool searchMode = false, bool exactMatchOnly = false, SimilarityParameters parameters = null)
        {
            if (removeaccents)
            {
                return TestAreEqual(text1, text2, ignoreCase, ignorePunctuation, searchMode, exactMatchOnly, parameters) || TestAreEqual(RemoveAccents(text1), RemoveAccents(text2), ignoreCase, ignorePunctuation, searchMode, exactMatchOnly, parameters);
            }
            else
            {
                return TestAreEqual(text1, text2, ignoreCase, ignorePunctuation, searchMode, exactMatchOnly, parameters);
            }
        }

        /// <summary>
        /// Checks two strings for similarity
        /// </summary>
        /// <param name="text1">This text</param>
        /// <param name="text2">Text to compare with</param>
        /// <param name="ignoreCase">Ignore case</param>
        /// <param name="ignorePunctuation">Ignore punctuation</param>
        /// <param name="removeaccents">Compare strings with accents replaced by english letters</param>
        /// <param name="exactMatchOnly">Exact match check. It means that strings will be compared with their original case and with punctuation.
        /// ignoreCase and ignorePunctuation parameters are ignored. Though you can still compare strings without accents by setting removeaccents to true.</param>
        /// <param name="parameters">String similarity parameters</param>
        /// <returns>True is strings are similar</returns>
        public static bool IsSimilarTo(this string text1, string text2, bool ignoreCase = true, bool ignorePunctuation = true, bool removeaccents = true, bool searchMode = false, bool exactMatchOnly = false, SimilarityParameters parameters = null)
        {
            return AreSimilar(text1, text2, ignoreCase, ignorePunctuation, removeaccents, searchMode, exactMatchOnly, parameters);
        }

        private static bool TestAreEqual(string string1, string string2, bool ignoreCase = true, bool ignorePunctuation = true, bool searchMode = false, bool exactMatchOnly = false, SimilarityParameters parameters = null)
        {
            string string1WithoutPunctuation = string.Empty;
            string string2WithoutPunctuation = string.Empty;

            // Step 1. Just check for simple equality
            if (string.Compare(string1, string2, ignoreCase) == 0)
            {
                return true;
            }

            if (exactMatchOnly)
            {
                return false;
            }

            // Step 2. Check for simple equality without punctuation
            if (ignorePunctuation)
            {
                string1WithoutPunctuation = RemovePunctuation(string1);
                string2WithoutPunctuation = RemovePunctuation(string2);

                if (string.Compare(string1WithoutPunctuation, string2WithoutPunctuation, ignoreCase) == 0)
                {
                    return true;
                }
            }

            if (searchMode)
            {
                // Step 3. Check for one text containing another one
                if (ignoreCase)
                {
                    string invariant1 = string1.ToUpperInvariant();
                    string invariant2 = string2.ToUpperInvariant();

                    if (invariant1.Contains(invariant2) || invariant2.Contains(invariant1))
                    {
                        return true;
                    }
                }
                else
                {
                    if (string1.Contains(string2) || string2.Contains(string1))
                    {
                        return true;
                    }
                }

                // Step 4. Check for one text containing another one without punctuation
                if (ignorePunctuation)
                {
                    if (ignoreCase)
                    {
                        string invariant1WithoutPunctuation = string1WithoutPunctuation.ToUpperInvariant();
                        string invariant2WithoutPunctuation = string2WithoutPunctuation.ToUpperInvariant();

                        if (invariant1WithoutPunctuation.Contains(invariant2WithoutPunctuation) || invariant2WithoutPunctuation.Contains(invariant1WithoutPunctuation))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (string1WithoutPunctuation.Contains(string2WithoutPunctuation) || string2WithoutPunctuation.Contains(string1WithoutPunctuation))
                        {
                            return true;
                        }
                    }
                }
            }

            if (parameters == null)
            {
                parameters = new SimilarityParameters();
            }

            // Step 5. Check for similarity
            StringCompare sc = new StringCompare(parameters.MinSimilarityLong, parameters.MinSimilarityShort, parameters.MaxToleranceLong, parameters.MaxToleranceShort);

            double similarity = 0;
            double tolerance = 0;

            if (sc.IsEqual(string1, string2, out similarity, out tolerance))
            {
                return true;
            }

            // Step 6. Check for similarity without punctuation
            if (ignorePunctuation)
            {
                return sc.IsEqual(string1WithoutPunctuation, string2WithoutPunctuation, out similarity, out tolerance);
            }

            return false;
        }

        private static int FindNextPercent(string text, int startPosition)
        {
            int i = startPosition;

            do
            {
                i = text.IndexOf('%', i);

                if (i == -1)
                {
                    return -1;
                }

                if (i == 0 || text[i - 1] != '\\')
                {
                    return i;
                }

                i++;
            } while (i < text.Length);

            return -1;
        }

        public static IEnumerable<string> ParseParameters(this string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            List<string> parameters = new List<string>();

            if (text.Contains("%"))
            {
                int start = FindNextPercent(text, 0);

                if (start != -1)
                {
                    int end;

                    do
                    {
                        end = FindNextPercent(text, start + 1);

                        if (end != -1)
                        {
                            parameters.Add(text.Substring(start, end - start + 1));

                            start = FindNextPercent(text, end + 1);
                        }
                    } while (start != -1 && end != -1);

                }
            }

            return parameters;
        }

        public static string ReplaceParameter(this string text, string parameter, string value)
        {
            if (string.IsNullOrEmpty(parameter))
            {
                throw new ArgumentNullException("parameter");
            }

            if (parameter[0] != '%' || parameter.Last() != '%')
            {
                throw new ArgumentException("Wrong parameter format");
            }

            StringBuilder sb = new StringBuilder(text);
            sb.ReplaceParameter(parameter, value);
            return sb.ToString();
        }

        public static void ReplaceParameter(this StringBuilder sb, string parameter, string value)
        {
            if (string.IsNullOrEmpty(parameter))
            {
                throw new ArgumentNullException("parameter");
            }

            if (parameter[0] != '%' || parameter.Last() != '%')
            {
                throw new ArgumentException("Wrong parameter format");
            }

            int pos;

            do
            {
                pos = sb.ToString().IndexOf(parameter);

                if (pos != -1 && (pos == 0 || sb[pos - 1] != '\\'))
                {
                    sb.Replace(parameter, value, pos, parameter.Length);
                }
            } while (pos != -1 && pos < sb.Length);
        }
    }
}
