unit CommonTypes;

interface

Uses
  SysUtils,
  Classes;

{$REGION 'Constants'}

Const
  TAGSLIBRARY_VERSION = $01002132;

Const
  TAGSLIBRARY_SUCCESS = 0;
  TAGSLIBRARY_ERROR = $FFFF;
  TAGSLIBRARY_ERROR_NO_TAG_FOUND = 1;
  TAGSLIBRARY_ERROR_FILENOTFOUND = 2;
  TAGSLIBRARY_ERROR_EMPTY_TAG = 3;
  TAGSLIBRARY_ERROR_EMPTY_FRAMES = 4;
  TAGSLIBRARY_ERROR_OPENING_FILE = 5;
  TAGSLIBRARY_ERROR_READING_FILE = 6;
  TAGSLIBRARY_ERROR_WRITING_FILE = 7;
  TAGSLIBRARY_ERROR_CORRUPT = 8;
  TAGSLIBRARY_ERROR_NOT_SUPPORTED_VERSION = 9;
  TAGSLIBRARY_ERROR_NOT_SUPPORTED_FORMAT = 10;
  TAGSLIBRARY_ERROR_BASS_NOT_LOADED = 11;
  TAGSLIBRARY_ERROR_BASS_ChannelGetTags_NOT_FOUND = 12;
  TAGSLIBRARY_ERROR_DOESNT_FIT = 13;
  TAGSLIBRARY_ERROR_NEED_EXCLUSIVE_ACCESS = 14;
  TAGSLIBRARY_ERROR_WMATAGLIBRARY_COULDNTLOADDLL = 15;
  TAGSLIBRARY_ERROR_WMATAGLIBRARY_COULDNOTCREATEMETADATAEDITOR = 16;
  TAGSLIBRARY_ERROR_WMATAGLIBRARY_COULDNOTQIFORIWMHEADERINFO3 = 17;
  TAGSLIBRARY_ERROR_WMATAGLIBRARY_COULDNOTQUERY_ATTRIBUTE_COUNT = 18;
  TAGSLIBRARY_ERROR_MP4TAGLIBRARY_UPDATE_stco = 19;
  TAGSLIBRARY_ERROR_MP4TAGLIBRARY_UPDATE_co64 = 20;

Const
  BEXT_Description = 'BEXT Description';
  BEXT_Originator = 'BEXT Originator';
  BEXT_OriginatorReference = 'BEXT OriginatorReference';
  BEXT_OriginationDate = 'BEXT OriginationDate';
  BEXT_OriginationTime = 'BEXT OriginationTime';
  BEXT_TimeReference = 'BEXT TimeReference';
  BEXT_Version = 'BEXT Version';
  BEXT_UMID = 'BEXT UMID';
  BEXT_CodingHistory = 'BEXT CodingHistory';

  CART_Version = 'CART Version';
  CART_Title = 'CART Title';
  CART_Artist = 'CART Artist';
  CART_CutID = 'CART CutID';
  CART_ClientID = 'CART ClientID';
  CART_Category = 'CART Category';
  CART_Classification = 'CART Classification';
  CART_OutCue = 'CART OutCue';
  CART_StartDate = 'CART StartDate';
  CART_StartTime = 'CART StartTime';
  CART_EndDate = 'CART EndDate';
  CART_EndTime = 'CART EndTime';
  CART_ProducerAppID = 'CART ProducerAppID';
  CART_ProducerAppVersion = 'CART ProducerAppVersion';
  CART_UserDef = 'CART UserDef';
  CART_LevelReference = 'CART LevelReference';
  CART_PostTimer = 'CART PostTimer';
  CART_URL = 'CART URL';
  CART_Reserved = 'CART Reserved';
  CART_TagText = 'CART TagText';

Const
  // BASS_ChannelGetTags types : what's returned
  BASS_TAG_ID3 = 0; // ID3v1 tags : TAG_ID3 structure
  BASS_TAG_ID3V2 = 1; // ID3v2 tags : variable length block
  BASS_TAG_OGG = 2; // OGG comments : series of null-terminated UTF-8 strings
  BASS_TAG_HTTP = 3; // HTTP headers : series of null-terminated ANSI strings
  BASS_TAG_ICY = 4; // ICY headers : series of null-terminated ANSI strings
  BASS_TAG_META = 5; // ICY metadata : ANSI string
  BASS_TAG_APE = 6; // APEv2 tags : series of null-terminated UTF-8 strings
  BASS_TAG_MP4 = 7;
  // MP4/iTunes metadata : series of null-terminated UTF-8 strings
  BASS_TAG_VENDOR = 9; // OGG encoder : UTF-8 string
  BASS_TAG_LYRICS3 = 10; // Lyric3v2 tag : ASCII string
  BASS_TAG_CA_CODEC = 11; // CoreAudio codec info : TAG_CA_CODEC structure
  BASS_TAG_MF = 13;
  // Media Foundation tags : series of null-terminated UTF-8 strings
  BASS_TAG_WAVEFORMAT = 14; // WAVE format : WAVEFORMATEEX structure
  BASS_TAG_RIFF_INFO = $100;
  // RIFF "INFO" tags : series of null-terminated ANSI strings
  BASS_TAG_RIFF_BEXT = $101; // RIFF/BWF "bext" tags : TAG_BEXT structure
  BASS_TAG_RIFF_CART = $102; // RIFF/BWF "cart" tags : TAG_CART structure
  BASS_TAG_RIFF_DISP = $103; // RIFF "DISP" text tag : ANSI string
  BASS_TAG_APE_BINARY = $1000;
  // + index #, binary APEv2 tag : TAG_APE_BINARY structure
  BASS_TAG_MUSIC_NAME = $10000; // MOD music name : ANSI string
  BASS_TAG_MUSIC_MESSAGE = $10001; // MOD message : ANSI string
  BASS_TAG_MUSIC_ORDERS = $10002;
  // MOD order list : BYTE array of pattern numbers
  BASS_TAG_MUSIC_INST = $10100;
  // + instrument #, MOD instrument name : ANSI string
  BASS_TAG_MUSIC_SAMPLE = $10300; // + sample #, MOD sample name : ANSI string
  BASS_TAG_WMA = 8; // WMA header tags : series of null-terminated UTF-8 strings
{$IFDEF MSWINDOWS}
  bassdll = 'bass.dll';
{$ENDIF}
{$IFDEF LINUX}
  bassdll = 'libbass.so';
{$ENDIF}
{$IFDEF MACOS}
  bassdll = 'libbass.dylib';
{$ENDIF}
{$IFDEF ANDROID}
  bassdll = 'libbass.so';
{$ENDIF}
{$IFDEF IOS}
  bassdll = 'libbass.dylib';
{$ENDIF}

Const
  MAGIC_PNG = $5089; // * Little endian form
  MAGIC_JPG = $D8FF; // * Little endian form
  MAGIC_GIF = $4947; // * Little endian form
  MAGIC_BMP = $4D42; // * Little endian form

Const
  DEFAULT_UPPERCASE_FIELD_NAMES = True;
{$ENDREGION}

type
  DWord = Cardinal;

type
  TTagType = (ttNone, ttAutomatic, ttAPEv2, ttFlac, ttID3v1, ttID3v2, ttMP4,
    ttOpusVorbis, ttWAV, ttWMA);

  TTagTypes = set of TTagType;

type
  TTagPictureFormat = (tpfUnknown, tpfJPEG, tpfPNG, tpfBMP, tpfGIF);

type
  TTagPriority = Array [0 .. 8] of TTagType;

type
  TExtTagType = (ettUnknown, ettTXXX, ettWXXX);

type
  TTagsBase = class;

  TTag = class
  private
  public
    Name: String;
    Value: String;
    Language: String;
    Description: String;
    ExtTagType: TExtTagType;
    Parent: TTagsBase;
    Index: Integer;
    Constructor Create(Parent: TTagsBase);
    Destructor Destroy; override;
    function GetAsList(var List: TStrings): Boolean;
    function SetAsList(List: TStrings): Boolean;
    procedure Clear;
    function Delete: Boolean;
    procedure Remove;
    function Assign(Tag: TTag): Boolean;
  end;

  TTagArray = array of TTag;

  TCoverArt = class
  private
  public
    Name: String;
    Stream: TStream;
    Description: String;
    CoverType: Cardinal;
    MIMEType: String;
    PictureFormat: TTagPictureFormat;
    Width: Cardinal;
    Height: Cardinal;
    ColorDepth: Cardinal;
    NoOfColors: Cardinal;
    Parent: TTagsBase;
    Index: Integer;
    Constructor Create(Parent: TTagsBase);
    Destructor Destroy; override;
    procedure Clear;
    procedure Delete;
    function Assign(CoverArt: TCoverArt): Boolean;
  end;

  TSourceAudioAttributes = class
  private
    function GetChannels: Word;
    function GetSamplesPerSec: DWord;
    function GetBitsPerSample: Word;
    function GetPlayTime: Double;
    function GetSampleCount: Int64;
    function GetBitRate: Integer;
  public
    Parent: TTagsBase;
    Constructor Create(Parent: TTagsBase);
    property Channels: Word read GetChannels;
    property SamplesPerSec: DWord read GetSamplesPerSec;
    property BitsPerSample: Word read GetBitsPerSample;
    property PlayTime: Double read GetPlayTime;
    property SampleCount: Int64 read GetSampleCount;
    property BitRate: Integer read GetBitRate;
  end;

  ITags = Interface
    function LoadFromFile(FileName: String): Integer;
    function LoadFromBASS(Channel: Cardinal): Integer;
    function SaveToFile(FileName: String): Integer;
    function Delete(Index: Integer): Boolean;
  end;

  TTagsBase = class
  private
  protected
    function GetSize: Int64; virtual; abstract;
  public
    FileName: String;  // Probably it should be removed to another class or it should be made protected
    Loaded: Boolean;
    Tags: Array of TTag;
    CoverArts: Array of TCoverArt;
    function LoadFromFile(FileName: String): Integer; virtual; abstract;
    function LoadFromBASS(Channel: Cardinal): Integer; virtual; abstract;
    function SaveToFile: Integer; overload;
    function SaveToFile(FileName: String): Integer; overload; virtual; abstract;
    function Add(Name: String): TTag; virtual;
    function Tag(Name: String): TTag;
    function GetTag(Name: String): String;
    function SetTag(Name: String; Text: String): Integer;
    function SetList(Name: String; List: TStrings): Integer;
    function Delete(Index: Integer): Boolean; overload; virtual; abstract;
    function Delete(Name: String): Boolean; overload;
    function Remove(Index: Integer): Boolean;
    procedure DeleteAllTags;
    procedure Clear; virtual;
    function Count: Integer;
    function Exists(Name: String): Integer; overload;
    function TypeCount(Name: String): Integer;
    procedure RemoveEmptyTags;
    function CoverArtCount: Integer;
    function CoverArt(Name: String): TCoverArt;
    function AddCoverArt(Name: String): TCoverArt; overload; virtual;
    function AddCoverArt(Name: String; Stream: TStream; MIMEType: String)
      : TCoverArt; overload; virtual;
    function AddCoverArt(Name: String; FileName: String): TCoverArt;
      overload; virtual;
    function DeleteCoverArt(Index: Integer): Boolean;
    procedure DeleteAllCoverArts;
    function Assign(Source: TTagsBase): Boolean; virtual;
    function PictureStreamType(PictureStream: TStream): TTagPictureFormat;
    function PicturePointerType(Picture: Pointer): TTagPictureFormat;
    function GetTypeString: String; virtual;
    property Size: Int64 read GetSize;
    { property FileName: String read FileName;
      property Loaded: Boolean read mLoaded;
      property Tags: Array of TTag read mTags;
      property CoverArts: Array of TCoverArt read mCoverArts; }
  end;

function PictureFormatFromMIMEType(MIMEType: String): TTagPictureFormat;
function TagTypeFromFileName(FileName: String): TTagTypes;

implementation

{$REGION 'TTag'}

Constructor TTag.Create(Parent: TTagsBase);
begin
  Inherited Create;
  Self.Parent := Parent;
end;

Destructor TTag.Destroy;
begin
  Inherited;
end;

function TTag.SetAsList(List: TStrings): Boolean;
var
  i: Integer;
begin
  // if Format <> ffText then begin
  // Exit;
  // end;
  Self.Value := '';
  for i := 0 to List.Count - 1 do
  begin
    Self.Value := Self.Value + List.Names[i] + #13#10 + List.ValueFromIndex
      [i] + #13#10;
  end;
  Result := True;
end;

function TTag.GetAsList(var List: TStrings): Boolean;
var
  TempList: TStrings;
  i: Integer;
begin
  // Result := False;
  List.Clear;
  {
    if Format <> ffText then begin
    Exit;
    end;
  }
  TempList := TStringList.Create;
  try
    TempList.Text := Self.Value;
    for i := 0 to (TempList.Count - 1) div 2 do
    begin
      List.Append(TempList[i * 2] + '=' + TempList[i * 2 + 1]);
    end;
  finally
    FreeAndNil(TempList);
  end;
  Result := True;
end;

procedure TTag.Remove;
begin
  Parent.Remove(Index);
end;

procedure TTag.Clear;
begin
  Self.Name := '';
  Self.Value := '';
  Self.Language := '';
  Self.Description := '';
end;

function TTag.Delete: Boolean;
begin
  Result := Parent.Delete(Index);
end;

function TTag.Assign(Tag: TTag): Boolean;
begin
  Self.Clear;
  if Tag <> nil then
  begin
    Self.Name := Tag.Name;
    Self.Value := Tag.Value;
    Self.Language := Tag.Language;
    Self.Description := Tag.Description;
  end;
  Result := True;
end;
{$ENDREGION}
{$REGION 'TCoverArt'}

function TCoverArt.Assign(CoverArt: TCoverArt): Boolean;
begin
  Self.Clear;
  if CoverArt <> nil then
  begin
    Self.Name := CoverArt.Name;
    TMemoryStream(Stream).Clear;
    Stream.CopyFrom(CoverArt.Stream, 0);
    Self.CoverType := CoverArt.CoverType;
    Self.Description := CoverArt.Description;
    Self.MIMEType := CoverArt.MIMEType;
    Self.PictureFormat := CoverArt.PictureFormat;
    Self.Width := CoverArt.Width;
    Self.Height := CoverArt.Height;
    Self.ColorDepth := CoverArt.ColorDepth;
    Self.NoOfColors := CoverArt.NoOfColors;
  end;
  Result := True;
end;

procedure TCoverArt.Clear;
begin
  Self.Name := '';
  TMemoryStream(Stream).Clear;
  Self.CoverType := 0;
  Self.Description := '';
  Self.MIMEType := '';
  Self.PictureFormat := tpfUnknown;
  Self.Width := 0;
  Self.Height := 0;
  Self.ColorDepth := 0;
  Self.NoOfColors := 0;
end;

constructor TCoverArt.Create(Parent: TTagsBase);
begin
  inherited Create;
  Self.Parent := Parent;
  Stream := TMemoryStream.Create;
end;

procedure TCoverArt.Delete;
begin
  Parent.DeleteCoverArt(Index);
end;

destructor TCoverArt.Destroy;
begin
  FreeAndNil(Stream);
  inherited;
end;
{$ENDREGION}
{$REGION 'TSourceAudioAttributes'}

constructor TSourceAudioAttributes.Create(Parent: TTagsBase);
begin
  inherited Create;
  Self.Parent := Parent;
end;

function TSourceAudioAttributes.GetBitRate: Integer;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.BitRate <> 0 then begin
    Result := Parent.FlacTag.BitRate;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.BitRate <> 0 then begin
    Result := Parent.ID3v2Tag.BitRate;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.BitRate <> 0 then begin
    Result := Parent.ID3v2Tag.BitRate;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.BitRate <> 0 then begin
    Result := Parent.ID3v2Tag.BitRate;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.BitRate <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.BitRate;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Format = ofOpus then begin
    if Parent.OggVorbisAndOpusTag.Info.BitRate <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.BitRate;
    end;
    end;
    if Parent.OggVorbisAndOpusTag.Format = ofVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.VorbisParameters.BitRateNominal <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.VorbisParameters.BitRateNominal;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.BitRate <> 0 then begin
    Result := Parent.WAVTag.BitRate;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.BitRate <> 0 then begin
    Result := Parent.WMATag.BitRate;
    end;
    end;
    {$ENDIF}
    end; *)
end;

function TSourceAudioAttributes.GetBitsPerSample: Word;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.BitsPerSample <> 0 then begin
    Result := Parent.FlacTag.BitsPerSample;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.MPEGInfo.SampleRate <> 0 then begin
    Result := 32;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.WAVInfo.BitsPerSample <> 0 then begin
    Result := Parent.ID3v2Tag.WAVInfo.BitsPerSample;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.AIFFInfo.SampleSize <> 0 then begin
    Result := Round(Parent.ID3v2Tag.AIFFInfo.SampleSize);
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.BitsPerSample <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.BitsPerSample;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Format = ofOpus then begin
    if Parent.OggVorbisAndOpusTag.Info.OpusParameters.ChannelCount <> 0 then begin
    Result := 32;
    end;
    end;
    if Parent.OggVorbisAndOpusTag.Format = ofVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.VorbisParameters.ChannelMode <> 0 then begin
    Result := 32;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.Attributes.BitsPerSample <> 0 then begin
    Result := Parent.WAVTag.Attributes.BitsPerSample;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.Loaded then begin
    Result := 32;
    end;
    end;
    {$ENDIF}
    end; *)
end;

function TSourceAudioAttributes.GetChannels: Word;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.Channels <> 0 then begin
    Result := Parent.FlacTag.Channels;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.MPEGInfo.ChannelMode <> tmpegcmUnknown then begin
    case Parent.ID3v2Tag.MPEGInfo.ChannelMode of
    tmpegcmUnknown: Result := 0;
    tmpegcmMono: Result := 1;
    tmpegcmDualChannel: Result := 2;
    tmpegcmJointStereo: Result := 2;
    tmpegcmStereo: Result := 2;
    end;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.WAVInfo.Channels <> 0 then begin
    Result := Parent.ID3v2Tag.WAVInfo.Channels;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.AIFFInfo.Channels <> 0 then begin
    Result := Round(Parent.ID3v2Tag.AIFFInfo.Channels);
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.ChannelNumber <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.ChannelNumber;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Format = ofOpus then begin
    if Parent.OggVorbisAndOpusTag.Info.OpusParameters.ChannelCount <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.OpusParameters.ChannelCount;
    end;
    end;
    if Parent.OggVorbisAndOpusTag.Format = ofVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.VorbisParameters.ChannelMode <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.VorbisParameters.ChannelMode;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.Attributes.Channels <> 0 then begin
    Result := Parent.WAVTag.Attributes.Channels;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.Loaded then begin
    Result := 0; //* Not implemented
    end;
    end;
    {$ENDIF}
    end; *)
end;

function TSourceAudioAttributes.GetPlayTime: Double;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.Channels <> 0 then begin
    Result := Parent.FlacTag.PlayTime;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.PlayTime <> 0 then begin
    Result := Parent.ID3v2Tag.PlayTime;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.PlayTime <> 0 then begin
    Result := Parent.ID3v2Tag.PlayTime;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.PlayTime <> 0 then begin
    Result := Round(Parent.ID3v2Tag.PlayTime);
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.PlayTime <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.PlayTime;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.PlayTime <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.PlayTime;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.PlayTime <> 0 then begin
    Result := Parent.WAVTag.PlayTime;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.Duration <> 0 then begin
    Result := Parent.WMATag.Duration / 1000;
    end;
    end;
    {$ENDIF}
    end; *)
end;

function TSourceAudioAttributes.GetSampleCount: Int64;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.SampleCount <> 0 then begin
    Result := Parent.FlacTag.SampleCount;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.SampleCount <> 0 then begin
    Result := Parent.ID3v2Tag.SampleCount;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.SampleCount <> 0 then begin
    Result := Parent.ID3v2Tag.SampleCount;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.AIFFInfo.SampleFrames <> 0 then begin
    Result := Round(Parent.ID3v2Tag.AIFFInfo.SampleFrames);
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.SampleCount <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.SampleCount;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.SampleCount <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.SampleCount;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.SampleCount <> 0 then begin
    Result := Parent.WAVTag.SampleCount;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.NumberOfFrames <> 0 then begin
    Result := Parent.WMATag.NumberOfFrames;
    end;
    end;
    {$ENDIF}
    end; *)
end;

function TSourceAudioAttributes.GetSamplesPerSec: DWord;
var
  i: Integer;
begin
  Result := 0;
  (* for i := High(Parent.TagLoadPriority) downto Low(Parent.TagLoadPriority) do begin
    if Parent.TagLoadPriority[i] = TagsLibrary.ttFlac then begin
    if Parent.FlacTag.SampleRate <> 0 then begin
    Result := Parent.FlacTag.SampleRate;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttID3v2 then begin
    if Parent.ID3v2Tag.SourceFileType = sftMPEG then begin
    if Parent.ID3v2Tag.MPEGInfo.SampleRate <> 0 then begin
    Result := Parent.ID3v2Tag.MPEGInfo.SampleRate;
    end;
    end;
    if (Parent.ID3v2Tag.SourceFileType = sftWAVE)
    OR (Parent.ID3v2Tag.SourceFileType = sftRF64)
    then begin
    if Parent.ID3v2Tag.WAVInfo.SamplesPerSec <> 0 then begin
    Result := Parent.ID3v2Tag.WAVInfo.SamplesPerSec;
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftAIFF then begin
    if Parent.ID3v2Tag.AIFFInfo.SampleRate <> 0 then begin
    Result := Round(Parent.ID3v2Tag.AIFFInfo.SampleRate);
    end;
    end;
    if Parent.ID3v2Tag.SourceFileType = sftDSF then begin
    if Parent.ID3v2Tag.DSFInfo.SamplingFrequency <> 0 then begin
    Result := Parent.ID3v2Tag.DSFInfo.SamplingFrequency;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttOpusVorbis then begin
    if Parent.OggVorbisAndOpusTag.Format = ofOpus then begin
    if Parent.OggVorbisAndOpusTag.Info.OpusParameters.SampleRate <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.OpusParameters.SampleRate;
    end;
    end;
    if Parent.OggVorbisAndOpusTag.Format = ofVorbis then begin
    if Parent.OggVorbisAndOpusTag.Info.VorbisParameters.SampleRate <> 0 then begin
    Result := Parent.OggVorbisAndOpusTag.Info.VorbisParameters.SampleRate;
    end;
    end;
    end;
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWAV then begin
    if Parent.WAVTag.Attributes.SamplesPerSec <> 0 then begin
    Result := Parent.WAVTag.Attributes.SamplesPerSec;
    end;
    end;
    {$IFDEF MSWINDOWS}
    if Parent.TagLoadPriority[i] = TagsLibrary.ttWMA then begin
    if Parent.WMATag.Loaded then begin
    Result := 0; //* Not implemented
    end;
    end;
    {$ENDIF}
    end; *)
end;

{$ENDREGION}
{$REGION 'TTagsBase'}

function TTagsBase.Add(Name: String): TTag;
begin
  Result := nil;
  try
    SetLength(Tags, Length(Tags) + 1);
    Tags[Length(Tags) - 1] := TTag.Create(Self);
    Tags[Length(Tags) - 1].Name := Name;
    Tags[Length(Tags) - 1].Index := Length(Tags) - 1;
    Result := Tags[Length(Tags) - 1];
  except
    // *
  end;
end;

function TTagsBase.Delete(Name: String): Boolean;
var
  Tag: TTag;
begin
  Result := False;
  Tag := Self.Tag(Name);
  if Assigned(Tag) then
  begin
    Result := Tag.Delete;
  end;
end;

procedure TTagsBase.DeleteAllTags;
var
  i: Integer;
begin
  for i := 0 to Length(Tags) - 1 do
  begin
    FreeAndNil(Tags[i]);
  end;
  SetLength(Tags, 0);
end;

function TTagsBase.DeleteCoverArt(Index: Integer): Boolean;
var
  i: Integer;
  j: Integer;
begin
  Result := False;
  if (Index >= Length(CoverArts)) OR (Index < 0) then
  begin
    Exit;
  end;
  FreeAndNil(CoverArts[Index]);
  i := 0;
  j := 0;
  while i <= Length(CoverArts) - 1 do
  begin
    if CoverArts[i] <> nil then
    begin
      CoverArts[j] := CoverArts[i];
      CoverArts[j].Index := j;
      Inc(j);
    end;
    Inc(i);
  end;
  SetLength(CoverArts, j);
  Result := True;
end;

procedure TTagsBase.DeleteAllCoverArts;
var
  i: Integer;
begin
  for i := 0 to Length(CoverArts) - 1 do
  begin
    FreeAndNil(CoverArts[i]);
  end;
  SetLength(CoverArts, 0);
end;

function TTagsBase.Exists(Name: String): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to Length(Tags) - 1 do
  begin
    if SameText(Name, Tags[i].Name) then
    begin
      Result := i;
      Break;
    end;
  end;
end;

function TTagsBase.Tag(Name: String): TTag;
var
  Index: Integer;
begin
  Result := nil;
  Index := Exists(Name);
  if Index > -1 then
  begin
    Result := Tags[Index];
  end;
end;

function TTagsBase.TypeCount(Name: String): Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Length(Tags) - 1 do
  begin
    if SameText(Name, Tags[i].Name) then
    begin
      Inc(Result);
    end;
  end;
end;

function TTagsBase.GetTag(Name: String): String;
var
  Index: Integer;
begin
  Result := '';
  Index := Exists(Name);
  if Index > -1 then
  begin
    Result := Tags[Index].Value;
  end;
end;

procedure TTagsBase.Clear;
begin
  DeleteAllTags;
  DeleteAllCoverArts;
  FileName := '';
  Loaded := False;
end;

function TTagsBase.Count: Integer;
begin
  Result := Length(Tags);
end;

function TTagsBase.CoverArtCount: Integer;
begin
  Result := Length(CoverArts);
end;

function TTagsBase.CoverArt(Name: String): TCoverArt;
var
  i: Integer;
begin
  Result := nil;
  for i := 0 to Length(CoverArts) - 1 do
  begin
    if SameText(Name, CoverArts[i].Name) then
    begin
      Result := CoverArts[i];
      Break;
    end;
  end;
end;

function TTagsBase.SetTag(Name: String; Text: String): Integer;
var
  i: Integer;
  l: Integer;
begin
  i := 0;
  l := Length(Tags);
  while (i < l) AND (NOT SameText(Tags[i].Name, Name)) do
  begin
    Inc(i);
  end;
  if i = l then
  begin
    with Add(Name) do
    begin
      Value := Text;
      Result := Index;
    end;
  end
  else
  begin
    Tags[i].Value := Text;
    Result := i;
  end;
end;

function TTagsBase.SetList(Name: String; List: TStrings): Integer;
var
  i: Integer;
  l: Integer;
begin
  i := 0;
  l := Length(Tags);
  while (i < l) AND SameText(Tags[i].Name, Name) do
  begin
    Inc(i);
  end;
  if i = l then
  begin
    with Add(Name) do
    begin
      SetAsList(List);
      Result := Index;
    end;
  end
  else
  begin
    Tags[i].SetAsList(List);
    Result := i;
  end;
end;

function TTagsBase.Remove(Index: Integer): Boolean;
var
  i, j: Integer;
begin
  Result := False;
  if (Index >= Length(Tags)) OR (Index < 0) then
  begin
    Exit;
  end;
  // * Do the delete from array
  FreeAndNil(Tags[Index]);
  i := 0;
  j := 0;
  while i <= Length(Tags) - 1 do
  begin
    if Tags[i] <> nil then
    begin
      Tags[j] := Tags[i];
      Tags[j].Index := j;
      Inc(j);
    end;
    Inc(i);
  end;
  SetLength(Tags, j);
  Result := True;
end;

procedure TTagsBase.RemoveEmptyTags;
var
  i: Integer;
begin
  for i := Length(Tags) - 1 downto 0 do
  begin
    if Tags[i].Value = '' then
    begin
      Delete(i);
    end;
  end;
end;

function TTagsBase.AddCoverArt(Name: String): TCoverArt;
begin
  Result := nil;
  try
    SetLength(CoverArts, Length(CoverArts) + 1);
    CoverArts[Length(CoverArts) - 1] := TCoverArt.Create(Self);
    CoverArts[Length(CoverArts) - 1].Name := Name;
    CoverArts[Length(CoverArts) - 1].Description := 'No description';
    CoverArts[Length(CoverArts) - 1].PictureFormat := tpfUnknown;
    CoverArts[Length(CoverArts) - 1].Index := Length(CoverArts) - 1;
    Result := CoverArts[Length(CoverArts) - 1];
  except
    // *
  end;
end;

function TTagsBase.AddCoverArt(Name: String; Stream: TStream; MIMEType: String)
  : TCoverArt;
var
  CoverArt: TCoverArt;
begin
  Result := nil;
  CoverArt := AddCoverArt(Name);
  if Assigned(CoverArt) then
  begin
    CoverArt.MIMEType := MIMEType;
    CoverArt.PictureFormat := PictureFormatFromMIMEType(MIMEType);
    CoverArt.Stream.CopyFrom(Stream, 0);
    Result := CoverArt;
  end;
end;

function TTagsBase.AddCoverArt(Name, FileName: String): TCoverArt;
var
  PictureStream: TFileStream;
  PictureMagic: Word;
  MIMEType: String;
begin
  Result := nil;
  if FileExists(FileName) then
  begin
    try
      PictureStream := TFileStream.Create(FileName, fmOpenRead);
      try
        PictureStream.Seek(0, soBeginning);
        PictureStream.Read(PictureMagic, 2);
        PictureStream.Seek(0, soBeginning);
        if PictureMagic = MAGIC_JPG then
        begin
          MIMEType := 'image/jpeg';
          {
            JPEGPicture := TJPEGImage.Create;
            try
            JPEGPicture.LoadFromStream(PictureStream);
            Width := JPEGPicture.Width;
            Height := JPEGPicture.Height;
            NoOfColors := 0;
            ColorDepth := 24;
            finally
            FreeAndNil(JPEGPicture);
            end;
          }
        end;
        if PictureMagic = MAGIC_PNG then
        begin
          MIMEType := 'image/png';
          {
            PNGPicture := TPNGImage.Create;
            try
            PNGPicture.LoadFromStream(PictureStream);
            Width := PNGPicture.Width;
            Height := PNGPicture.Height;
            NoOfColors := 0;
            ColorDepth := PNGPicture.PixelInformation.Header.BitDepth;
            finally
            FreeAndNil(PNGPicture);
            end;
          }
        end;
        if PictureMagic = MAGIC_GIF then
        begin
          MIMEType := 'image/gif';
          {
            GIFPicture := TGIFImage.Create;
            try
            GIFPicture.LoadFromStream(PictureStream);
            Width := GIFPicture.Width;
            Height := GIFPicture.Height;
            NoOfColors := 0;   //GIFPicture.ColorResolution
            ColorDepth := GIFPicture.BitsPerPixel;
            finally
            FreeAndNil(GIFPicture);
            end;
          }
        end;
        if PictureMagic = MAGIC_BMP then
        begin
          MIMEType := 'image/bmp';
          {
            BMPPicture := TBitmap.Create;
            try
            BMPPicture.LoadFromStream(PictureStream);
            Width := BMPPicture.Width;
            Height := BMPPicture.Height;
            NoOfColors := 0;
            case BMPPicture.PixelFormat of
            pfDevice: ColorDepth := 32;
            pf1bit: ColorDepth := 1;
            pf4bit: ColorDepth := 4;
            pf8bit: ColorDepth := 8;
            pf15bit: ColorDepth := 15;
            pf16bit: ColorDepth := 16;
            pf24bit: ColorDepth := 24;
            pf32bit: ColorDepth := 32;
            pfCustom: ColorDepth := 32;
            end;
            finally
            FreeAndNil(BMPPicture);
            end;
          }
        end;
      finally
        PictureStream.Seek(0, soBeginning);
        Result := AddCoverArt(Name, PictureStream, MIMEType);
      end;
    finally
      FreeAndNil(PictureStream);
    end;
  end;
  if Assigned(Result) then
  begin
    Result.Description := ExtractFileName(FileName);
  end;
end;

function TTagsBase.PictureStreamType(PictureStream: TStream): TTagPictureFormat;
var
  Magic: Word;
begin
  Result := tpfUnknown;
  if NOT Assigned(PictureStream) OR (PictureStream.Size = 0) then
  begin
    Exit;
  end;
  try
    PictureStream.Seek(0, soBeginning);
    try
      PictureStream.Read(Magic, SizeOf(Magic));
      case Magic of
        MAGIC_PNG:
          Result := tpfPNG;
        MAGIC_JPG:
          Result := tpfJPEG;
        MAGIC_GIF:
          Result := tpfGIF;
        MAGIC_BMP:
          Result := tpfBMP;
      end;
    finally
      PictureStream.Seek(0, soBeginning);
    end;
  except
    Result := tpfUnknown;
  end;
end;

function TTagsBase.PicturePointerType(Picture: Pointer): TTagPictureFormat;
var
  Magic: Word;
begin
  Result := tpfUnknown;
  if NOT Assigned(Picture) then
  begin
    Exit;
  end;
  try
    Magic := PWord(Picture)^;
    case Magic of
      MAGIC_PNG:
        Result := tpfPNG;
      MAGIC_JPG:
        Result := tpfJPEG;
      MAGIC_GIF:
        Result := tpfGIF;
      MAGIC_BMP:
        Result := tpfBMP;
    end;
  except
    Result := tpfUnknown;
  end;
end;

function TTagsBase.SaveToFile: Integer;
begin
  Result := SaveToFile(FileName);
end;

function TTagsBase.GetTypeString: String;
begin
  Result := 'TagsLibrary.TagsBase';
end;

function TTagsBase.Assign(Source: TTagsBase): Boolean;
var i: Integer;
begin
    Clear;
    try
      for I := Low(Source.Tags) to High(Source.Tags) do
      begin
        SetTag(Source.Tags[i].Name, Source.Tags[i].Value);
      end;
    except
      Result:=False;
    end;
end;
{$ENDREGION}

function PictureFormatFromMIMEType(MIMEType: String): TTagPictureFormat;
begin
  Result := tpfUnknown;
  if SameText(MIMEType, 'image/jpeg') OR SameText(MIMEType, 'image/jpg') then
  begin
    Result := tpfJPEG;
  end;
  if SameText(MIMEType, 'image/png') then
  begin
    Result := tpfPNG;
  end;
  if SameText(MIMEType, 'image/bmp') then
  begin
    Result := tpfBMP;
  end;
  if SameText(MIMEType, 'image/gif') then
  begin
    Result := tpfGIF;
  end;
end;

function TagTypeFromFileName(FileName: String): TTagTypes;
var
  Fext: String;
begin
  Result := [ttAPEv2];
  Fext := UpperCase(ExtractFileExt(FileName));
  if (Fext = '.WAV') OR (Fext = '.WAVE') OR (Fext = '.RF64') OR (Fext = '.BWF')
  then
  begin
    Result := [ttWAV, ttID3v2];
  end;
  if (Fext = '.APE') OR (Fext = '.MPC') OR (Fext = '.WV') OR (Fext = '.OFR')
  then
  begin
    Result := [ttAPEv2];
  end;
  if (Fext = '.MP4') OR (Fext = '.M4A') OR (Fext = '.M4B') then
  begin
    Result := [ttMP4];
  end;
  if (Fext = '.WMA') OR (Fext = '.WMV') OR (Fext = '.ASF') then
  begin
    Result := [ttWMA];
  end;
  if (Fext = '.FLAC') OR (Fext = '.FLA') OR (Fext = '.FLC') OR (Fext = '.OGA')
  then
  begin
    Result := [ttFlac];
  end;
  if (Fext = '.OGG') OR (Fext = '.OPUS') then
  begin
    Result := [ttOpusVorbis];
  end;
  // * MPEG
  if (Fext = '.MPG') OR (Fext = '.MP1') OR (Fext = '.MP2') OR (Fext = '.MP3') OR
    (Fext = '.MPA')
  // * AIFF
    OR (Fext = '.AIFF') OR (Fext = '.AIF') OR (Fext = '.AIFC') OR
    (Fext = '.AFC')
  // * DSD DSF
    OR (Fext = '.DSF') then
  begin
    Result := [ttID3v2];
  end;
end;

Initialization

End.
