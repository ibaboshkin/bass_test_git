unit ExportDecl;

interface

uses
  TagsLibExports;

exports Tag_get_Name;
exports Tag_get_Value;
exports Tag_get_Language;
exports Tag_get_Description;
exports Tag_get_ExtTagType;
exports Tag_get_Parent;
exports Tag_get_Index;
exports Tag_set_Name;
exports Tag_set_Value;
exports Tag_set_Language;
exports Tag_set_Description;
exports Tag_set_ExtTagType;
exports Tag_set_Parent;
exports Tag_set_Index;
exports Tag_Create;
exports Tag_Destroy;
exports Tag_Clear;
exports Tag_Delete;
exports Tag_Remove;
exports Tag_Assign;

exports CoverArt_get_Name;
exports CoverArt_get_Stream;
exports CoverArt_get_Description;
exports CoverArt_get_CoverType;
exports CoverArt_get_MIMEType;
exports CoverArt_get_PictureFormat;
exports CoverArt_get_Width;
exports CoverArt_get_Height;
exports CoverArt_get_ColorDepth;
exports CoverArt_get_NoOfColors;
exports CoverArt_get_Parent;
exports CoverArt_get_Index;

exports CoverArt_set_Name;
exports CoverArt_set_Stream;
exports CoverArt_set_Description;
exports CoverArt_set_CoverType;
exports CoverArt_set_MIMEType;
exports CoverArt_set_PictureFormat;
exports CoverArt_set_Width;
exports CoverArt_set_Height;
exports CoverArt_set_ColorDepth;
exports CoverArt_set_NoOfColors;
exports CoverArt_set_Parent;
exports CoverArt_set_Index;

exports CoverArt_Create;
exports CoverArt_Destroy;
exports CoverArt_Clear;
exports CoverArt_Delete;
exports CoverArt_Assign;

exports TagsBase_get_Loaded;
exports TagsBase_get_Tags;
exports TagsBase_get_CoverArts;
exports TagsBase_get_Size;

exports TagsBase_set_Loaded;
exports TagsBase_set_Tags;
exports TagsBase_set_CoverArts;

exports TagsBase_Create;
exports TagsBase_Destroy;
exports TagsBase_LoadFromFile;
exports TagsBase_LoadFromBASS;
exports TagsBase_SaveToFile1;
exports TagsBase_SaveToFile2;
exports TagsBase_Add;
exports TagsBase_Tag;
exports TagsBase_GetTag;
exports TagsBase_SetTag;
exports TagsBase_Delete;
exports TagsBase_Delete2;
exports TagsBase_Remove;
exports TagsBase_DeleteAllTags;
exports TagsBase_Clear;
exports TagsBase_Count;
exports TagsBase_Exists;
exports TagsBase_TypeCount;
exports TagsBase_RemoveEmptyTags;
exports TagsBase_CoverArtCount;
exports TagsBase_CoverArt;
exports TagsBase_AddCoverArt;
exports TagsBase_AddCoverArt2;
exports TagsBase_AddCoverArt3;
exports TagsBase_DeleteCoverArt;
exports TagsBase_DeleteAllCoverArts;
exports TagsBase_Assign;
exports TagsBase_PictureStreamType;
exports TagsBase_PicturePointerType;
exports TagsBase_GetTypeString;

exports ID3v1Tag_Create;
exports ID3v1Tag_Destroy;
exports ID3v1Tag_SetTitle;
exports ID3v1Tag_SetArtist;
exports ID3v1Tag_SetAlbum;
exports ID3v1Tag_SetYear;
exports ID3v1Tag_SetComment;
exports ID3v1Tag_SetTrack;
exports ID3v1Tag_SetGenre;

exports ID3v1Tag_GetTite;
exports ID3v1Tag_GetArtist;
exports ID3v1Tag_GetAlbum;
exports ID3v1Tag_GetYear;
exports ID3v1Tag_GetComment;
exports ID3v1Tag_GetTrack;
exports ID3v1Tag_GetGenre;

exports ID3v1Tag_Assign1;
exports ID3v1Tag_Assign2;
exports ID3v1Tag_LoadFromFile;
exports ID3v1Tag_LoadFromBASS;
exports ID3v1Tag_SaveToFile1;
exports ID3v1Tag_SaveToFile2;
exports ID3v1Tag_Clear;
exports ID3v1Tag_GetTypeString;

exports ID3v1TagToTagsBase;
exports TagsBaseToID3v1Tag;
implementation

end.
