unit TagsLibExports;

interface

Uses SysUtils,
  Classes,
  ID3v1Library,
  CommonTypes,
  Logger;
// TagsLibrary;

type
  TTagArray = Array of TTag;
  PTagArray = ^TTagArray;
  TCoverArtArray = Array of TCoverArt;
  PCoverArt = ^TCoverArtArray;

{$REGION 'Tag Declarations'}
function Tag_get_Name(Tag: TTag): PChar; export; stdcall;
function Tag_get_Value(Tag: TTag): PChar; export; stdcall;
function Tag_get_Language(Tag: TTag): PChar; export; stdcall;
function Tag_get_Description(Tag: TTag): PChar; export; stdcall;
function Tag_get_ExtTagType(Tag: TTag): TExtTagType; export; stdcall;
function Tag_get_Parent(Tag: TTag): TTagsBase; export; stdcall;
function Tag_get_Index(Tag: TTag): Integer; export; stdcall;

procedure Tag_set_Name(Tag: TTag; Name: PChar); export; stdcall;
procedure Tag_set_Value(Tag: TTag; Value: PChar); export; stdcall;
procedure Tag_set_Language(Tag: TTag; Language: PChar); export; stdcall;
procedure Tag_set_Description(Tag: TTag; Description: PChar); export; stdcall;
procedure Tag_set_ExtTagType(Tag: TTag; ExtTagType: TExtTagType);
  export; stdcall;
procedure Tag_set_Parent(Tag: TTag; Parent: TTagsBase); export; stdcall;
procedure Tag_set_Index(Tag: TTag; Index: Integer); export; stdcall;

function Tag_Create(Parent: TTagsBase): TTag; export; stdcall;
procedure Tag_Destroy(Tag: TTag); export; stdcall;
procedure Tag_Clear(Tag: TTag); export; stdcall;
function Tag_Delete(Tag: TTag): Boolean; export; stdcall;
procedure Tag_Remove(Tag: TTag); export; stdcall;
function Tag_Assign(this: TTag; Tag: TTag): Boolean; export; stdcall;
{$ENDREGION}
{$REGION 'CoverArt Declarations'}
function CoverArt_get_Name(CoverArt: TCoverArt): PChar; export; stdcall;
procedure CoverArt_get_Stream(CoverArt: TCoverArt; var buffer: PByte;
  var size: Int64); export; stdcall;
function CoverArt_get_Description(CoverArt: TCoverArt): PChar; export; stdcall;
function CoverArt_get_CoverType(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_MIMEType(CoverArt: TCoverArt): PChar; export; stdcall;
function CoverArt_get_PictureFormat(CoverArt: TCoverArt): TTagPictureFormat;
  export; stdcall;
function CoverArt_get_Width(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_Height(CoverArt: TCoverArt): Cardinal; export; stdcall;
function CoverArt_get_ColorDepth(CoverArt: TCoverArt): Cardinal;
  export; stdcall;
function CoverArt_get_NoOfColors(CoverArt: TCoverArt): Cardinal;
  export; stdcall;
function CoverArt_get_Parent(CoverArt: TCoverArt): TTagsBase; export; stdcall;
function CoverArt_get_Index(CoverArt: TCoverArt): Integer; export; stdcall;

procedure CoverArt_set_Name(CoverArt: TCoverArt; Name: PChar); export; stdcall;
procedure CoverArt_set_Stream(CoverArt: TCoverArt; var buffer: PByte;
  size: Integer); export; stdcall;
procedure CoverArt_set_Description(CoverArt: TCoverArt; Description: PChar);
  export; stdcall;
procedure CoverArt_set_CoverType(CoverArt: TCoverArt; CoverType: Cardinal);
  export; stdcall;
procedure CoverArt_set_MIMEType(CoverArt: TCoverArt; MIMEType: PChar);
  export; stdcall;
procedure CoverArt_set_PictureFormat(CoverArt: TCoverArt;
  PictureFormat: TTagPictureFormat); export; stdcall;
procedure CoverArt_set_Width(CoverArt: TCoverArt; Width: Cardinal);
  export; stdcall;
procedure CoverArt_set_Height(CoverArt: TCoverArt; Height: Cardinal);
  export; stdcall;
procedure CoverArt_set_ColorDepth(CoverArt: TCoverArt; ColorDepth: Cardinal);
  export; stdcall;
procedure CoverArt_set_NoOfColors(CoverArt: TCoverArt; NoOfColors: Cardinal);
  export; stdcall;
procedure CoverArt_set_Parent(CoverArt: TCoverArt; Parent: TTagsBase);
  export; stdcall;
procedure CoverArt_set_Index(CoverArt: TCoverArt; Index: Integer);
  export; stdcall;

function CoverArt_Create(Parent: TTagsBase): TCoverArt; export; stdcall;
procedure CoverArt_Destroy(CoverArt: TCoverArt); export; stdcall;
procedure CoverArt_Clear(CoverArt: TCoverArt); export; stdcall;
procedure CoverArt_Delete(CoverArt: TCoverArt); export; stdcall;
function CoverArt_Assign(thisCover: TCoverArt; CoverArt: TCoverArt): Boolean;
  export; stdcall;
{$ENDREGION}
{$REGION 'SourceAudioAttributes Declarations'}
function SourceAudioAttributes_Create(Parent: TTagsBase)
  : TSourceAudioAttributes; export; stdcall;
procedure SourceAudioAttributes_Destroy(SourceAudioAttributes
  : TSourceAudioAttributes); export; stdcall;
function SourceAudioAttributes_get_Channels(SourceAudioAttributes
  : TSourceAudioAttributes): Word; export; stdcall;
function SourceAudioAttributes_get_SamplesPerSec(SourceAudioAttributes
  : TSourceAudioAttributes): Cardinal; export; stdcall;
function SourceAudioAttributes_get_BitsPerSample(SourceAudioAttributes
  : TSourceAudioAttributes): Word; export; stdcall;
function SourceAudioAttributes_get_PlayTime(SourceAudioAttributes
  : TSourceAudioAttributes): Double; export; stdcall;
function SourceAudioAttributes_get_SampleCount(SourceAudioAttributes
  : TSourceAudioAttributes): Int64; export; stdcall;
function SourceAudioAttributes_get_BitRate(SourceAudioAttributes
  : TSourceAudioAttributes): Integer; export; stdcall;
function SourceAudioAttributes_get_Parent(SourceAudioAttributes
  : TSourceAudioAttributes): TTagsBase; export; stdcall;
procedure SourceAudioAttributes_set_Parent(SourceAudioAttributes
  : TSourceAudioAttributes; Parent: TTagsBase); export; stdcall;
{$ENDREGION}
{$REGION 'TagsBase Declaration'}
function TagsBase_get_Loaded(Tags: TTagsBase): Boolean; export; stdcall;
procedure TagsBase_get_Tags(Tags: TTagsBase; var pointer: PTagArray;
  var size: Integer); export; stdcall;
procedure TagsBase_get_CoverArts(Tags: TTagsBase; var pointer: PCoverArt;
  var size: Integer); export; stdcall;
function TagsBase_get_Size(Tags: TTagsBase): Int64; export; stdcall;

procedure TagsBase_set_Loaded(Tags: TTagsBase; Loaded: Boolean);
  export; stdcall;
procedure TagsBase_set_Tags(thisTags: TTagsBase; Tags: TTagArray);
  export; stdcall;
procedure TagsBase_set_CoverArts(Tags: TTagsBase; CoverArts: TCoverArtArray);
  export; stdcall;

function TagsBase_Create: TTagsBase; export; stdcall;
procedure TagsBase_Destroy(Tags: TTagsBase); export; stdcall;
function TagsBase_LoadFromFile(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
function TagsBase_LoadFromBASS(Tags: TTagsBase; Channel: Cardinal): Integer;
  export; stdcall;
function TagsBase_SaveToFile1(Tags: TTagsBase): Integer; export; stdcall;
function TagsBase_SaveToFile2(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
function TagsBase_Add(Tags: TTagsBase; Name: PChar): TTag; export; stdcall;
function TagsBase_Tag(Tags: TTagsBase; Name: PChar): TTag; export; stdcall;
function TagsBase_GetTag(Tags: TTagsBase; Name: PChar): PChar; export; stdcall;
function TagsBase_SetTag(Tags: TTagsBase; Name: PChar; Text: PChar): Integer;
  export; stdcall;
function TagsBase_Delete(Tags: TTagsBase; Index: Integer): Boolean;
  export; stdcall;
function TagsBase_Delete2(Tags: TTagsBase; Name: PChar): Boolean;
  export; stdcall;
function TagsBase_Remove(Tags: TTagsBase; Index: Integer): Boolean;
  export; stdcall;
procedure TagsBase_DeleteAllTags(Tags: TTagsBase); export; stdcall;
procedure TagsBase_Clear(Tags: TTagsBase); export; stdcall;
function TagsBase_Count(Tags: TTagsBase): Integer; export; stdcall;
function TagsBase_Exists(Tags: TTagsBase; Name: PChar): Integer;
  export; stdcall;
function TagsBase_TypeCount(Tags: TTagsBase; Name: PChar): Integer;
  export; stdcall;
procedure TagsBase_RemoveEmptyTags(Tags: TTagsBase); export; stdcall;
function TagsBase_CoverArtCount(Tags: TTagsBase): Integer; export; stdcall;
function TagsBase_CoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
  export; stdcall;
function TagsBase_AddCoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
  export; stdcall;
function TagsBase_AddCoverArt2(Tags: TTagsBase; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt; export; stdcall;
function TagsBase_AddCoverArt3(Tags: TTagsBase; Name: PChar; FileName: PChar)
  : TCoverArt; export; stdcall;
function TagsBase_DeleteCoverArt(Tags: TTagsBase; Index: Integer): Boolean;
  export; stdcall;
procedure TagsBase_DeleteAllCoverArts(Tags: TTagsBase); export; stdcall;
function TagsBase_Assign(Tags: TTagsBase; Source: TTagsBase): Boolean;
  export; stdcall;
function TagsBase_PictureStreamType(Tags: TTagsBase; var buffer: PByte;
  size: Int64): TTagPictureFormat; export; stdcall;
function TagsBase_PicturePointerType(Tags: TTagsBase; Picture: pointer)
  : TTagPictureFormat; export; stdcall;
function TagsBase_GetTypeString(Tags: TTagsBase): PChar; export; stdcall;
{$ENDREGION}
{$REGION 'ID3v1Tag Declaration'}
function ID3v1Tag_Create(): TID3v1Tag; export; stdcall;
procedure ID3v1Tag_Destroy(Tags: TID3v1Tag); export; stdcall;
procedure ID3v1Tag_SetTitle(Tags: TID3v1Tag; Value: PChar); export; stdcall;
procedure ID3v1Tag_SetArtist(Tags: TID3v1Tag; Value: PChar); export; stdcall;
procedure ID3v1Tag_SetAlbum(Tags: TID3v1Tag; Value: PChar); export; stdcall;
procedure ID3v1Tag_SetYear(Tags: TID3v1Tag; Value: PChar); export; stdcall;
procedure ID3v1Tag_SetComment(Tags: TID3v1Tag; Value: PChar); export; stdcall;
procedure ID3v1Tag_SetTrack(Tags: TID3v1Tag; Value: Byte); export; stdcall;
procedure ID3v1Tag_SetGenre(Tags: TID3v1Tag; Value: PChar); export; stdcall;

function ID3v1Tag_GetTite(Tags: TID3v1Tag): PChar; export; stdcall;
function ID3v1Tag_GetArtist(Tags: TID3v1Tag): PChar; export; stdcall;
function ID3v1Tag_GetAlbum(Tags: TID3v1Tag): PChar; export; stdcall;
function ID3v1Tag_GetYear(Tags: TID3v1Tag): PChar; export; stdcall;
function ID3v1Tag_GetComment(Tags: TID3v1Tag): PChar; export; stdcall;
function ID3v1Tag_GetTrack(Tags: TID3v1Tag): Byte; export; stdcall;
function ID3v1Tag_GetGenre(Tags: TID3v1Tag): PChar; export; stdcall;

function ID3v1Tag_Assign1(Tags: TID3v1Tag; Source: TTagsBase): Boolean;
  export; stdcall;
function ID3v1Tag_Assign2(Tags: TID3v1Tag; Source: TID3v1Tag): Boolean;
  export; stdcall;
function ID3v1Tag_LoadFromFile(Tags: TID3v1Tag; FileName: PChar): Integer; export; stdcall;
function ID3v1Tag_LoadFromBASS(Tags: TID3v1Tag; Channel: Cardinal): Integer; export; stdcall;
function ID3v1Tag_SaveToFile1(Tags: TID3v1Tag; FileName: PChar): Integer; export; stdcall;
function ID3v1Tag_SaveToFile2(Tags: TID3v1Tag; FileName: PChar;
  WriteLyricsTag: Boolean): Integer; export; stdcall;
procedure ID3v1Tag_Clear(Tags: TID3v1Tag); export; stdcall;
function ID3v1Tag_GetTypeString(Tags: TID3v1Tag): PChar; export; stdcall;
{$ENDREGION}
{$REGION 'Convertions'}
function ID3v1TagToTagsBase(Tags: TID3v1Tag): TTagsBase; export; stdcall;
function TagsBaseToID3v1Tag(Tags: TTagsBase): TID3v1Tag; export; stdcall;
{$ENDREGION}
{$REGION 'Comments'}
(* {$REGION 'Tags Declaration'}
  function Tags_get_FileName(Tags: TTagsBase): PChar; export; stdcall;
  function Tags_get_Loaded(Tags: TTagsBase): Boolean; export; stdcall;
  procedure Tags_get_Tags(Tags: TTagsBase; var pointer: PTagArray; var size: Integer);
  export; stdcall;
  procedure Tags_get_CoverArts(Tags: TTagsBase; var pointer: PCoverArt;
  var size: Integer); export; stdcall;
  function Tags_get_SourceAudioAttributes(Tags: TTagsBase): TSourceAudioAttributes;
  export; stdcall;
  function Tags_get_UpperCaseFieldNamesToWrite(Tags: TTagsBase): Boolean;
  export; stdcall;
  function Tags_get_Size(Tags: TTagsBase): Int64; export; stdcall;

  procedure Tags_set_FileName(Tags: TTagsBase; FileName: PChar); export; stdcall;
  procedure Tags_set_Loaded(Tags: TTagsBase; Loaded: Boolean); export; stdcall;
  procedure Tags_set_Tags(thisTags: TTagsBase; Tags: TTagArray); export; stdcall;
  procedure Tags_set_CoverArts(Tags: TTagsBase; CoverArts: TCoverArtArray);
  export; stdcall;
  procedure Tags_set_SourceAudioAttributes(Tags: TTagsBase;
  SourceAudioAttributes: TSourceAudioAttributes); export; stdcall;
  procedure Tags_set_UpperCaseFieldNamesToWrite(Tags: TTagsBase;
  UpperCaseFieldNamesToWrite: Boolean); export; stdcall;

  function Tags_Create: TTagsBase; export; stdcall;
  procedure Tags_Destroy(Tags: TTagsBase); export; stdcall;
  function Tags_LoadFromFile(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_LoadFromBASS(Tags: TTagsBase; Channel: Cardinal): Integer;
  export; stdcall;
  function Tags_SaveToFile(Tags: TTagsBase; FileName: PChar; TagType: TTagType)
  : Integer; export; stdcall;
  procedure Tags_LoadTags(Tags: TTagsBase); export; stdcall;
  function Tags_Add(Tags: TTagsBase; Name: PChar): TTag; export; stdcall;
  function Tags_Tag(Tags: TTagsBase; Name: PChar): TTag; export; stdcall;
  function Tags_GetTag(Tags: TTagsBase; Name: PChar): PChar; export; stdcall;
  function Tags_SetTag(Tags: TTagsBase; Name: PChar; Text: PChar): Integer;
  export; stdcall;
  // TStrings
  function Tags_SetList(Tags: TTagsBase; Name: PChar; List: TStrings): Integer;
  export; stdcall;
  function Tags_Delete(Tags: TTagsBase; Index: Integer): Boolean; export; stdcall;
  function Tags_Delete2(Tags: TTagsBase; Name: PChar): Boolean; export; stdcall;
  function Tags_Remove(Tags: TTagsBase; Index: Integer): Boolean; export; stdcall;
  procedure Tags_DeleteAllTags(Tags: TTagsBase); export; stdcall;
  procedure Tags_Clear(Tags: TTagsBase); export; stdcall;
  function Tags_Count(Tags: TTagsBase): Integer; export; stdcall;
  function Tags_Exists(Tags: TTagsBase; Name: PChar): Integer; export; stdcall;
  function Tags_TypeCount(Tags: TTagsBase; Name: PChar): Integer; export; stdcall;
  procedure Tags_RemoveEmptyTags(Tags: TTagsBase); export; stdcall;
  function Tags_CoverArtCount(Tags: TTagsBase): Integer; export; stdcall;
  function Tags_CoverArt(Tags: TTagsBase; Name: PChar): TCoverArt; export; stdcall;
  function Tags_AddCoverArt(Tags: TTagsBase; Name: PChar): TCoverArt; export; stdcall;
  function Tags_AddCoverArt2(Tags: TTagsBase; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt; export; stdcall;
  function Tags_AddCoverArt3(Tags: TTagsBase; Name: PChar; FileName: PChar)
  : TCoverArt; export; stdcall;
  function Tags_DeleteCoverArt(Tags: TTagsBase; Index: Integer): Boolean;
  export; stdcall;
  procedure Tags_DeleteAllCoverArts(Tags: TTagsBase); export; stdcall;
  function Tags_Assign(Tags: TTagsBase; Source: TTagsBase): Boolean; export; stdcall;
  function Tags_SaveAPEv2Tag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveFlacTag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveID3v1Tag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveID3v2Tag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveMP4Tag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveOggVorbisAndOpusTag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  function Tags_SaveWAVTag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  {$IFDEF MSWINDOWS}
  function Tags_SaveWMATag(Tags: TTagsBase; FileName: PChar): Integer;
  export; stdcall;
  {$ENDIF}
  procedure Tags_LoadAPEv2Tags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadFlacTags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadOggVorbisAndOpusTags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadID3v1Tags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadID3v2Tags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadMP4Tags(Tags: TTagsBase); export; stdcall;
  procedure Tags_LoadWAVTags(Tags: TTagsBase); export; stdcall;
  {$IFDEF MSWINDOWS}
  procedure Tags_LoadWMATags(Tags: TTagsBase); export; stdcall;
  {$ENDIF}
  // TStrings
  procedure Tags_LoadNullTerminatedStrings(Tags: TTagsBase; TagType: PChar;
  TagList: TStrings); export; stdcall;
  // TStrings
  procedure Tags_LoadNullTerminatedWAVRIFFINFOStrings(Tags: TTagsBase;
  TagList: TStrings); export; stdcall;
  function Tags_PictureStreamType(Tags: TTagsBase; var buffer: PByte; size: Int64)
  : TTagPictureFormat; export; stdcall;
  function Tags_PicturePointerType(Tags: TTagsBase; Picture: pointer)
  : TTagPictureFormat; export; stdcall;

  {$ENDREGION}
  function RemoveTagsFromFile2(FileName: PChar; TagType: TTagType = ttAutomatic)
  : Integer; export; stdcall;
  function TagsLibraryVersion: Cardinal; export; stdcall;
  function TagsLibraryErrorCode2String2(ErrorCode: Integer): PChar;
  export; stdcall; *)
{$ENDREGION}

implementation

{$REGION 'Tag'}

function Tag_Create(Parent: TTagsBase): TTag;
begin
  Result := TTag.Create(Parent);
end;

procedure Tag_Destroy(Tag: TTag);
begin
  Tag.Destroy;
end;

function Tag_get_Name(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Name);
end;

function Tag_get_Value(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Value);
end;

function Tag_get_Language(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Language);
end;

function Tag_get_Description(Tag: TTag): PChar;
begin
  Result := PChar(Tag.Description);
end;

function Tag_get_ExtTagType(Tag: TTag): TExtTagType;
begin
  Result := Tag.ExtTagType;
end;

function Tag_get_Parent(Tag: TTag): TTagsBase;
begin
  Result := Tag.Parent;
end;

function Tag_get_Index(Tag: TTag): Integer;
begin
  Result := Tag.Index;
end;

procedure Tag_set_Name(Tag: TTag; Name: PChar);
begin
  Tag.Name := Name;
end;

procedure Tag_set_Value(Tag: TTag; Value: PChar);
begin
  Tag.Value := Value;
end;

procedure Tag_set_Language(Tag: TTag; Language: PChar);
begin
  Tag.Language := Language;
end;

procedure Tag_set_Description(Tag: TTag; Description: PChar);
begin
  Tag.Description := Description;
end;

procedure Tag_set_ExtTagType(Tag: TTag; ExtTagType: TExtTagType);
begin
  Tag.ExtTagType := ExtTagType;
end;

procedure Tag_set_Parent(Tag: TTag; Parent: TTagsBase);
begin
  Tag.Parent := Parent;
end;

procedure Tag_set_Index(Tag: TTag; Index: Integer);
begin
  Tag.Index := Index;
end;

procedure Tag_Clear(Tag: TTag);
begin
  Tag.Clear;
end;

function Tag_Delete(Tag: TTag): Boolean;
begin
  Result := Tag.Delete;
end;

procedure Tag_Remove(Tag: TTag);
begin
  Tag.Remove;
end;

function Tag_Assign(this: TTag; Tag: TTag): Boolean;
begin
  Result := this.Assign(Tag);
end;
{$ENDREGION}
{$REGION 'CoverArt'}

function CoverArt_get_Name(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.Name);
end;

procedure CoverArt_get_Stream(CoverArt: TCoverArt; var buffer: PByte;
  var size: Int64);
var
  bytes: array of Byte;
begin
  if CoverArt.Stream = nil then
  begin
    exit;
  end;

  SetLength(bytes, CoverArt.Stream.size);
  CoverArt.Stream.Seek(0, soBeginning);
  CoverArt.Stream.Read(bytes[0], CoverArt.Stream.size);
  buffer := PByte(bytes);
  size := CoverArt.Stream.size;
end;

function CoverArt_get_Description(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.Description);
end;

function CoverArt_get_CoverType(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.CoverType;
end;

function CoverArt_get_MIMEType(CoverArt: TCoverArt): PChar;
begin
  Result := PChar(CoverArt.MIMEType);
end;

function CoverArt_get_PictureFormat(CoverArt: TCoverArt): TTagPictureFormat;
begin
  Result := CoverArt.PictureFormat;
end;

function CoverArt_get_Width(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.Width;
end;

function CoverArt_get_Height(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.Height;
end;

function CoverArt_get_ColorDepth(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.ColorDepth;
end;

function CoverArt_get_NoOfColors(CoverArt: TCoverArt): Cardinal;
begin
  Result := CoverArt.NoOfColors;
end;

function CoverArt_get_Parent(CoverArt: TCoverArt): TTagsBase;
begin
  Result := CoverArt.Parent;
end;

function CoverArt_get_Index(CoverArt: TCoverArt): Integer;
begin
  Result := CoverArt.Index;
end;

procedure CoverArt_set_Name(CoverArt: TCoverArt; Name: PChar);
begin
  CoverArt.Name := Name;
end;

procedure CoverArt_set_Stream(CoverArt: TCoverArt; var buffer: PByte;
  size: Integer);
begin
  if CoverArt.Stream <> nil then
    CoverArt.Stream.Destroy;

  CoverArt.Stream := TMemoryStream.Create;
  CoverArt.Stream.Write(buffer[0], size);
end;

procedure CoverArt_set_Description(CoverArt: TCoverArt; Description: PChar);
begin
  CoverArt.Description := Description;
end;

procedure CoverArt_set_CoverType(CoverArt: TCoverArt; CoverType: Cardinal);
begin
  CoverArt.CoverType := CoverType;
end;

procedure CoverArt_set_MIMEType(CoverArt: TCoverArt; MIMEType: PChar);
begin
  CoverArt.MIMEType := MIMEType;
end;

procedure CoverArt_set_PictureFormat(CoverArt: TCoverArt;
  PictureFormat: TTagPictureFormat);
begin
  CoverArt.PictureFormat := PictureFormat;
end;

procedure CoverArt_set_Width(CoverArt: TCoverArt; Width: Cardinal);
begin
  CoverArt.Width := Width;
end;

procedure CoverArt_set_Height(CoverArt: TCoverArt; Height: Cardinal);
begin
  CoverArt.Height := Height;
end;

procedure CoverArt_set_ColorDepth(CoverArt: TCoverArt; ColorDepth: Cardinal);
begin
  CoverArt.ColorDepth := ColorDepth;
end;

procedure CoverArt_set_NoOfColors(CoverArt: TCoverArt; NoOfColors: Cardinal);
begin
  CoverArt.NoOfColors := NoOfColors;
end;

procedure CoverArt_set_Parent(CoverArt: TCoverArt; Parent: TTagsBase);
begin
  CoverArt.Parent := Parent;
end;

procedure CoverArt_set_Index(CoverArt: TCoverArt; Index: Integer);
begin
  CoverArt.Index := Index;
end;

function CoverArt_Create(Parent: TTagsBase): TCoverArt;
begin
  Result := TCoverArt.Create(Parent);
end;

procedure CoverArt_Destroy(CoverArt: TCoverArt);
begin
  CoverArt.Destroy;
end;

procedure CoverArt_Clear(CoverArt: TCoverArt);
begin
  CoverArt.Clear;
end;

procedure CoverArt_Delete(CoverArt: TCoverArt);
begin
  CoverArt.Delete;
end;

function CoverArt_Assign(thisCover: TCoverArt; CoverArt: TCoverArt): Boolean;
begin
  Result := thisCover.Assign(CoverArt);
end;

{$ENDREGION}
{$REGION 'SourceAudioAttributes'}

function SourceAudioAttributes_Create(Parent: TTagsBase)
  : TSourceAudioAttributes;
begin
  Result := TSourceAudioAttributes.Create(Parent);
end;

procedure SourceAudioAttributes_Destroy(SourceAudioAttributes
  : TSourceAudioAttributes);
begin
  SourceAudioAttributes.Destroy;
end;

function SourceAudioAttributes_get_Channels(SourceAudioAttributes
  : TSourceAudioAttributes): Word;
begin
  Result := SourceAudioAttributes.Channels;
end;

function SourceAudioAttributes_get_SamplesPerSec(SourceAudioAttributes
  : TSourceAudioAttributes): Cardinal;
begin
  Result := SourceAudioAttributes.SamplesPerSec;
end;

function SourceAudioAttributes_get_BitsPerSample(SourceAudioAttributes
  : TSourceAudioAttributes): Word;
begin
  Result := SourceAudioAttributes.BitsPerSample;
end;

function SourceAudioAttributes_get_PlayTime(SourceAudioAttributes
  : TSourceAudioAttributes): Double;
begin
  Result := SourceAudioAttributes.PlayTime;
end;

function SourceAudioAttributes_get_SampleCount(SourceAudioAttributes
  : TSourceAudioAttributes): Int64;
begin
  Result := SourceAudioAttributes.SampleCount;
end;

function SourceAudioAttributes_get_BitRate(SourceAudioAttributes
  : TSourceAudioAttributes): Integer;
begin
  Result := SourceAudioAttributes.BitRate;
end;

function SourceAudioAttributes_get_Parent(SourceAudioAttributes
  : TSourceAudioAttributes): TTagsBase;
begin
  Result := SourceAudioAttributes.Parent;
end;

procedure SourceAudioAttributes_set_Parent(SourceAudioAttributes
  : TSourceAudioAttributes; Parent: TTagsBase);
begin
  SourceAudioAttributes.Parent := Parent;
end;
{$ENDREGION}
{$REGION 'Tags'}

function TagsBase_get_Loaded(Tags: TTagsBase): Boolean;
begin
  Result := Tags.Loaded;
end;

procedure TagsBase_get_Tags(Tags: TTagsBase; var pointer: PTagArray;
  var size: Integer);
begin
  pointer := PTagArray(Tags.Tags);
  size := Length(Tags.Tags);
end;

procedure TagsBase_get_CoverArts(Tags: TTagsBase; var pointer: PCoverArt;
  var size: Integer);
begin
  pointer := PCoverArt(Tags.CoverArts);
  size := Length(Tags.CoverArts);
end;

function TagsBase_get_Size(Tags: TTagsBase): Int64;
begin
  Result := Tags.size;
end;

procedure TagsBase_set_Loaded(Tags: TTagsBase; Loaded: Boolean);
begin
  Tags.Loaded := Loaded;
end;

procedure TagsBase_set_Tags(thisTags: TTagsBase; Tags: TTagArray);
begin
  TTagArray(thisTags.Tags) := Tags;
end;

procedure TagsBase_set_CoverArts(Tags: TTagsBase; CoverArts: TCoverArtArray);
begin
  TCoverArtArray(Tags.CoverArts) := CoverArts;
end;

function TagsBase_Create: TTagsBase;
begin
  Result := TTagsBase.Create;
end;

procedure TagsBase_Destroy(Tags: TTagsBase);
begin
  Tags.Destroy;
end;

function TagsBase_LoadFromFile(Tags: TTagsBase; FileName: PChar): Integer;
begin
  Result := Tags.LoadFromFile(FileName);
end;

function TagsBase_LoadFromBASS(Tags: TTagsBase; Channel: Cardinal): Integer;
begin
  Result := Tags.LoadFromBASS(Channel);
end;

function TagsBase_SaveToFile1(Tags: TTagsBase): Integer;
begin
  Result := Tags.SaveToFile;
end;

function TagsBase_SaveToFile2(Tags: TTagsBase; FileName: PChar): Integer;
begin
  Result := Tags.SaveToFile(FileName);
end;

function TagsBase_Add(Tags: TTagsBase; Name: PChar): TTag;
begin
  Result := Tags.Add(Name);
end;

function TagsBase_Tag(Tags: TTagsBase; Name: PChar): TTag;
begin
  Result := Tags.Tag(Name);
end;

function TagsBase_GetTag(Tags: TTagsBase; Name: PChar): PChar;
begin
  Result := PChar(Tags.GetTag(Name));
end;

function TagsBase_SetTag(Tags: TTagsBase; Name: PChar; Text: PChar): Integer;
begin
  Result := Tags.SetTag(Name, Text);
end;

function TagsBase_Delete(Tags: TTagsBase; Index: Integer): Boolean;
begin
  Result := Tags.Delete(Index);
end;

function TagsBase_Delete2(Tags: TTagsBase; Name: PChar): Boolean;
begin
  Result := Tags.Delete(Name);
end;

function TagsBase_Remove(Tags: TTagsBase; Index: Integer): Boolean;
begin
  Result := Tags.Remove(Index);
end;

procedure TagsBase_DeleteAllTags(Tags: TTagsBase);
begin
  Tags.DeleteAllTags;
end;

procedure TagsBase_Clear(Tags: TTagsBase);
begin
  Tags.Clear;
end;

function TagsBase_Count(Tags: TTagsBase): Integer;
begin
  Result := Tags.Count;
end;

function TagsBase_Exists(Tags: TTagsBase; Name: PChar): Integer;
begin
  Result := Tags.Exists(Name);
end;

function TagsBase_TypeCount(Tags: TTagsBase; Name: PChar): Integer;
begin
  Result := Tags.TypeCount(Name);
end;

procedure TagsBase_RemoveEmptyTags(Tags: TTagsBase);
begin
  Tags.RemoveEmptyTags;
end;

function TagsBase_CoverArtCount(Tags: TTagsBase): Integer;
begin
  Result := Tags.CoverArtCount;
end;

function TagsBase_CoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
begin
  Result := Tags.CoverArt(Name);
end;

function TagsBase_AddCoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
begin
  Result := Tags.AddCoverArt(Name);
end;

function TagsBase_AddCoverArt2(Tags: TTagsBase; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt;
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.AddCoverArt(Name, Stream, MIMEType);
end;

function TagsBase_AddCoverArt3(Tags: TTagsBase; Name: PChar; FileName: PChar)
  : TCoverArt;
begin
  Result := Tags.AddCoverArt(Name, FileName);
end;

function TagsBase_DeleteCoverArt(Tags: TTagsBase; Index: Integer): Boolean;
begin
  Result := Tags.DeleteCoverArt(Index);
end;

procedure TagsBase_DeleteAllCoverArts(Tags: TTagsBase);
begin
  Tags.DeleteAllCoverArts;
end;

function TagsBase_Assign(Tags: TTagsBase; Source: TTagsBase): Boolean;
begin
  Result := Tags.Assign(Source);
end;

function TagsBase_PictureStreamType(Tags: TTagsBase; var buffer: PByte;
  size: Int64): TTagPictureFormat;
var
  Stream: TMemoryStream;
begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.PictureStreamType(Stream);
end;

function TagsBase_PicturePointerType(Tags: TTagsBase; Picture: pointer)
  : TTagPictureFormat;
begin
  Result := Tags.PicturePointerType(Picture);
end;

function TagsBase_GetTypeString(Tags: TTagsBase): PChar;
begin
  Log('TagsBase_GetTypeString');
  Log('Pointer = ' + IntToStr(Integer(Pointer(Tags))));
  Log('TypeString = ' + Tags.GetTypeString);
  Result := PChar(Tags.GetTypeString);
end;
{$ENDREGION}
{$REGION 'ID3v1Tag'}

function ID3v1Tag_Create(): TID3v1Tag;
begin
  Result := TID3v1Tag.Create;
end;

procedure ID3v1Tag_Destroy(Tags: TID3v1Tag);
begin
  Tags.Destroy;
end;

procedure ID3v1Tag_SetTitle(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Title := Value;
end;

procedure ID3v1Tag_SetArtist(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Artist := Value;
end;

procedure ID3v1Tag_SetAlbum(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Album := Value;
end;

procedure ID3v1Tag_SetYear(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Year := Value;
end;

procedure ID3v1Tag_SetComment(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Comment := Value;
end;

procedure ID3v1Tag_SetTrack(Tags: TID3v1Tag; Value: Byte);
begin
  Tags.Track := Value;
end;

procedure ID3v1Tag_SetGenre(Tags: TID3v1Tag; Value: PChar);
begin
  Tags.Genre := Value;
end;

function ID3v1Tag_GetTite(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Title);
end;

function ID3v1Tag_GetArtist(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Artist);
end;

function ID3v1Tag_GetAlbum(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Album);
end;

function ID3v1Tag_GetYear(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Year);
end;

function ID3v1Tag_GetComment(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Comment);
end;

function ID3v1Tag_GetTrack(Tags: TID3v1Tag): Byte;
begin
  Result := Tags.Track;
end;

function ID3v1Tag_GetGenre(Tags: TID3v1Tag): PChar;
begin
  Result := PChar(Tags.Genre);
end;

function ID3v1Tag_Assign1(Tags: TID3v1Tag; Source: TTagsBase): Boolean;
begin
  Result := Tags.Assign(Source);
end;

function ID3v1Tag_Assign2(Tags: TID3v1Tag; Source: TID3v1Tag): Boolean;
begin
  Result := Tags.Assign(Source);
end;

function ID3v1Tag_SaveToFile1(Tags: TID3v1Tag; FileName: PChar): Integer;
begin
  Result := Tags.SaveToFile(FileName);
end;

function ID3v1Tag_SaveToFile2(Tags: TID3v1Tag; FileName: PChar;
  WriteLyricsTag: Boolean): Integer;
begin
  Result := Tags.SaveToFile(FileName, WriteLyricsTag);
end;

function ID3v1Tag_GetTypeString(Tags: TID3v1Tag): PChar;
begin
  Log('ID3v1Tag_GetTypeString');
  Log('Pointer = ' + IntToStr(Integer(Pointer(Tags))));
  Log('TypeString = ' + Tags.GetTypeString);
  Result := PChar(Tags.GetTypeString);
end;

function ID3v1Tag_LoadFromFile(Tags: TID3v1Tag; FileName: PChar): Integer;
begin
  Result := Tags.LoadFromFile(FileName);
end;

function ID3v1Tag_LoadFromBASS(Tags: TID3v1Tag; Channel: Cardinal): Integer;
begin
  Result := Tags.LoadFromBASS(Channel);
end;

procedure ID3v1Tag_Clear(Tags: TID3v1Tag);
begin
  Tags.Clear;
end;
{$ENDREGION}
{$REGION 'Convertions'}

function ID3v1TagToTagsBase(Tags: TID3v1Tag): TTagsBase;
begin
  Result := TTagsBase(Tags);
end;

function TagsBaseToID3v1Tag(Tags: TTagsBase): TID3v1Tag;
begin
  Result := TID3v1Tag(Tags);
end;
{$ENDREGION}
{$REGION 'Comments'}
(* {$REGION 'Tags'}

  function Tags_get_FileName(Tags: TTagsBase): PChar;
  begin
  Result := PChar(Tags.FileName);
  end;

  function Tags_get_Loaded(Tags: TTagsBase): Boolean;
  begin
  Result := Tags.Loaded;
  end;

  procedure Tags_get_Tags(Tags: TTagsBase; var pointer: PTagArray; var size: Integer);
  begin
  pointer := PTagArray(Tags.Tags);
  size := Length(Tags.Tags);
  end;

  procedure Tags_get_CoverArts(Tags: TTagsBase; var pointer: PCoverArt;
  var size: Integer);
  begin
  pointer := PCoverArt(Tags.CoverArts);
  size := Length(Tags.CoverArts);
  end;

  function Tags_get_SourceAudioAttributes(Tags: TTagsBase): TSourceAudioAttributes;
  begin
  Result := Tags.SourceAudioAttributes;
  end;

  function Tags_get_UpperCaseFieldNamesToWrite(Tags: TTagsBase): Boolean;
  begin
  Result := Tags.UpperCaseFieldNamesToWrite;
  end;

  function Tags_get_Size(Tags: TTagsBase): Int64;
  begin
  Result := Tags.size;
  end;

  procedure Tags_set_FileName(Tags: TTagsBase; FileName: PChar);
  begin
  Tags.FileName := FileName;
  end;

  procedure Tags_set_Loaded(Tags: TTagsBase; Loaded: Boolean);
  begin
  Tags.Loaded := Loaded;
  end;

  procedure Tags_set_Tags(thisTags: TTagsBase; Tags: TTagArray);
  begin
  TTagArray(thisTags.Tags) := Tags;
  end;

  procedure Tags_set_CoverArts(Tags: TTagsBase; CoverArts: TCoverArtArray);
  begin
  TCoverArtArray(Tags.CoverArts) := CoverArts;
  end;

  procedure Tags_set_SourceAudioAttributes(Tags: TTagsBase;
  SourceAudioAttributes: TSourceAudioAttributes);
  begin
  Tags.SourceAudioAttributes := SourceAudioAttributes;
  end;

  procedure Tags_set_UpperCaseFieldNamesToWrite(Tags: TTagsBase;
  UpperCaseFieldNamesToWrite: Boolean);
  begin
  Tags.UpperCaseFieldNamesToWrite := UpperCaseFieldNamesToWrite;
  end;

  function Tags_Create: TTagsBase;
  begin
  Result := TTagsBase.Create;
  end;

  procedure Tags_Destroy(Tags: TTagsBase);
  begin
  Tags.Destroy;
  end;

  function Tags_LoadFromFile(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.LoadFromFile(FileName);
  end;

  function Tags_LoadFromBASS(Tags: TTagsBase; Channel: Cardinal): Integer;
  begin
  Result := Tags.LoadFromBASS(Channel);
  end;

  function Tags_SaveToFile(Tags: TTagsBase; FileName: PChar;
  TagType: TTagType): Integer;
  begin
  Result := Tags.SaveToFile(FileName, TagType);
  end;

  procedure Tags_LoadTags(Tags: TTagsBase);
  begin
  Tags.LoadTags;
  end;

  function Tags_Add(Tags: TTagsBase; Name: PChar): TTag;
  begin
  Result := Tags.Add(Name);
  end;

  function Tags_Tag(Tags: TTagsBase; Name: PChar): TTag;
  begin
  Result := Tags.Tag(Name);
  end;

  function Tags_GetTag(Tags: TTagsBase; Name: PChar): PChar;
  begin
  Result := PChar(Tags.GetTag(Name));
  end;

  function Tags_SetTag(Tags: TTagsBase; Name: PChar; Text: PChar): Integer;
  begin
  Result := Tags.SetTag(Name, Text);
  end;

  // TStrings
  function Tags_SetList(Tags: TTagsBase; Name: PChar; List: TStrings): Integer;
  begin
  Result := Tags.SetList(Name, List);
  end;

  function Tags_Delete(Tags: TTagsBase; Index: Integer): Boolean;
  begin
  Result := Tags.Delete(Index);
  end;

  function Tags_Delete2(Tags: TTagsBase; Name: PChar): Boolean;
  begin
  Result := Tags.Delete(Name);
  end;

  function Tags_Remove(Tags: TTagsBase; Index: Integer): Boolean;
  begin
  Result := Tags.Remove(Index);
  end;

  procedure Tags_DeleteAllTags(Tags: TTagsBase);
  begin
  Tags.DeleteAllTags;
  end;

  procedure Tags_Clear(Tags: TTagsBase);
  begin
  Tags.Clear;
  end;

  function Tags_Count(Tags: TTagsBase): Integer;
  begin
  Result := Tags.Count;
  end;

  function Tags_Exists(Tags: TTagsBase; Name: PChar): Integer;
  begin
  Result := Tags.Exists(Name);
  end;

  function Tags_TypeCount(Tags: TTagsBase; Name: PChar): Integer;
  begin
  Result := Tags.TypeCount(Name);
  end;

  procedure Tags_RemoveEmptyTags(Tags: TTagsBase);
  begin
  Tags.RemoveEmptyTags;
  end;

  function Tags_CoverArtCount(Tags: TTagsBase): Integer;
  begin
  Result := Tags.CoverArtCount;
  end;

  function Tags_CoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
  begin
  Result := Tags.CoverArt(Name);
  end;

  function Tags_AddCoverArt(Tags: TTagsBase; Name: PChar): TCoverArt;
  begin
  Result := Tags.AddCoverArt(Name);
  end;

  function Tags_AddCoverArt2(Tags: TTagsBase; Name: PChar; var buffer: PByte;
  size: Int64; MIMEType: PChar): TCoverArt;
  var
  Stream: TMemoryStream;
  begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.AddCoverArt(Name, Stream, MIMEType);
  end;

  function Tags_AddCoverArt3(Tags: TTagsBase; Name: PChar; FileName: PChar)
  : TCoverArt;
  begin
  Result := Tags.AddCoverArt(Name, FileName);
  end;

  function Tags_DeleteCoverArt(Tags: TTagsBase; Index: Integer): Boolean;
  begin
  Result := Tags.DeleteCoverArt(Index);
  end;

  procedure Tags_DeleteAllCoverArts(Tags: TTagsBase);
  begin
  Tags.DeleteAllCoverArts;
  end;

  function Tags_Assign(Tags: TTagsBase; Source: TTagsBase): Boolean;
  begin
  Result := Tags.Assign(Source);
  end;

  function Tags_SaveAPEv2Tag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveAPEv2Tag(FileName);
  end;

  function Tags_SaveFlacTag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveFlacTag(FileName);
  end;

  function Tags_SaveID3v1Tag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveID3v1Tag(FileName);
  end;

  function Tags_SaveID3v2Tag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveID3v2Tag(FileName);
  end;

  function Tags_SaveMP4Tag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveMP4Tag(FileName);
  end;

  function Tags_SaveOggVorbisAndOpusTag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveOggVorbisAndOpusTag(FileName);
  end;

  function Tags_SaveWAVTag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveWAVTag(FileName);
  end;

  {$IFDEF MSWINDOWS}

  function Tags_SaveWMATag(Tags: TTagsBase; FileName: PChar): Integer;
  begin
  Result := Tags.SaveWMATag(FileName);
  end;

  {$ENDIF}

  procedure Tags_LoadAPEv2Tags(Tags: TTagsBase);
  begin
  Tags.LoadAPEv2Tags;
  end;

  procedure Tags_LoadFlacTags(Tags: TTagsBase);
  begin
  Tags.LoadFlacTags;
  end;

  procedure Tags_LoadOggVorbisAndOpusTags(Tags: TTagsBase);
  begin
  Tags.LoadOggVorbisAndOpusTags;
  end;

  procedure Tags_LoadID3v1Tags(Tags: TTagsBase);
  begin
  Tags.LoadID3v1Tags;
  end;

  procedure Tags_LoadID3v2Tags(Tags: TTagsBase);
  begin
  Tags.LoadID3v2Tags;
  end;

  procedure Tags_LoadMP4Tags(Tags: TTagsBase);
  begin
  Tags.LoadMP4Tags;
  end;

  procedure Tags_LoadWAVTags(Tags: TTagsBase);
  begin
  Tags.LoadWAVTags;
  end;

  {$IFDEF MSWINDOWS}

  procedure Tags_LoadWMATags(Tags: TTagsBase);
  begin
  Tags.LoadWMATags;
  end;

  {$ENDIF}

  // TStrings
  procedure Tags_LoadNullTerminatedStrings(Tags: TTagsBase; TagType: PChar;
  TagList: TStrings);
  begin
  Tags.LoadNullTerminatedStrings(TagType, TagList);
  end;

  // TStrings
  procedure Tags_LoadNullTerminatedWAVRIFFINFOStrings(Tags: TTagsBase;
  TagList: TStrings);
  begin
  Tags.LoadNullTerminatedWAVRIFFINFOStrings(TagList);
  end;

  function Tags_PictureStreamType(Tags: TTagsBase; var buffer: PByte; size: Int64)
  : TTagPictureFormat;
  var
  Stream: TMemoryStream;
  begin
  Stream := TMemoryStream.Create;
  Stream.Write(buffer[0], size);
  Result := Tags.PictureStreamType(Stream);
  end;

  function Tags_PicturePointerType(Tags: TTagsBase; Picture: pointer)
  : TTagPictureFormat;
  begin
  Result := Tags.PicturePointerType(Picture);
  end;

  {$ENDREGION}

  function RemoveTagsFromFile2(FileName: PChar;
  TagType: TTagType = ttAutomatic): Integer;
  begin
  RemoveTagsFromFile(FileName, TagType);
  end;

  function TagsLibraryVersion: Cardinal;
  begin
  Result := TAGSLIBRARY_VERSION;
  end;

  function TagsLibraryErrorCode2String2(ErrorCode: Integer): PChar;
  begin
  Result := PChar(TagsLibraryErrorCode2String(ErrorCode));
  end; *)
{$ENDREGION}

end.
