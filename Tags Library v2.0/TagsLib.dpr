library TagsLib;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  System.SysUtils,
  System.Classes,
  TagsLibExports in 'TagsLibExports.pas',
  ID3v1Library in 'Modules\ID3v1Library.pas',
  TagsLibraryDefs in 'Modules\TagsLibraryDefs.pas',
  CommonTypes in 'Modules\CommonTypes.pas',
  ExportDecl in 'ExportDecl.pas',
  Logger in 'Logger.pas';

{$R *.res}
function DllMessage(text: PChar): PWideChar; export; stdcall;
  var str: String;
begin
  str := text;
  WriteLn(str);
  Result := PWideChar(str + ' World!');
end;

exports DllMessage;
(*

exports SourceAudioAttributes_Create;
exports SourceAudioAttributes_Destroy;
exports SourceAudioAttributes_get_Channels;
exports SourceAudioAttributes_get_SamplesPerSec;
exports SourceAudioAttributes_get_BitsPerSample;
exports SourceAudioAttributes_get_PlayTime;
exports SourceAudioAttributes_get_SampleCount;
exports SourceAudioAttributes_get_BitRate;
exports SourceAudioAttributes_get_Parent;
exports SourceAudioAttributes_set_Parent;

exports Tags_get_FileName;
exports Tags_get_Loaded;
exports Tags_get_Tags;
exports Tags_get_CoverArts;
exports Tags_get_SourceAudioAttributes;
exports Tags_get_UpperCaseFieldNamesToWrite;
exports Tags_get_Size;

exports Tags_set_FileName;
exports Tags_set_Loaded;
exports Tags_set_Tags;
exports Tags_set_CoverArts;
exports Tags_set_SourceAudioAttributes;
exports Tags_set_UpperCaseFieldNamesToWrite;

exports Tags_Create;
exports Tags_Destroy;
exports Tags_LoadFromFile;
exports Tags_LoadFromBASS;
exports Tags_SaveToFile;
exports Tags_LoadTags;
exports Tags_Add;
exports Tags_Tag;
exports Tags_GetTag;
exports Tags_SetTag;
exports Tags_SetList;
exports Tags_Delete;
exports Tags_Delete2;
exports Tags_Remove;
exports Tags_DeleteAllTags;
exports Tags_Clear;
exports Tags_Count;
exports Tags_Exists;
exports Tags_TypeCount;
exports Tags_RemoveEmptyTags;
exports Tags_CoverArtCount;
exports Tags_CoverArt;
exports Tags_AddCoverArt;
exports Tags_AddCoverArt2;
exports Tags_AddCoverArt3;
exports Tags_DeleteCoverArt;
exports Tags_DeleteAllCoverArts;
exports Tags_Assign;
exports Tags_SaveAPEv2Tag;
exports Tags_SaveFlacTag;
exports Tags_SaveID3v1Tag;
exports Tags_SaveID3v2Tag;
exports Tags_SaveMP4Tag;
exports Tags_SaveOggVorbisAndOpusTag;
exports Tags_SaveWAVTag;
{$IFDEF MSWINDOWS}
exports Tags_SaveWMATag;
{$ENDIF}
exports Tags_LoadAPEv2Tags;
exports Tags_LoadFlacTags;
exports Tags_LoadOggVorbisAndOpusTags;
exports Tags_LoadID3v1Tags;
exports Tags_LoadID3v2Tags;
exports Tags_LoadMP4Tags;
exports Tags_LoadWAVTags;
{$IFDEF MSWINDOWS}
exports Tags_LoadWMATags;
{$ENDIF}
exports Tags_LoadNullTerminatedStrings;
exports Tags_LoadNullTerminatedWAVRIFFINFOStrings;
exports Tags_PictureStreamType;
exports Tags_PicturePointerType;

exports TagsLibraryErrorCode2String;
exports RemoveTagsFromFile2;
exports TagsLibraryErrorCode2String2;

exports ID3v1TagLibraryErrorCodeToTagsLibraryErrorCode;
exports ID3v2TagLibraryErrorCodeToTagsLibraryErrorCode;
exports FlacTagLibraryErrorCodeToTagsLibraryErrorCode;
exports MP4TagLibraryErrorCodeToTagsLibraryErrorCode;
exports APEv2TagLibraryErrorCodeToTagsLibraryErrorCode;
exports OpusVorbisTagLibraryErrorCodeToTagsLibraryErrorCode;
exports WAVTagLibraryErrorCodeToTagsLibraryErrorCode;
{$IFDEF MSWINDOWS}
exports WMATagLibraryErrorCodeToTagsLibraryErrorCode;
{$ENDIF}

exports TagsLibraryVersion;    *)
begin
end.
