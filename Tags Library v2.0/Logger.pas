unit Logger;

interface
procedure Log(Message: String);

implementation
procedure Log(Message: String);
var
  myFile : TextFile;
begin
  //Exit;
  AssignFile(myFile, 'TestLog.log');
  Append(myFile);
  WriteLn(myFile, ' --> Native: ', Message);
  CloseFile(myFile);
end;
end.
