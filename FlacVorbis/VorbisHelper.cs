﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TagReader;
using Un4seen.Bass.AddOn.Tags;

namespace TagReader.FlacVorbis
{
    public static class VorbisHelper
    {
        private const string VENDOR_NAME = "Belfegor Inc.";

        private static Dictionary<string, string> tagNames = new Dictionary<string, string>()
        {
            {"Title", "title"},
            {"Artist", "artist"},
            {"Album", "album"},
            {"Year", "year"},
            {"TrackNumber", "tracknumber"},
            {"TrackTotal", "tracktotal"},
            {"Genre", "genre"},
            {"Copyright", "copyright"},
            {"Date", "date"},
            {"Comment", "comment"},
            {"Composer", "composer"},
            {"DiscNumber", "discnumber"},
            {"DiscTotal", "disctotal"},
            {"Publisher", "organization"}
        };

        public static VorbisComment ReadVorbisComment(this Stream stream)
        {
            VorbisComment result = new VorbisComment();

            int vendor_length = stream.ReadLength(4);
            stream.Position += vendor_length;
            int user_comment_list_length = stream.ReadLength(4);

            for (int i = 0; i < user_comment_list_length; i++)
            {
                int length = stream.ReadLength(4);

                byte[] buffer = new byte[length];
                stream.Read(buffer, 0, length);

                string content = Encoding.UTF8.GetString(buffer);
                int eqPos = content.IndexOf('=');

                if (eqPos <= 0)
                {
                    continue;
                }

                var parts = content.Split(new[] { '=' }, 2);
                //string id = content
                SetData(parts[0].ToLower(), parts[1], result);
            }

            stream.ReadByte();
            return result;
        }

        public static byte[] GetBytes(VorbisComment tags)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                byte[] buffer = null;
                buffer = Encoding.UTF8.GetBytes(VENDOR_NAME);
                stream.WriteLength(buffer.Length, 4, false);
                stream.Write(buffer, 0, buffer.Length);
                int user_comment_list_length_position = (int)stream.Position;
                int user_comment_list_length = 0;
                stream.Position += 4;

                Type type = typeof(TagsBase);
                var properties = type.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);

                foreach (var property in properties)
                {
                    string propName = property.Name;
                    string id = tagNames
                        .Where(tn => tn.Key == propName)
                        .Select(tn => tn.Value).FirstOrDefault();

                    if (id != null)
                    {
                        object data = property.GetValue(tags, null);

                        if (data == null)
                        {
                            continue;
                        }

                        user_comment_list_length++;
                        string stringData = id.ToUpper() + "=" + data.ToString();
                        buffer = Encoding.UTF8.GetBytes(stringData);
                        stream.WriteLength(buffer.Length, 4, false);
                        stream.Write(buffer, 0, buffer.Length);
                    }
                }

                foreach (var customTag in tags.GetCustomFields())
                {
                    object data = customTag.Value.Data;

                    if (data == null)
                    {
                        continue;
                    }

                    user_comment_list_length++;
                    string stringData = customTag.Key.ToUpper() + "=" + data.ToString();
                    buffer = Encoding.UTF8.GetBytes(stringData);
                    stream.WriteLength(buffer.Length, 4, false);
                    stream.Write(buffer, 0, buffer.Length);
                }

                stream.WriteByte(0x01);
                stream.Position = user_comment_list_length_position;
                stream.WriteLength(user_comment_list_length, 4, false);

                return stream.ToArray();
            }
        }

        private static byte[] GetBytes(int value, bool invert = false)
        {
            byte[] result = new byte[4];
            result = BitConverter.GetBytes(value);

            if (invert)
            {
                Array.Reverse(result);
            }

            return result;
        }

        private static void SetData(string fieldName, string value, VorbisComment tags)
        {
            if (fieldName != "APIC")
            {
                switch (fieldName)
                {
                    case "title":
                        tags.Title = value;
                        break;
                    case "artist":
                        tags.Artist = value;
                        break;
                    case "album":
                        tags.Album = value;
                        break;
                    case "year":
                        if (value == null)
                        {
                            tags.Year = null;
                        }
                        else
                        {
                            tags.Year = short.Parse(value);
                        }
                        break;
                    case "tracknumber":
                        tags.TrackNumber = value;
                        break;
                    case "tracktotal":
                        tags.TrackTotal = value;
                        break;
                    case "genre":
                        tags.Genre = value;
                        break;
                    case "copyright":
                        tags.Copyright = value;
                        break;
                    case "date":
                        tags.Date = value;
                        //if (value == null)
                        //{
                        //    tags.Date = null;
                        //}

                        //try
                        //{
                        //    tags.Date = Convert.ToDateTime(value);
                        //}
                        //catch (FormatException)
                        //{
                        //    try
                        //    {
                        //        tags.Date = new DateTime(Convert.ToInt32(value), 1, 1);
                        //    }
                        //    catch
                        //    {
                        //        tags.Date = null;
                        //    }
                        //}
                        break;
                    case "comment":
                        tags.Comment = value;
                        break;
                    case "composer":
                        tags.Composer = value;
                        break;
                    case "discnumber":
                        tags.DiscNumber = value;
                        break;
                    case "disctotal":
                        tags.DiscTotal = value;
                        break;
                    case "organization":
                        tags.Publisher = value;
                        break;
                    case "version":
                        tags.Version = value;
                        break;
                    case "performer":
                        tags.Performer = value;
                        break;
                    case "description":
                        tags.Description = value;
                        break;
                    case "license":
                        tags.License = value;
                        break;
                    case "location":
                        tags.Location = value;
                        break;
                    case "contact":
                        tags.Contact = value;
                        break;
                    default:
                        tags.AddCustomField(fieldName, FieldType.Text);
                        tags.SetCustomField(fieldName, value);
                        break;
                }
            }
            else
            {
                //tags.Picture = frame.Data;
            }
        }

        public static VorbisComment ToVorbis(this TAG_INFO bassTags)
        {
            VorbisComment result = new VorbisComment();

            if (bassTags.NativeTags != null)
            {
                foreach (var tag in bassTags.NativeTags)
                {
                    var parts = tag.Split(new[] { '=' });

                    SetData(parts[0].ToLower(), parts[1], result);
                }
            }

            return result;
        }
    }
}
