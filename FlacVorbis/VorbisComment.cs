﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagReader.FlacVorbis
{
    public class VorbisComment : TagsBase
    {
        public VorbisComment()
        {
            AddField("version", FieldType.Text);
            AddField("performer", FieldType.Text);
            AddField("description", FieldType.Text);
            AddField("license", FieldType.Text);
            AddField("location", FieldType.Text);
            AddField("contact", FieldType.Text);
        }

        public string Version
        {
            get
            {
                return (string)this["version"];
            }
            set
            {
                this["version"] = value;
            }
        }

        public string Performer
        {
            get
            {
                return (string)this["performer"];
            }
            set
            {
                this["performer"] = value;
            }
        }

        public string Description
        {
            get
            {
                return (string)this["description"];
            }
            set
            {
                this["description"] = value;
            }
        }

        public string License
        {
            get
            {
                return (string)this["license"];
            }
            set
            {
                this["license"] = value;
            }
        }

        public string Location
        {
            get
            {
                return (string)this["location"];
            }
            set
            {
                this["location"] = value;
            }
        }

        public string Contact
        {
            get
            {
                return (string)this["contact"];
            }
            set
            {
                this["contact"] = value;
            }
        }
    }
}
