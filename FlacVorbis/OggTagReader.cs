﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using DamienG.Security.Cryptography;
using System.Text;

namespace TagReader.FlacVorbis
{
    public class OggTagReader : ITagReader
    {
        private enum PacketType : byte
        {
            Identification = 0x01,
            Comment = 0x03,
            Setup = 0x05
        }

        [Flags]
        private enum HeaderTypeFlags
        {
            Default = 0x00,
            ContinuedPacket = 0x01,
            FirstPage = 0x02,
            LastPage = 0x04
        }

        private class PageHeader
        {
            public HeaderTypeFlags Flags { get; set; }
            public byte[] AbsoluteGranulePosition { get; set; }
            public byte[] SerialNumber { get; set; }
            public int PageSequenceNumber { get; set; }
            public byte[] Checksum { get; set; }
            public int Length { get; set; }
            public int PageDataPosition { get; set; }
            public PacketType Type { get; set; }
        }

        public TagsBase GetTags(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                Crc32 crc = new Crc32();
                var hash = crc.ComputeHash(stream);
                return GetTags(stream);
            }
        }

        public TagsBase GetTags(System.IO.Stream stream)
        {
            VorbisComment result = null;
            stream.Position = 0;
            var headers = GetHeaders(stream);
            CheckHeader(headers[0], PacketType.Identification);
            CheckHeader(headers[1], PacketType.Comment);
            CheckHeader(headers[2], PacketType.Setup);
            stream.Position = headers[1].PageDataPosition;
            result = stream.ReadVorbisComment();

            return result;
        }

        private IList<PageHeader> GetHeaders(Stream stream)
        {
            PageHeader[] headers = new PageHeader[3];
            int index = 0;

            do
            {
                byte[] headerData = new byte[27];

                if (stream.Read(headerData, 0, 27) == 0)
                {
                    return null;
                }

                string capture_pattern = Encoding.ASCII.GetString(headerData, 0, 4);

                if (string.Compare(capture_pattern, "OggS", false) == 0)
                {
                    PageHeader header = new PageHeader();
                    int segment_table_size = headerData[26];

                    header.Flags = (HeaderTypeFlags)headerData[5];
                    header.AbsoluteGranulePosition = new byte[8];
                    Array.Copy(headerData, 6, header.AbsoluteGranulePosition, 0, 8);
                    header.SerialNumber = new byte[4];
                    Array.Copy(headerData, 14, header.SerialNumber, 0, 4);
                    header.PageSequenceNumber = BitConverter.ToInt32(headerData, 18);

                    if (header.PageSequenceNumber != index)
                    {
                        throw new InvalidOperationException("Decoding error");
                    }

                    header.Checksum = new byte[4];
                    Array.Copy(headerData, 22, header.Checksum, 0, 4);
                    header.Length = -7;

                    byte[] segment_table = new byte[segment_table_size];
                    stream.Read(segment_table, 0, segment_table_size);
                    int allDataLength = segment_table.Sum(s => (int)s);
                    byte[] segmentData = new byte[allDataLength];

                    for (int i = 0; i < segment_table_size; i++)
                    {
                        stream.Read(segmentData, 0, segment_table[i]);
                        byte packtype = segmentData[0];
                        string vorbis = Encoding.ASCII.GetString(segmentData, 1, 6);

                        if (Enum.IsDefined(typeof(PacketType), packtype) && vorbis == "vorbis")
                        {
                            if (i > 0)
                            {
                                header.Checksum = null;
                                headers[index] = header;

                                header = new PageHeader();
                                header.SerialNumber = new byte[4];
                                header.Length = -7;
                                header.PageSequenceNumber = headers[index].PageSequenceNumber + 1;
                                Array.Copy(headers[index].SerialNumber, header.SerialNumber, 4);
                                index++;
                            }

                            header.PageDataPosition = (int)stream.Position - segment_table[i] + 7;
                            header.Type = (PacketType)packtype;
                        }

                        header.Length += segment_table[i];
                    }

                    headers[index++] = header;
                }
                else
                {
                    break;
                }
            } while (index < 3);

            return headers;
        }

        private void CheckHeader(PageHeader header, PacketType type)
        {
            if (header == null || header.Type != type)
            {
                throw new InvalidOperationException("Decoding error");
            }
        }

        public void WriteTags(string fileName, TagsBase tags)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                WriteTags(stream, tags);
            }
        }

        public void WriteTags(System.IO.Stream stream, TagsBase tags)
        {
            stream.Position = 0;

            var headers = GetHeaders(stream);
            CheckHeader(headers[0], PacketType.Identification);
            CheckHeader(headers[1], PacketType.Comment);
            CheckHeader(headers[2], PacketType.Setup);

            byte[] initData = new byte[headers[0].Length];
            stream.Position = headers[0].PageDataPosition;
            stream.Read(initData, 0, headers[0].Length);

            byte[] setupData = new byte[headers[2].Length];
            stream.Position = headers[2].PageDataPosition;
            stream.Read(setupData, 0, headers[2].Length);
            int restDataLength = (int)(stream.Length - stream.Position);
            byte[] restData = new byte[restDataLength];
            stream.Read(restData, 0, restDataLength);
            byte[] commentData = VorbisHelper.GetBytes(tags as VorbisComment);

            stream.Position = 0;

            using (FileStream s = new FileStream(@"d:\temp.txt", FileMode.Truncate, FileAccess.Write))
            {
                var crc = headers[0].Checksum;
                headers[0].Checksum = new byte[4];
                WriteHeader(s, headers[0], initData);
                headers[0].Checksum = crc;
            }

            WriteHeader(stream, headers[0], initData);

            stream.Position = headers[0].PageDataPosition + headers[0].Length;
            headers[1].Checksum = null;
            WriteHeader(stream, headers[1], commentData);
            WriteHeader(stream, headers[2], setupData);
            stream.Write(restData, 0, restDataLength);
            long newLength = stream.Position;

            if (stream is FileStream)
            {
                (stream as FileStream).SetLength(newLength);
            }
        }

        private void WriteHeader(Stream stream, PageHeader header, byte[] data)
        {
            byte[] buffer;
            int headerPos = (int)stream.Position;
            stream.WriteString("OggS", Encoding.ASCII);
            stream.WriteByte(0);
            stream.WriteByte((byte)header.Flags);

            if (header.AbsoluteGranulePosition != null)
            {
                stream.Write(header.AbsoluteGranulePosition, 0, 8);
            }
            else
            {
                buffer = new byte[8];
                stream.Write(buffer, 0, 8);
            }

            stream.Write(header.SerialNumber, 0, 4);
            stream.Write(BitConverter.GetBytes(header.PageSequenceNumber), 0, 4);
            int checksumPos = (int)stream.Position;
            bool calcChecksum = false;

            if (header.Checksum != null)
            {
                //stream.Write(header.Checksum, 0, 4);
                buffer = new byte[4];
                stream.Write(buffer, 0, 4);
            }
            else
            {
                buffer = new byte[4];
                stream.Write(buffer, 0, 4);
                calcChecksum = true;
            }

            int dataLen = data.Length + 7;
            byte page_segments = (byte)Math.Ceiling(dataLen * 1.0 / 0xFF);
            byte lastSegmentValue = (byte)(dataLen % 0xFF);

            byte[] segment_table = new byte[page_segments];

            for (int i = 0; i < page_segments; i++)
            {
                segment_table[i] = 0xFF;
            }

            if (lastSegmentValue != 0)
            {
                segment_table[page_segments - 1] = lastSegmentValue;
            }

            stream.WriteByte(page_segments);
            stream.Write(segment_table, 0, page_segments);
            stream.WriteByte((byte)header.Type);
            stream.WriteString("vorbis", Encoding.ASCII);
            stream.Write(data, 0, data.Length);

            int endPos = (int)stream.Position;
            int allDataLen = (int)(endPos - headerPos);
            stream.Position = headerPos;
            CRC32 crc = new CRC32();
            byte[] hash = crc.ComputeHash(stream, allDataLen);

            stream.Position = checksumPos;

            if (!calcChecksum)
            {
                stream.Write(header.Checksum, 0, 4);
            }
            else
            {
                stream.Write(hash, 0, 4);
            }

            stream.Position = endPos;
        }

        public string Extension
        {
            get { return "ogg"; }
        }
    }
}
