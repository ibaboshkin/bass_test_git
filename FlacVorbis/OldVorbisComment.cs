﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagReader.FlacVorbis
{
    public class VorbisComment : TagsBase
    {
        public VorbisComment()
        {
            AddField("title", FieldType.Text);
            AddField("version", FieldType.Text);
            AddField("artist", FieldType.Text);
            AddField("album", FieldType.Text);
            AddField("performer", FieldType.Text);
            AddField("year", FieldType.Integer);
            AddField("description", FieldType.Text);
            AddField("tracknumber", FieldType.Byte);
            AddField("genre", FieldType.Text);
            AddField("copyright", FieldType.Text);
            AddField("license", FieldType.Text);
            AddField("date", FieldType.DateTime);
            AddField("location", FieldType.Text);
            AddField("contact", FieldType.Text);
            AddField("organization", FieldType.Text);
            AddField("comment", FieldType.Text);
        }

        public string Title
        {
            get
            {
                return (string)this["title"];
            }
            set
            {
                this["title"] = value;
            }
        }

        public string Version
        {
            get
            {
                return (string)this["version"];
            }
            set
            {
                this["version"] = value;
            }
        }

        public string Artist
        {
            get
            {
                return (string)this["artist"];
            }
            set
            {
                this["artist"] = value;
            }
        }

        public string Album
        {
            get
            {
                return (string)this["album"];
            }
            set
            {
                this["album"] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Performer
        {
            get
            {
                return (string)this["performer"];
            }
            set
            {
                this["performer"] = value;
            }
        }

        public short? Year
        {
            get
            {
                if (this["year"] == null)
                {
                    return null;
                }

                return Convert.ToInt16(this["year"]);
            }
            set
            {
                if (!value.HasValue)
                {
                    this["year"] = null;
                }
                else
                {
                    this["year"] = value;
                }
            }
        }

        public string Description
        {
            get
            {
                return (string)this["description"];
            }
            set
            {
                this["description"] = value;
            }
        }

        public string Comment
        {
            get
            {
                return (string)this["comment"];
            }
            set
            {
                this["comment"] = value;
            }
        }

        public byte? TrackNumber
        {
            get
            {
                if (this["tracknumber"] == null)
                {
                    return null;
                }

                return Convert.ToByte(this["tracknumber"]);
            }
            set
            {
                if (!value.HasValue)
                {
                    this["tracknumber"] = null;
                }
                else
                {
                    this["tracknumber"] = value;
                }
            }
        }

        public string Genre
        {
            get
            {
                return (string)this["genre"];
            }
            set
            {
                this["genre"] = value;
            }
        }

        public string Copyright
        {
            get
            {
                return (string)this["copyright"];
            }
            set
            {
                this["copyright"] = value;
            }
        }

        public string License
        {
            get
            {
                return (string)this["license"];
            }
            set
            {
                this["license"] = value;
            }
        }

        public string Organization
        {
            get
            {
                return (string)this["organization"];
            }
            set
            {
                this["organization"] = value;
            }
        }

        public DateTime? Date
        {
            get
            {
                if (this["date"] == null)
                {
                    return null;
                }

                DateTime? result = null;

                try
                {
                    result = Convert.ToDateTime(this["date"]);
                }
                catch (FormatException)
                {
                    result = new DateTime(Convert.ToInt32(this["date"]), 1, 1);
                }

                return result;
            }
            set
            {
                if (!value.HasValue)
                {
                    this["date"] = null;
                }
                else
                {
                    this["date"] = value;
                }
            }
        }

        public string Location
        {
            get
            {
                return (string)this["location"];
            }
            set
            {
                this["location"] = value;
            }
        }

        public string Contact
        {
            get
            {
                return (string)this["contact"];
            }
            set
            {
                this["contact"] = value;
            }
        }

        internal void SetNonCustomField(string key, object value)
        {
            this[key] = value;
        }
    }
}
