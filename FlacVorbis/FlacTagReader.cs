﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Tags;

namespace TagReader.FlacVorbis
{
    public class FlacTagReader : ITagReader
    {
        private enum MetadataBlockHeaderType : byte
        {
            StreamInfo = 0,
            Padding = 1,
            Application = 2,
            Seektable = 3,
            VorbisComment = 4,
            CUESheet = 5,
            Picture = 6,
            Invalid = 127
        }

        private class METADATA_BLOCK_HEADER
        {
            public bool IsLastMetadataBlock { get; set; }
            public MetadataBlockHeaderType Type { get; set; }
            public int Length { get; set; }
        }

        private class MetadataBlock
        {
            public METADATA_BLOCK_HEADER Header { get; set; }
            public byte[] Data { get; set; }

            public MetadataBlock()
            {

            }

            public MetadataBlock(METADATA_BLOCK_HEADER header, byte[] data)
            {
                Header = header;
                Data = data;
            }
        }

        private class METADATA_BLOCK_PICTURE
        {
            public PictureType PictureType { get; set; }
            public string MimeType { get; set; }
            public string Description { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public int ColorDepth { get; set; }
            public int NumberOfColors { get; set; }
            public byte[] Data { get; set; }
        }

        public TagsBase GetTags(string fileName)
        {
            //Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            //var res = Bass.BASS_PluginLoadDirectory(Path.GetDirectoryName(this.GetType().Assembly.Location));
            //TAG_INFO bassTags = BassTags.BASS_TAG_GetFromFile(fileName, true, true);

            //if (res != null)
            //{
            //    foreach (var item in res)
            //    {
            //        bool ok = Bass.BASS_PluginFree(item.Key);
            //    }
            //}

            //Bass.BASS_Free();
            //return bassTags.ToVorbis();
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return GetTags(stream);
            }
        }

        public TagsBase GetTags(System.IO.Stream stream)
        {
            VorbisComment result = null;

            stream.Seek(0, SeekOrigin.Begin);

            byte[] buffer = new byte[4];
            stream.Read(buffer, 0, 4);

            string flac = Encoding.ASCII.GetString(buffer);
            List<PictureInfo> pictures = new List<PictureInfo>();

            if (string.Compare(flac, "fLaC", false) == 0)
            {
                bool finished = false;

                do
                {
                    METADATA_BLOCK_HEADER metadataBlockHeader = ReadMetadataBlockHeader(stream);

                    switch (metadataBlockHeader.Type)
                    {
                        case MetadataBlockHeaderType.StreamInfo:
                            stream.Position += metadataBlockHeader.Length;
                            break;
                        case MetadataBlockHeaderType.VorbisComment:
                            result = stream.ReadVorbisComment();
                            break;
                        case MetadataBlockHeaderType.Picture:
                            METADATA_BLOCK_PICTURE pictureMetadata = ReadMetadataBlockPicture(stream);
                            pictures.Add(new PictureInfo(pictureMetadata.PictureType, pictureMetadata.Data));
                            break;
                        case MetadataBlockHeaderType.Invalid:
                            //throw new InvalidOperationException("Cannot read data");
                            finished = true;
                            break;
                        default:
                            stream.Position += metadataBlockHeader.Length;
                            continue;
                    }

                    if (metadataBlockHeader.IsLastMetadataBlock)
                    {
                        finished = true;
                        break;
                    }
                } while (!finished);

                if (result != null)
                {
                    result.Picture = pictures;
                }
            }

            return result;
        }

        public void WriteTags(string fileName, TagsBase tags)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                WriteTags(stream, tags);
            }
        }

        public void WriteTags(System.IO.Stream stream, TagsBase tags)
        {
            stream.Seek(0, SeekOrigin.Begin);

            byte[] buffer = new byte[4];
            stream.Read(buffer, 0, 4);

            string flac = Encoding.ASCII.GetString(buffer);

            if (string.Compare(flac, "fLaC", false) != 0)
            {
                return;
            }

#if Method1
            MemoryStream memStream = new MemoryStream();
            //long picPos = 0;

            do
            {
                METADATA_BLOCK_HEADER metadataBlockHeader = ReadMetadataBlockHeader(stream);

                if (metadataBlockHeader.Type == MetadataBlockHeaderType.Invalid)
                {
                    stream.Position -= 4;
                    break;
                }

                if (metadataBlockHeader.Type == MetadataBlockHeaderType.VorbisComment ||
                    metadataBlockHeader.Type == MetadataBlockHeaderType.Picture)
                {
                    stream.Position += metadataBlockHeader.Length;
                }
                else
                {
                    WriteMetadataBlockHeader(memStream, metadataBlockHeader);
                    buffer = new byte[metadataBlockHeader.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    memStream.Write(buffer, 0, buffer.Length);

                    if (metadataBlockHeader.Type == MetadataBlockHeaderType.StreamInfo && tags != null)
                    {
                        METADATA_BLOCK_HEADER header = null;
                        buffer = VorbisHelper.GetBytes(tags as VorbisComment);

                        if (buffer != null && buffer.Length > 0)
                        {
                            header = new METADATA_BLOCK_HEADER();
                            header.Length = buffer.Length;
                            header.Type = MetadataBlockHeaderType.VorbisComment;
                            header.IsLastMetadataBlock = false;
                            WriteMetadataBlockHeader(memStream, header);
                            memStream.Write(buffer, 0, buffer.Length);
                        }

                        if (tags.Picture != null && tags.Picture.Count > 0)
                        {
                            foreach (PictureInfo picture in tags.Picture)
                            {
                                header = new METADATA_BLOCK_HEADER();
                                header.Type = MetadataBlockHeaderType.Picture;
                                long pos = memStream.Position;
                                memStream.Position += 4;
                                int size = WritePictureMetadata(memStream, GetMetadataFromImage(picture));
                                memStream.Position = pos;
                                header.Length = size;
                                header.IsLastMetadataBlock = false;
                                WriteMetadataBlockHeader(memStream, header);
                                memStream.Position += size;
                            }

                        }
                    }
                }

                if (metadataBlockHeader.IsLastMetadataBlock)
                {
                    break;
                }
            } while (true);

            long toReadCount = stream.Length - stream.Position;
            buffer = new byte[toReadCount];
            stream.Read(buffer, 0, (int)toReadCount);
            memStream.Write(buffer, 0, buffer.Length);
            long newLength = memStream.Length;
            buffer = memStream.ToArray();
            stream.Position = 4;
            stream.Write(buffer, 0, buffer.Length);

            if (stream is FileStream)
            {
                (stream as FileStream).SetLength(newLength + 4);
            } 

            memStream.Close();
            memStream = null;
#endif
#if Method2
            List<MetadataBlock> blocks;
            int initTotalSize;
            int initSize = ReadMetadatBlocksExceptVorbisAndPicture(stream, out blocks, out initTotalSize);
            int remainingSize = initTotalSize - initSize;

            MemoryStream tempStream = new MemoryStream();

            METADATA_BLOCK_HEADER header = null;
            buffer = VorbisHelper.GetBytes(tags as VorbisComment);

            if (buffer != null && buffer.Length > 0)
            {
                header = new METADATA_BLOCK_HEADER();
                header.Length = buffer.Length;
                header.Type = MetadataBlockHeaderType.VorbisComment;
                header.IsLastMetadataBlock = false;
                WriteMetadataBlockHeader(tempStream, header);
                tempStream.Write(buffer, 0, buffer.Length);
            }

            if (tags.Picture != null && tags.Picture.Count > 0)
            {
                foreach (PictureInfo picture in tags.Picture)
                {
                    header = new METADATA_BLOCK_HEADER();
                    header.Type = MetadataBlockHeaderType.Picture;
                    long pos = tempStream.Position;
                    tempStream.Position += 4;
                    int size = WritePictureMetadata(tempStream, GetMetadataFromImage(picture));
                    tempStream.Position = pos;
                    header.Length = size;
                    header.IsLastMetadataBlock = false;
                    WriteMetadataBlockHeader(tempStream, header);
                    tempStream.Position += size;
                }

            }

            byte[] newData = tempStream.ToArray();
            tempStream.Close();
            tempStream = null;

            MetadataBlock padding = blocks
                .Where(b => b.Header.Type == MetadataBlockHeaderType.Padding)
                .FirstOrDefault();

            if (padding == null)
            {
                padding = new MetadataBlock();
                padding.Header = new METADATA_BLOCK_HEADER();
                padding.Header.Type = MetadataBlockHeaderType.Padding;
            }

            padding.Header.IsLastMetadataBlock = true;
            byte[] frames = null;
            padding.Header.Length = 4096;

            //if (newData.Length <= remainingSize + padding.Header.Length)
            //{
            //    padding.Header.Length += remainingSize - newData.Length;

            //    if (padding.Header.Length < 0)
            //    {
            //        throw new Exception("Error while writing file");
            //    }
            //}
            //else
            //{
            //    //padding.Header.Length = 0;
            //    int size = (int)(stream.Length - stream.Position);
            //    frames = new byte[size];
            //    stream.Read(frames, 0, size);
            //}

            int newLength = -1;

            //if (padding.Header.Length > 4096)
            {
                int size = (int)(stream.Length - stream.Position);
                frames = new byte[size];
                stream.Read(frames, 0, size);
                padding.Header.Length = 4096;
                newLength = initSize - padding.Data.Length + padding.Header.Length + newData.Length +
                    size + 4;
            }

            MetadataBlock streamInfo = blocks
                .Where(b => b.Header.Type == MetadataBlockHeaderType.StreamInfo)
                .FirstOrDefault();

            stream.Position = 4;
            WriteMetadataBlockHeader(stream, streamInfo.Header);
            stream.Write(streamInfo.Data, 0, streamInfo.Data.Length);
            stream.Write(newData, 0, newData.Length);

            foreach (var block in blocks
                .Where(b => b.Header.Type != MetadataBlockHeaderType.StreamInfo &&
                            b.Header.Type != MetadataBlockHeaderType.Padding))
            {
                block.Header.IsLastMetadataBlock = false;
                WriteMetadataBlockHeader(stream, block.Header);
                stream.Write(block.Data, 0, block.Data.Length);
            }

            if (padding.Data.Length != padding.Header.Length)
            {
                padding.Data = new byte[padding.Header.Length];
            }

            WriteMetadataBlockHeader(stream, padding.Header);
            stream.Write(padding.Data, 0, padding.Data.Length);

            if (frames != null)
            {
                stream.Write(frames, 0, frames.Length);
            }

            if (newLength > 0 && stream is FileStream)
            {
                (stream as FileStream).SetLength(newLength);
            }
#endif
#if Method3
            List<MetadataBlock> blocks;
            int initTotalSize;
            int initSize = ReadMetadatBlocksExceptVorbisAndPicture(stream, out blocks, out initTotalSize);
            int remainingSize = initTotalSize - initSize;

            MetadataBlock padding = blocks
                .Where(b => b.Header.Type == MetadataBlockHeaderType.Padding)
                .FirstOrDefault();

            if (padding == null)
            {
                padding = new MetadataBlock();
                padding.Header = new METADATA_BLOCK_HEADER();
                padding.Header.Type = MetadataBlockHeaderType.Padding;
            }

            padding.Header.IsLastMetadataBlock = true;
            byte[] frames = null;
            padding.Header.Length = 4096;
            int newLength = -1;
            int size = (int)(stream.Length - stream.Position);
            frames = new byte[size];
            stream.Read(frames, 0, size);
            padding.Header.Length = 4096;

            METADATA_BLOCK_HEADER header = null;

            MetadataBlock streamInfo = blocks
                .Where(b => b.Header.Type == MetadataBlockHeaderType.StreamInfo)
                .FirstOrDefault();
            stream.Position = 4;
            WriteMetadataBlockHeader(stream, streamInfo.Header);
            stream.Write(streamInfo.Data, 0, streamInfo.Data.Length);

            buffer = VorbisHelper.GetBytes(tags as VorbisComment);

            if (buffer != null && buffer.Length > 0)
            {
                header = new METADATA_BLOCK_HEADER();
                header.Length = buffer.Length;
                header.Type = MetadataBlockHeaderType.VorbisComment;
                header.IsLastMetadataBlock = false;
                WriteMetadataBlockHeader(stream, header);
                stream.Write(buffer, 0, buffer.Length);
            }

            if (tags.Picture != null && tags.Picture.Count > 0)
            {
                foreach (PictureInfo picture in tags.Picture)
                {
                    header = new METADATA_BLOCK_HEADER();
                    header.Type = MetadataBlockHeaderType.Picture;
                    long pos = stream.Position;
                    stream.Position += 4;
                    size = WritePictureMetadata(stream, GetMetadataFromImage(picture));
                    stream.Position = pos;
                    header.Length = size;
                    header.IsLastMetadataBlock = false;
                    WriteMetadataBlockHeader(stream, header);
                    stream.Position += size;
                }

            }

            newLength = (int)stream.Position + padding.Header.Length + frames.Length + 4;

            foreach (var block in blocks
                .Where(b => b.Header.Type != MetadataBlockHeaderType.StreamInfo &&
                            b.Header.Type != MetadataBlockHeaderType.Padding))
            {
                block.Header.IsLastMetadataBlock = false;
                WriteMetadataBlockHeader(stream, block.Header);
                stream.Write(block.Data, 0, block.Data.Length);
                newLength += block.Header.Length + 4;
            }

            if (padding.Data.Length != padding.Header.Length)
            {
                padding.Data = new byte[padding.Header.Length];
            }

            WriteMetadataBlockHeader(stream, padding.Header);
            stream.Write(padding.Data, 0, padding.Data.Length);

            if (frames != null)
            {
                stream.Write(frames, 0, frames.Length);
            }

            if (newLength > 0 && stream is FileStream)
            {
                (stream as FileStream).SetLength(newLength);
            }
#endif
        }

        private METADATA_BLOCK_HEADER ReadMetadataBlockHeader(Stream stream)
        {
            METADATA_BLOCK_HEADER result = new METADATA_BLOCK_HEADER();
            byte metadata = (byte)stream.ReadByte();
            result.IsLastMetadataBlock = (metadata & 0x80) != 0;
            result.Type = (MetadataBlockHeaderType)(metadata & 0x7F);
            result.Length = stream.ReadLength(3, true);
            return result;
        }

        private METADATA_BLOCK_PICTURE ReadMetadataBlockPicture(Stream stream)
        {
            METADATA_BLOCK_PICTURE metadata = new METADATA_BLOCK_PICTURE();
            int pt = stream.ReadLength(invert: true);
            metadata.PictureType = (PictureType)pt;
            int len = stream.ReadLength(invert: true);
            byte[] buffer = new byte[len];
            stream.Read(buffer, 0, len);
            metadata.MimeType = Encoding.ASCII.GetString(buffer);
            len = stream.ReadLength();

            if (len != 0)
            {
                buffer = new byte[len];
                stream.Read(buffer, 0, len);
                metadata.Description = Encoding.ASCII.GetString(buffer);
            }

            metadata.Width = stream.ReadLength(invert: true);
            metadata.Height = stream.ReadLength(invert: true);
            metadata.ColorDepth = stream.ReadLength(invert: true);
            metadata.NumberOfColors = stream.ReadLength(invert: true);
            len = stream.ReadLength(invert: true);
            metadata.Data = new byte[len];
            stream.Read(metadata.Data, 0, len);
            return metadata;
        }

        private int ReadMetadatBlocksExceptVorbisAndPicture(Stream stream, out List<MetadataBlock> blocks, out int totalSize)
        {
            blocks = new List<MetadataBlock>();
            int len = 0;
            totalSize = 0;

            do
            {
                METADATA_BLOCK_HEADER metadataBlockHeader = ReadMetadataBlockHeader(stream);
                byte[] buffer;

                if (metadataBlockHeader.Type == MetadataBlockHeaderType.Invalid)
                {
                    //int size = (int)(stream.Length - stream.Position);
                    //buffer = new byte[size];
                    //stream.Read(buffer, 0, size);
                    //len += size + 4;
                    //blocks.Add(new MetadataBlock(metadataBlockHeader, buffer));
                    stream.Position -= 4;
                    break;
                }

                totalSize += metadataBlockHeader.Length + 4;

                if (metadataBlockHeader.Type != MetadataBlockHeaderType.VorbisComment &&
                    metadataBlockHeader.Type != MetadataBlockHeaderType.Picture)
                {
                    buffer = new byte[metadataBlockHeader.Length];
                    stream.Read(buffer, 0, metadataBlockHeader.Length);
                    blocks.Add(new MetadataBlock(metadataBlockHeader, buffer));
                    len += metadataBlockHeader.Length + 4;
                }
                else
                {
                    stream.Position += metadataBlockHeader.Length;
                }

                if (metadataBlockHeader.IsLastMetadataBlock)
                {
                    break;
                }
            } while (true);

            return len;
        }

        private void WriteMetadataBlockHeader(Stream stream, METADATA_BLOCK_HEADER header)
        {
            byte first = (byte)((header.IsLastMetadataBlock ? 0x80 : 0x00)
                + (byte)header.Type);

            stream.WriteByte(first);
            stream.WriteLength(header.Length, 3, true);
        }

        private METADATA_BLOCK_PICTURE GetMetadataFromImage(PictureInfo picture)
        {
            METADATA_BLOCK_PICTURE metadata = new METADATA_BLOCK_PICTURE();

            metadata.MimeType = picture.Picture.GetImageMimeType();
            metadata.Width = picture.Picture.Width;
            metadata.Height = picture.Picture.Height;
            metadata.Data = picture.Picture.GetBytes();
            return metadata;
        }

        private int WritePictureMetadata(Stream stream, METADATA_BLOCK_PICTURE metadata)
        {
            int size = 32;
            byte[] buffer = new byte[4];
            buffer[3] = (byte)metadata.PictureType;
            stream.Write(buffer, 0, 4);
            buffer = Encoding.UTF8.GetBytes(metadata.MimeType);
            size += buffer.Length;
            stream.WriteLength(buffer.Length, 4, true);
            stream.Write(buffer, 0, buffer.Length);

            if (!string.IsNullOrEmpty(metadata.Description))
            {
                buffer = Encoding.UTF8.GetBytes(metadata.Description);
                size += buffer.Length;
                stream.WriteLength(buffer.Length, 4, true);
                stream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                stream.WriteLength(0, 4, true);
            }

            stream.WriteLength(metadata.Width, 4, true);
            stream.WriteLength(metadata.Height, 4, true);
            stream.WriteLength(metadata.ColorDepth, 4, true);
            stream.WriteLength(metadata.NumberOfColors, 4, true);
            stream.WriteLength(metadata.Data.Length, 4, true);
            stream.Write(metadata.Data, 0, metadata.Data.Length);
            size += metadata.Data.Length;
            return size;
        }

        public string Extension
        {
            get { return "flac"; }
        }
    }
}
