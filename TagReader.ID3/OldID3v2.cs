﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagReader.ID3
{
    public class ID3v2 : TagsBase
    {
        

        public ID3v2()
        {
            AddField("TIT2", FieldType.Text);
            AddField("TPE1", FieldType.Text);
            AddField("TALB", FieldType.Text);
            AddField("TYER", FieldType.Integer);
            AddField("TRCK", FieldType.Bin);
            AddField("TCON", FieldType.Text);
            AddField("TCOP", FieldType.Text);
            AddField("TDRL", FieldType.DateTime);
            AddField("COMM", FieldType.Text);
            AddField("TCOM", FieldType.Text);
            AddField("APIC", FieldType.Bin);
            AddField("TPOS", FieldType.Bin);
        }
            public string Title
        {
            get
            {
                return (string)this["TIT2"];
            }
            set
            {
                this["TIT2"] = value;
            }
        }

        public string Artist
        {
            get
            {
                return (string)this["TPE1"];
            }
            set
            {
                this["TPE1"] = value;
            }
        }

        public string Composer
        {
            get
            {
                return (string)this["TCOM"];
            }
            set
            {
                this["TCOM"] = value;
            }
        }

        public string Album
        {
            get
            {
                return (string)this["TALB"];
            }
            set
            {
                this["TALB"] = value;
            }
        }

        //public string Performer
        //{
        //    get
        //    {
        //        return (string)this["performer"];
        //    }
        //    set
        //    {
        //        this["performer"] = value;
        //    }
        //}

        public short? Year
        {
            get
            {
                if (this["TYER"] == null)
                {
                    return null;
                }

                return Convert.ToInt16(this["TYER"]);
            }
            set
            {
                if (!value.HasValue)
                {
                    this["TYER"] = null;
                }
                else
                {
                    this["TYER"] = value;
                }
            }
        }

        public string Comment
        {
            get
            {
                return (string)this["COMM"];
            }
            set
            {
                this["COMM"] = value;
            }
        }

        public TrackNum TrackNumber
        {
            get
            {
                if (this["TRCK"] == null/* || !(this["TRCK"] is TrackNum)*/)
                {
                    return null;
                }

                if (this["TRCK"] is string)
                {
                    return (TrackNum)(string)this["TRCK"];
                }
                else
                    if (this["TRCK"] is TrackNum)
                    {
                        return (TrackNum)this["TRCK"];
                    }
                    else
                    {
                        return null;
                    }
            }
            set
            {
                this["TRCK"] = value;
            }
        }

        public DiscNum DiskNumber
        {
            get
            {
                if (this["TPOS"] == null/* || !(this["TPOS"] is DiscNum)*/)
                {
                    return null;
                }

                if (this["TPOS"] is string)
                {
                    return (DiscNum)(string)this["TPOS"];
                }
                else
                    if (this["TRCK"] is TrackNum)
                    {
                        return (DiscNum)this["TPOS"];
                    }
                    else
                    {
                        return null;
                    }
            }
            set
            {
                this["TPOS"] = value;
            }
        }

        public string Genre
        {
            get
            {
                return (string)this["TCON"];
            }
            set
            {
                this["TCON"] = value;
            }
        }

        public string Copyright
        {
            get
            {
                return (string)this["TCOP"];
            }
            set
            {
                this["TCOP"] = value;
            }
        }

        public DateTime? Date
        {
            get
            {
                if (this["TDRL"] == null)
                {
                    return null;
                }

                DateTime? result = null;

                try
                {
                    result = Convert.ToDateTime(this["TDRL"]);
                }
                catch (FormatException)
                {
                    result = new DateTime(Convert.ToInt32(this["TDRL"]), 1, 1);
                }

                return result;
            }
            set
            {
                if (!value.HasValue)
                {
                    this["TDRL"] = null;
                }
                else
                {
                    this["TDRL"] = value;
                }
            }
        }

        public object Picture
        {
            get
            {
                return this["APIC"];
            }
            set
            {
                this["APIC"] = value;
            }
        }
        internal void SetNonCustomField(string key, object value)
        {
            this[key] = value;
        }
    }
}
