﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace TagReader.ID3
{
    public class ID3v1Reader : ITagReader
    {
        [StructLayout(LayoutKind.Explicit, Size = 128)]
        private unsafe struct UnmanagedID3v1
        {
            [FieldOffset(0)]
            private fixed byte tag[3];

            [FieldOffset(3)]
            private fixed byte title[30];

            [FieldOffset(33)]
            private fixed byte artist[30];

            [FieldOffset(63)]
            private fixed byte album[30];

            [FieldOffset(93)]
            private fixed byte year[4];

            [FieldOffset(97)]
            private fixed byte comment[30];

            [FieldOffset(127)]
            private byte genre;

            public static explicit operator UnmanagedID3v1(TagsBase source)
            {
                UnmanagedID3v1 t = new UnmanagedID3v1();
                "TAG".CopyToBytePtr(t.tag);

                if (!string.IsNullOrEmpty(source.Title))
                {
                    source.Title.CopyToBytePtr(t.title);
                }

                if (!string.IsNullOrEmpty(source.Artist))
                {
                    source.Artist.CopyToBytePtr(t.artist);
                }

                if (!string.IsNullOrEmpty(source.Album))
                {
                    source.Album.CopyToBytePtr(t.album);
                }

                if (!string.IsNullOrEmpty(source.Comment))
                {
                    source.Comment.CopyToBytePtr(t.comment);
                }

                if (source.TrackNumber != null && !string.IsNullOrEmpty(source.TrackNumber))
                {
                    t.comment[28] = 0;

                    try
                    {
                        byte num = byte.Parse(source.TrackNumber);
                        t.comment[28] = 0;
                        t.comment[28] = num;
                    }
                    catch { }
                }

                if (source.Year.HasValue)
                {
                    source.Year.Value.ToString().CopyToBytePtr(t.year);
                }

                if (!string.IsNullOrEmpty(source.Genre))
                {
                    t.genre = (byte)source.Genre.GetGenreByName();
                }

                return t;
            }
        }

        public TagsBase GetTags(string fileName)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return GetTags(stream);
            }
        }

        public TagsBase GetTags(System.IO.Stream stream)
        {
            ID3v1 result = null;

            stream.Seek(-128, SeekOrigin.End);

            if (ReadIdentificator(stream))
            {
                byte[] buffer = new byte[125];
                stream.Read(buffer, 0, 125);
                result = new ID3v1();
                result.Title = Encoding.ASCII.GetString(buffer, 0, 30).Trim('\0');
                result.Artist = Encoding.ASCII.GetString(buffer, 30, 30).Trim('\0');
                result.Album = Encoding.ASCII.GetString(buffer, 60, 30).Trim('\0');

                try
                {
                    result.Year = short.Parse(Encoding.ASCII.GetString(buffer, 90, 4).Trim('\0'));
                }
                catch { }

                int commentLength = 30;

                if (buffer[122] == 0)
                {
                    commentLength -= 2;
                    result.TrackNumber = buffer[123].ToString();
                }

                result.Comment = Encoding.ASCII.GetString(buffer, 94, commentLength).Trim('\0');
                result.Genre = ((Genre)buffer[124]).GetGenreName();
            }

            return result;
        }

        public void WriteTags(string fileName, TagsBase tags)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                WriteTags(stream, tags);
            }
        }

        public void WriteTags(System.IO.Stream stream, TagsBase tags)
        {
            UnmanagedID3v1 uID3V1 = (UnmanagedID3v1)tags;

            stream.Seek(-128, SeekOrigin.End);

            if (!ReadIdentificator(stream))
            {
                stream.Seek(0, SeekOrigin.End);
            }
            else
            {
                stream.Position -= 3;
            }

            int size = Marshal.SizeOf(uID3V1);
            byte[] buffer = new byte[size];
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(uID3V1, ptr, true);
            Marshal.Copy(ptr, buffer, 0, size);
            Marshal.FreeHGlobal(ptr);

            stream.Write(buffer, 0, size);
        }

        private bool ReadIdentificator(Stream stream)
        {
            byte[] buffer = new byte[3];
            stream.Read(buffer, 0, 3);
            return Encoding.ASCII.GetString(buffer) == "TAG";
        }

        public string Extension
        {
            get { return "mp3"; }
        }
    }
}
