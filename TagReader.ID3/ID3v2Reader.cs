﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Tags;

namespace TagReader.ID3
{
    public class ID3v2Reader : ITagReader
    {
        private const int CLUSTER_SIZE = 4096;

        #region Types
        [Flags]
        private enum HeaderFlags : byte
        {
            None = 0x00,
            HasFooter = 0x10,
            IsExperimental = 0x20,
            HasExtendedHeader = 0x40,
            Unsynchronisation = 0x80
        }

        [Flags]
        private enum FrameStatusFlags : byte
        {
            None = 0x00,
            Readonly = 0x10,
            FileAlter = 0x20,
            TagAlter = 0x40
        }

        [Flags]
        private enum FrameFormatFlags : byte
        {
            None = 0x00,
            DataLengthIndicator = 0x01,
            Unsynchronisation = 0x02,
            Encryption = 0x04,
            Compression = 0x08,
            Grouping = 0x40
        }

        [StructLayout(LayoutKind.Explicit, Size = 10)]
        private unsafe struct UnmanagedHeader
        {
            [FieldOffset(0)]
            private fixed byte tag[3];

            [FieldOffset(3)]
            private short version;

            [FieldOffset(5)]
            private HeaderFlags flags;

            [FieldOffset(6)]
            private fixed byte size[4];

            public static UnmanagedHeader CreateHeader(int size, short version = 0x0003, HeaderFlags flags = HeaderFlags.None)
            {
                UnmanagedHeader header = new UnmanagedHeader();
                "ID3".CopyToBytePtr(header.tag);
                header.version = version;
                header.flags = flags;
                header.size[3] = (byte)(size & 0x7F);
                header.size[2] = (byte)((size >> 7) & 0x7F);
                header.size[1] = (byte)((size >> 14) & 0x7F);
                header.size[0] = (byte)((size >> 21) & 0x7F);

                return header;
            }
        }

        [StructLayout(LayoutKind.Explicit, Size = 10)]
        private unsafe struct UnmanagedFrameHeader
        {
            [FieldOffset(0)]
            private fixed byte id[4];

            [FieldOffset(4)]
            private fixed byte size[4];

            [FieldOffset(5)]
            public FrameStatusFlags FrameStatusFlags;

            [FieldOffset(6)]
            public FrameFormatFlags FrameFormatFlags;

            public static UnmanagedFrameHeader CreateFramwHeader(string frameId, int size)
            {
                UnmanagedFrameHeader header = new UnmanagedFrameHeader();
                frameId.CopyToBytePtr(header.id);
                PtrHelper.CopyToBytePtr<int>(size, header.size, true);
                //header.size[3] = (byte)(size & 0x7F);
                //header.size[2] = (byte)((size >> 7) & 0x7F);
                //header.size[1] = (byte)((size >> 14) & 0x7F);
                //header.size[0] = (byte)((size >> 21) & 0x7F);

                return header;
            }
        }

        private enum EncodingType : byte
        {
            ISO = 0,
            UTF16 = 1,
            UTF16BE = 2,
            UTF8 = 3
        }

        private class ID3v2Header
        {
            public Version Version { get; set; }
            public bool Unsynchronisation { get; set; }
            public bool ExtendedHeader { get; set; }
            public bool ExperimentalIndicator { get; set; }
            public bool FooterPresent { get; set; }
            public int Size { get; set; }
        }

        private class ID3v2ExtendedHeader
        {
            public int Size { get; set; }
            public bool TagIsAnUpdate { get; set; }
            public bool CRCDataPresent { get; set; }
            public bool TagRestrictions { get; set; }
        }

        private class FrameHeader
        {
            public string Id { get; set; }
            public int Size { get; set; }
            public bool TagAlterPreservation { get; set; }
            public bool FileAlterPreservation { get; set; }
            public bool Readonly { get; set; }
            public bool GroupingIdentity { get; set; }
            public bool Compression { get; set; }
            public bool Encryption { get; set; }
            public bool Unsynchronisation { get; set; }
            public bool DataLengthIndicator { get; set; }
        }

        private class Frame
        {
            public FrameHeader Header { get; set; }

            public EncodingType Encoding { get; set; }

            public string Language { get; set; }

            public byte[] Data { get; set; }
        }
        #endregion

        #region Fields
        private Dictionary<string, string> tagNames = new Dictionary<string, string>()
        {
            {"Title", "TIT2"},
            {"Artist", "TPE1"},
            {"Album", "TALB"},
            {"Year", "TYER"},
            //{"TrackNumber", "TRCK"},
            {"Genre", "TCON"},
            {"Copyright", "TCOP"},
            {"Date", "TDRL"},
            {"Comment", "COMM"},
            {"Composer", "TCOM"},
            //{"DiscNumber", "TPOS"},
            {"Publisher", "TPUB"},
            {"Picture", "APIC"}
        };
        #endregion

        #region ITagReader
        public string Extension
        {
            get { return "mp3"; }
        }

        public TagsBase GetTags(string fileName)
        {
            //Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            //var res = Bass.BASS_PluginLoadDirectory(Path.GetDirectoryName(this.GetType().Assembly.Location));
            //TAG_INFO bassTags = BassTags.BASS_TAG_GetFromFile(fileName, true, true);

            //if (res != null)
            //{
            //    foreach (var item in res)
            //    {
            //        bool ok = Bass.BASS_PluginFree(item.Key);
            //    }
            //}

            //Bass.BASS_Free();

            //ID3v2 result = new ID3v2();

            //if (bassTags.NativeTags != null)
            //{
            //    foreach (var tag in bassTags.NativeTags)
            //    {
            //        var parts = tag.Split(new[] { '=' });

            //        AddTag(result, parts[0].ToUpper(), parts[1]);
            //    }
            //}

            //return result;

            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                return GetTags(stream);
            }
        }

        public TagsBase GetTags(System.IO.Stream stream)
        {
            ID3v2 result = null;

            stream.Position = 0;
            ID3v2Header header = ReadHeader(stream);

            if (header != null)
            {
                if (header.ExtendedHeader)
                {
                    ID3v2ExtendedHeader extendedHeader = ReadExtendedHeader(stream);
                    stream.Position += extendedHeader.Size;
                }

                result = new ID3v2();

                do
                {
                    Frame frame = ReadFrame(stream);

                    if (string.IsNullOrEmpty(frame.Header.Id))
                    {
                        break;
                    }

                    SetData(frame, result);
                } while (stream.Position < header.Size);
            }

            return result;
        }

        public void WriteTags(string fileName, TagsBase tags)
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                WriteTags(stream, tags);
            }
        }

        public void WriteTags(System.IO.Stream stream, TagsBase tags)
        {
            stream.Position = 0;
            ID3v2Header header = ReadHeader(stream);
            List<UnmanagedFrameHeader> frameHeaders;
            List<byte[]> data;
            int newSize = GetTagsDataSize(tags, out frameHeaders, out data);
            byte[] temp = null;
            int zerosLen = 0;


            if (header == null || header.Size < newSize + 10)
            {
                stream.Position = header != null ? header.Size : 0;
                temp = new byte[stream.Length - header.Size];
                stream.Read(temp, 0, temp.Length);
            }
            else
            {
                int k1 = (int)Math.Ceiling((newSize + 10.0) / CLUSTER_SIZE);
                int k2 = (int)Math.Ceiling((double)header.Size / CLUSTER_SIZE);

                if (k1 < k2 && stream is FileStream)
                {
                    zerosLen = k1 * CLUSTER_SIZE - newSize - 10;
                    newSize = k1 * CLUSTER_SIZE - 10;
                    stream.Position = header.Size;
                    temp = new byte[stream.Length - header.Size];
                    stream.Read(temp, 0, temp.Length);

                    (stream as FileStream).SetLength(temp.Length + newSize + 10);
                }
                else
                {
                    zerosLen = header.Size - newSize - 10;
                    newSize = header.Size - 10;
                }
            }

            stream.Position = 0;
            UnmanagedHeader unmanagedHeader = UnmanagedHeader.CreateHeader(newSize);
            WriteStructToStream(unmanagedHeader, stream);

            for (int i = 0; i < frameHeaders.Count; i++)
            {
                WriteStructToStream(frameHeaders[i], stream);
                stream.Write(data[i], 0, data[i].Length);
            }

            if (zerosLen > 0)
            {
                byte[] temp2 = new byte[zerosLen];
                stream.Write(temp2, 0, zerosLen);
            }

            if (temp != null)
            {
                stream.Write(temp, 0, temp.Length);
            }
            //stream.Flush();
        }
        #endregion

        private static string GetString(Frame frame)
        {
            if (frame.Data == null)
            {
                return null;
            }

            return GetString(frame.Encoding, frame.Data, 0, frame.Data.Length);
        }

        private static string GetString(EncodingType encoding, byte[] bytes, int index, int count)
        {
            if (bytes == null)
            {
                return null;
            }

            switch (encoding)
            {
                case EncodingType.ISO:
                    return Encoding.ASCII.GetString(bytes, index, count);
                case EncodingType.UTF16:
                    return Encoding.Unicode.GetString(bytes, index, count);
                case EncodingType.UTF16BE:
                    return Encoding.BigEndianUnicode.GetString(bytes, index, count);
                case EncodingType.UTF8:
                    return Encoding.UTF8.GetString(bytes, index, count);
                default:
                    return null;
            }
        }

        #region Read Tag Methods
        private void SetData(Frame frame, ID3v2 tags)
        {
            if (frame.Header.Id != "APIC")
            {
                string stringData = GetString(frame);

                AddTag(tags, frame.Header.Id, stringData);
            }
            else
            {
                EncodingType encoding = (EncodingType)frame.Data[0];

                int i = 1;

                while (frame.Data[i] != 00 && i < frame.Data.Length)
                {
                    i++;
                }

                string mime = GetString(encoding, frame.Data, 1, i - 1);
                byte pictureType = frame.Data[++i];
                int start = i;

                while (frame.Data[i] != 00 && i < frame.Data.Length)
                {
                    i++;
                }

                i++;

                tags.Picture.Add(new PictureInfo((PictureType)pictureType, frame.Data, i, frame.Data.Length - i));
            }
        }

        private static void AddTag(ID3v2 tags, string key, string stringData)
        {
            if (key != "APIC")
            {
                if (string.IsNullOrEmpty(stringData))
                {
                    return;
                }

                switch (key)
                {
                    case "TIT2":
                        tags.Title = stringData;
                        break;
                    case "TPE1":
                        tags.Artist = stringData;
                        break;
                    case "TALB":
                        tags.Album = stringData;
                        break;
                    case "TYER":
                        if (stringData == null)
                        {
                            tags.Year = null;
                        }
                        else
                        {
                            tags.Year = short.Parse(stringData);
                        }
                        break;
                    case "TRCK":
                        TrackNum trk = (TrackNum)stringData;
                        if (trk != null)
                        {
                            tags.TrackNumber = trk.TrackNumber;
                            tags.TrackTotal = trk.TracksTotal;
                        }
                        else
                        {
                            tags.TrackNumber = stringData;
                        }
                        break;
                    case "TCON":
                        if (stringData.First() == '(' && stringData.Last() == ')')
                        {
                            try
                            {
                                tags.Genre = ((Genre)int.Parse(stringData.Substring(1, stringData.Length - 2)))
                                    .GetGenreName();
                            }
                            catch
                            {
                                tags.Genre = stringData;
                            }
                        }
                        else
                        {
                            tags.Genre = stringData;
                        }
                        break;
                    case "TCOP":
                        tags.Copyright = stringData;
                        break;
                    case "TDRL":
                        tags.Date = stringData;
                        //if (stringData == null)
                        //{
                        //    tags.Date = null;
                        //}

                        //try
                        //{
                        //    tags.Date = Convert.ToDateTime(stringData);
                        //}
                        //catch (FormatException)
                        //{
                        //    try
                        //    {
                        //        tags.Date = new DateTime(Convert.ToInt32(stringData), 1, 1);
                        //    }
                        //    catch
                        //    {
                        //        tags.Date = null;
                        //    }
                        //}
                        break;
                    case "COMM":
                        tags.Comment = stringData;
                        break;
                    case "TCOM":
                        tags.Composer = stringData;
                        break;
                    case "TPOS":
                        DiskNum dsk = (DiskNum)stringData;
                        if (dsk != null)
                        {
                            tags.DiscNumber = dsk.DiscNumber;
                            tags.DiscTotal = dsk.DiscsTotal;
                        }
                        else
                        {
                            tags.DiscNumber = stringData;
                        }
                        break;
                    case "TPUB":
                        tags.Publisher = stringData;
                        break;
                    default:
                        try
                        {
                            tags.AddCustomField(key, FieldType.Text);
                        }
                        catch (InvalidOperationException)
                        {
                        }

                        tags.SetCustomField(key, stringData);
                        break;
                }
            }
        }

        private ID3v2Header ReadHeader(Stream stream)
        {
            ID3v2Header result = null;

            byte[] buffer = new byte[10];
            stream.Read(buffer, 0, 6);

            string id3 = Encoding.ASCII.GetString(buffer, 0, 3);

            if (id3 == "ID3")
            {
                result = new ID3v2Header();
                result.Version = new Version(buffer[3], buffer[4]);
                result.Unsynchronisation = (buffer[5] & 0x80) != 0;
                result.ExtendedHeader = (buffer[5] & 0x40) != 0;
                result.ExperimentalIndicator = (buffer[5] & 0x20) != 0;
                result.FooterPresent = (buffer[5] & 0x10) != 0;
                /// TODO: The ID3v2 tag size is the sum of the byte length of the extended
                /// header, the padding and the frames after unsynchronisation. If a
                /// footer is present this equals to ('total size' - 20) bytes, otherwise
                /// ('total size' - 10) bytes.
                //result.Size = stream.ReadLength(4, true);
                stream.Read(buffer, 0, 4);
                int size = buffer[0];
                size = size << 7;
                size += buffer[1];
                size = size << 7;
                size += buffer[2];
                size = size << 7;
                size += buffer[3];
                result.Size = size + 10;
            }

            return result;
        }

        private ID3v2ExtendedHeader ReadExtendedHeader(Stream stream)
        {
            ID3v2ExtendedHeader result = new ID3v2ExtendedHeader();

            result.Size = stream.ReadLength(invert: true) - 6;
            stream.ReadByte();
            byte flags = (byte)stream.ReadByte();

            result.TagIsAnUpdate = (flags & 0x40) != 0;
            result.CRCDataPresent = (flags & 0x20) != 0;
            result.TagRestrictions = (flags & 0x10) != 0;

            return result;
        }

        private FrameHeader ReadFrameHeader(Stream stream)
        {
            FrameHeader result = new FrameHeader();

            byte[] buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            result.Id = Encoding.ASCII.GetString(buffer).Trim('\0');
            //stream.Read(buffer, 0, 4);
            //int size = buffer[0];
            //size = size << 7;
            //size += buffer[1];
            //size = size << 7;
            //size += buffer[2];
            //size = size << 7;
            //size += buffer[3];
            //result.Size = size + 10;
            result.Size = stream.ReadLength(4, true);

            byte statusFlags = (byte)stream.ReadByte();
            result.TagAlterPreservation = (statusFlags & 0x40) != 0;
            result.FileAlterPreservation = (statusFlags & 0x20) != 0;
            result.Readonly = (statusFlags & 0x10) != 0;

            byte frameFormatFlags = (byte)stream.ReadByte();
            result.GroupingIdentity = (frameFormatFlags & 0x40) != 0;
            result.Compression = (frameFormatFlags & 0x08) != 0;
            result.Encryption = (frameFormatFlags & 0x04) != 0;
            result.Unsynchronisation = (frameFormatFlags & 0x02) != 0;
            result.DataLengthIndicator = (frameFormatFlags & 0x01) != 0;

            return result;
        }

        private Frame ReadFrame(Stream stream)
        {
            Frame result = new Frame();

            result.Header = ReadFrameHeader(stream);
            byte encodingByte = (byte)stream.ReadByte();
            int toRead = result.Header.Size;

            if (Enum.IsDefined(typeof(EncodingType), encodingByte))
            {
                result.Encoding = (EncodingType)encodingByte;
                toRead--;
            }
            else
            {
                result.Encoding = EncodingType.ISO;
                stream.Position--;
            }

            if (result.Encoding == EncodingType.UTF16 || result.Encoding == EncodingType.UTF16BE)
            {
                byte bom1 = (byte)stream.ReadByte();

                if (bom1 != 0xFF && bom1 != 0xFE)
                {
                    stream.Position += 8;
                    toRead -= 9;
                }
                else
                {
                    stream.Position++;
                    toRead -= 2;
                }
            }

            if (toRead < 0)
            {
                return result;
            }

            result.Data = new byte[toRead];
            stream.Read(result.Data, 0, toRead);

            return result;
        }
        #endregion

        #region Write Tags Methods
        private void WriteStructToStream(object value, Stream stream)
        {
            int size = Marshal.SizeOf(value);
            IntPtr ptr = Marshal.AllocHGlobal(size);
            byte[] buffer = new byte[size];

            Marshal.StructureToPtr(value, ptr, true);
            Marshal.Copy(ptr, buffer, 0, size);
            stream.Write(buffer, 0, size);
            Marshal.FreeHGlobal(ptr);
        }

        private int GetTagsDataSize(TagsBase tags, out List<UnmanagedFrameHeader> frameHeaders, out List<byte[]> data)
        {
            int size = 0;

            Type type = typeof(ID3v2);
            var properties = type.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);

            frameHeaders = new List<UnmanagedFrameHeader>();
            data = new List<byte[]>();
            IEnumerable<UnmanagedFrameHeader> tmpFrameHeaders;
            IEnumerable<byte[]> bytes;
            int s;

            if (properties == null)
            {
                return 0;
            }

            foreach (var property in properties)
            {
                string propName = property.Name;
                string id = tagNames
                    .Where(tn => tn.Key == propName)
                    .Select(tn => tn.Value).FirstOrDefault();

                if (id != null)
                {
                    s = GetFrame(id, property.GetValue(tags, null), out tmpFrameHeaders, out bytes);

                    if (s != 0)
                    {
                        size += s + Marshal.SizeOf(typeof(UnmanagedFrameHeader)) * tmpFrameHeaders.Count();
                        (frameHeaders as List<UnmanagedFrameHeader>).AddRange(tmpFrameHeaders);
                        (data as List<byte[]>).AddRange(bytes);
                    }
                }
            }

            s = GetTrackDiscFrames(tags, out tmpFrameHeaders, out bytes);

            if (s != 0)
            {
                size += s + Marshal.SizeOf(typeof(UnmanagedFrameHeader)) * tmpFrameHeaders.Count();
                (frameHeaders as List<UnmanagedFrameHeader>).AddRange(tmpFrameHeaders);
                (data as List<byte[]>).AddRange(bytes);
            }

            foreach (var customTag in tags.GetCustomFields())
            {
                s = GetFrame(customTag.Key, customTag.Value.Data, out tmpFrameHeaders, out bytes);

                if (s != 0)
                {
                    size += s + Marshal.SizeOf(typeof(UnmanagedFrameHeader)) * tmpFrameHeaders.Count();
                    (frameHeaders as List<UnmanagedFrameHeader>).AddRange(tmpFrameHeaders);
                    (data as List<byte[]>).AddRange(bytes);
                }
            }

            return size;
        }

        private int GetFrame(string id, object data, out IEnumerable<UnmanagedFrameHeader> frameHeaders, out IEnumerable<byte[]> outData)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException("Identificator should not be empty");
            }

            if (id.Length > 4)
            {
                throw new ArgumentException("Identificator cannot be longer than 4 characters");
            }

            if (data == null)
            {
                frameHeaders = null;
                outData = null;
                return 0;
            }

            frameHeaders = new List<UnmanagedFrameHeader>();
            outData = new List<byte[]>();
            byte[] dataBytes;
            int size = 0;

            if (id != "APIC")
            {
                string stringData = data.ToString();

                if (!string.IsNullOrEmpty(stringData))
                {
                    byte[] buffer = Encoding.Unicode.GetBytes(stringData);
                    size += buffer.Length + 3;
                    dataBytes = new byte[size];
                    dataBytes[0] = (byte)EncodingType.UTF16;
                    dataBytes[1] = 0xFF;
                    dataBytes[2] = 0xFE;
                    Array.Copy(buffer, 0, dataBytes, 3, buffer.Length);
                }
                else
                {
                    dataBytes = null;
                }
            }
            else
            {
                if (data is byte[])
                {
                    size = (data as byte[]).Length + 1;
                    dataBytes = new byte[size];
                    dataBytes[2] = 0xFE;
                    Array.Copy(data as byte[], 0, dataBytes, 1, size - 1);
                }
                else
                    if (data is Image)
                    {
                        Image img = (data as Image);
                        dataBytes = GetBytesFromImage(PictureType.CoverFront, img);
                        size = dataBytes.Length;
                    }
                    else
                        if (data is IEnumerable<PictureInfo>)
                        {
                            IEnumerable<PictureInfo> pictures = data as IEnumerable<PictureInfo>;

                            foreach (PictureInfo picture in pictures)
                            {
                                dataBytes = GetBytesFromImage(picture.PictureType, picture.Picture);
                                (frameHeaders as List<UnmanagedFrameHeader>).Add(UnmanagedFrameHeader.CreateFramwHeader(id, dataBytes.Length));
                                (outData as List<byte[]>).Add(dataBytes);
                                size += dataBytes.Length;
                            }

                            return size;
                        }
                        else
                        {
                            dataBytes = null;
                        }
            }

            (frameHeaders as List<UnmanagedFrameHeader>).Add(UnmanagedFrameHeader.CreateFramwHeader(id, size));
            (outData as List<byte[]>).Add(dataBytes);
            return size;
        }

        private int GetTrackDiscFrames(TagsBase tags, out IEnumerable<UnmanagedFrameHeader> frameHeaders, out IEnumerable<byte[]> outData)
        {
            frameHeaders = new List<UnmanagedFrameHeader>();
            outData = new List<byte[]>();
            byte[] dataBytes;
            int size = 0;

            string trk = new TrackNum(tags.TrackNumber, tags.TrackTotal).ToString();
            string dsk = new DiskNum(tags.DiscNumber, tags.DiscTotal).ToString();

            if (!string.IsNullOrEmpty(trk))
            {
                byte[] buffer = Encoding.Unicode.GetBytes(trk);
                size += buffer.Length + 3;
                dataBytes = new byte[size];
                dataBytes[0] = (byte)EncodingType.UTF16;
                dataBytes[1] = 0xFF;
                dataBytes[2] = 0xFE;
                Array.Copy(buffer, 0, dataBytes, 3, buffer.Length);
                (frameHeaders as List<UnmanagedFrameHeader>).Add(UnmanagedFrameHeader.CreateFramwHeader("TRCK", size));
                (outData as List<byte[]>).Add(dataBytes);
            }

            if (!string.IsNullOrEmpty(dsk))
            {
                byte[] buffer = Encoding.Unicode.GetBytes(dsk);
                size += buffer.Length + 3;
                dataBytes = new byte[size];
                dataBytes[0] = (byte)EncodingType.UTF16;
                dataBytes[1] = 0xFF;
                dataBytes[2] = 0xFE;
                Array.Copy(buffer, 0, dataBytes, 3, buffer.Length);
                (frameHeaders as List<UnmanagedFrameHeader>).Add(UnmanagedFrameHeader.CreateFramwHeader("TPOS", size));
                (outData as List<byte[]>).Add(dataBytes);
            }

            return size;
        }

        private byte[] GetBytesFromImage(PictureType type, Image img)
        {
            byte[] mime = Encoding.ASCII.GetBytes(img.GetImageMimeType());
            byte[] imageBytes = img.GetBytes();
            int size = mime.Length + imageBytes.Length + 4;
            byte[] dataBytes = new byte[size];
            dataBytes[0] = (byte)EncodingType.ISO;
            Array.Copy(mime, 0, dataBytes, 1, mime.Length);
            dataBytes[mime.Length + 2] = (byte)type;
            Array.Copy(imageBytes, 0, dataBytes, mime.Length + 4, imageBytes.Length);

            return dataBytes;
        }
        #endregion
    }
}
