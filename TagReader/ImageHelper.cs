﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace TagReader
{
    public static class ImageHelper
    {
        public static string GetImageMimeType(this Image img)
        {
            Type type = typeof(ImageFormat);
            var formatProp = type
                .GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                .Where(p => p.PropertyType == typeof(ImageFormat) && img.RawFormat.Guid == (p.GetValue(null, null) as ImageFormat).Guid)
                .FirstOrDefault();

            if (formatProp != null)
            {
                return "image/" + formatProp.Name.ToLower();
            }
            else
            {
                return string.Empty;
            }
        }

        public static Image CreateImageFromBytes(byte[] data, int index, int count, out Stream sourceStream)
        {
            if (data == null || data.Length <= 0)
            {
                throw new ArgumentException("Cannot create image from empty data", "data");
            }
            
            sourceStream = new MemoryStream(data, index, count);
            try
            {
                return Image.FromStream(sourceStream);
            }
            catch (Exception)
            {
                try
                {
                    Bitmap bmp = new Bitmap(sourceStream);
                    return bmp;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static byte[] GetBytes(this Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                return ms.ToArray();
            }
        }
    }
}
