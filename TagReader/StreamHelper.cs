﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TagReader
{
    public static class StreamHelper
    {
        public static int ReadLength(this Stream stream, byte count = 4, bool invert = false)
        {
            int bufferSize = ((int)(count / 2)) * 2 == count ? count : ((int)(count / 2) + 1) * 2;
            byte[] buffer = new byte[bufferSize];
            byte[] temp = new byte[count];
            stream.Read(temp, 0, count);
            //stream.Position += count;


            if (invert)
            {
                Array.Copy(temp, 0, buffer, bufferSize - count, count);
                Array.Reverse(buffer);
            }
            else
            {
                Array.Copy(temp, 0, buffer, 0, count);
            }

            return BitConverter.ToInt32(buffer, 0);
        }

        public static void WriteLength(this Stream stream, int length, byte count, bool invert)
        {
            byte[] buffer = BitConverter.GetBytes(length);

            if (count > buffer.Length)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if (invert)
            {
                Array.Reverse(buffer);
                stream.Write(buffer, buffer.Length - count, count);
            }
            else
            {
                stream.Write(buffer, 0, count);
            }
        }

        public static void WriteString(this Stream stream, string text, Encoding encoding)
        {
            byte[] buffer = encoding.GetBytes(text);
            stream.Write(buffer, 0, buffer.Length);
        }
    }
}
