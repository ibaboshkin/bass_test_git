﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TagReader
{
    public interface ITagReader
    {
        TagsBase GetTags(string fileName);
        TagsBase GetTags(Stream stream);
        string Extension { get; }
    }
}
