﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace TagReader
{
    public static class PtrHelper
    {
        public static unsafe void CopyToBytePtr(this string source, byte* dest)
        {
            Marshal.Copy(Encoding.ASCII.GetBytes(source), 0, (IntPtr)dest, source.Length);
        }

        public static unsafe void CopyToBytePtr<T>(T source, byte* dest, bool invert = false)
        {
            if (source is string)
            {
                (source as string).CopyToBytePtr(dest);
                return;
            }

            Type type = typeof(BitConverter);
            MethodInfo method = type.GetMethod("GetBytes", BindingFlags.Public | BindingFlags.Static,
                null, new[] { typeof(T) }, null);

            if (method == null)
            {
                throw new InvalidCastException(string.Format("Cannot convert {0} to byte*", typeof(T).FullName));
            }

            byte[] buffer = (byte[])method.Invoke(null, new object[] { source });

            if (invert)
            {
                Array.Reverse(buffer);
            }

            Marshal.Copy(buffer, 0, (IntPtr)dest, buffer.Length);
        }
    }
}
