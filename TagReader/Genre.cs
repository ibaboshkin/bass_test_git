﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace TagReader
{
    public enum Genre : byte
    {
        Blues = 0,
        [EnumMember(Value = "Classic Rock")]
        ClassicRock,
        Country,
        Dance,
        Disco,
        None = 255
    }

    public static class GenreHelper
    {
        public static string GetGenreName(this Genre genre)
        {
            Type type = typeof(Genre);
            FieldInfo fi = type.GetField(genre.ToString());

            if (fi == null)
            {
                return genre.ToString();
            }

            var attr = fi.GetCustomAttributes(typeof(EnumMemberAttribute), false).FirstOrDefault() as EnumMemberAttribute;

            if (attr != null)
            {
                return attr.Value;
            }

            return genre.ToString();
        }

        public static Genre GetGenreByName(this string genreName)
        {
            Type type = typeof(Genre);
            var fields = type.GetFields().Where(f=>!f.Attributes.HasFlag(FieldAttributes.SpecialName));

            if (fields == null && fields.Count() == 0)
            {
                return default(Genre);
            }

            return fields.Where(f =>
            {
                return string.Compare(((Genre)f.GetValue(null)).GetGenreName(), genreName, true) == 0;
            })
            .Select(f =>
            {
                return (Genre)f.GetValue(null);
            })
            .FirstOrDefault();
        }
    }
}
