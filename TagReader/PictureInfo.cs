﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace TagReader
{
    public enum PictureType : byte
    {
        Other = 0,
        Icon32 = 1,
        OtherIcon = 2,
        CoverFront = 3,
        CoverBack = 4,
        Leaflet = 5,
        Media = 6,
        LeadArtist = 7,
        Artist = 8,
        Conductor = 9,
        Band = 10,
        Composer = 11,
        Lyricist = 12,
        RecordingLocation = 13,
        DuringRecording = 14,
        DuringPerformance = 15,
        ScreenCapture = 16,
        BrightColouredFish = 17,
        Illustration = 18,
        Logotype = 19,
        Publisher = 20
    }

    public class PictureInfo : IDisposable
    {
        private Stream sourceStream;

        public PictureType PictureType { get; set; }

        public Image Picture { get; set; }

        public PictureInfo(PictureType type, Image image)
        {
            PictureType = type;
            Picture = image;
        }

        public PictureInfo(PictureType type, byte[] data, int index, int count)
        {
            PictureType = type;
            Picture = ImageHelper.CreateImageFromBytes(data, index, count, out sourceStream);
        }

        public PictureInfo(byte[] data, int index, int count)
            : this(default(PictureType), data, index, count)
        {

        }

        public PictureInfo(PictureType type, byte[] data)
            : this(type, data, 0, data.Length)
        { }

        public PictureInfo(byte[] data)
            : this(default(PictureType), data)
        {

        }

        ~PictureInfo()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (sourceStream != null)
            {
                sourceStream.Close();
                sourceStream = null;
            }
        }
    }
}
