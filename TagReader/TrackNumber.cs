﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagReader
{
    public class TrackNum
    {
        public string TrackNumber;
        public string TracksTotal;

        public TrackNum(int number)
        {
            TrackNumber = number.ToString();
            TracksTotal = string.Empty;
        }

        public TrackNum(string number, string total)
        {
            TrackNumber = number;
            TracksTotal = total;
        }

        public TrackNum(string text)
        {
            if (text.Contains('/'))
            {
                var parts = text.Split(new[] { '/' });

                if (parts.Length == 2)
                {
                    TrackNumber = parts[0];
                    TracksTotal = parts[1];
                }
                else
                    if (parts.Length == 1)
                    {
                        TrackNumber = string.Empty;
                        TracksTotal = parts[0];
                    }
                    else
                    {
                        TrackNumber = null;
                        TracksTotal = string.Empty;
                    }
            }
            else
            {
                TrackNumber = text;
                TracksTotal = string.Empty;
            }
        }

        public override string ToString()
        {
            return (!string.IsNullOrEmpty(TrackNumber) ? TrackNumber : string.Empty) +
                (!string.IsNullOrEmpty(TracksTotal) ? "/" + TracksTotal : string.Empty);
        }

        public static explicit operator TrackNum(string text)
        {
            return new TrackNum(text);
        }

        public static explicit operator string(TrackNum track)
        {
            return track.ToString();
        }
    }
}
