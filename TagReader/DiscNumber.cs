﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagReader
{
    public class DiskNum
    {
        public string DiscNumber;
        public string DiscsTotal;

        public DiskNum(string number, string total)
        {
            DiscNumber = number;
            DiscsTotal = total;
        }

        public DiskNum(string text)
        {
            if (text.Contains('/'))
            {
                var parts = text.Split(new[] { '/' });

                if (parts.Length == 2)
                {
                    DiscNumber = parts[0];
                    DiscsTotal = parts[1];
                }
                else
                    if (parts.Length == 1)
                    {
                        DiscNumber = string.Empty;
                        DiscsTotal = parts[0];
                    }
                    else
                    {
                        DiscNumber = string.Empty;
                        DiscsTotal = string.Empty;
                    }
            }
            else
            {
                DiscNumber = text;
                DiscsTotal = string.Empty;
            }
        }

        public override string ToString()
        {
            return (!string.IsNullOrEmpty(DiscNumber) ? DiscNumber : string.Empty) +
                (!string.IsNullOrEmpty(DiscsTotal) ? "/" + DiscsTotal : string.Empty);
        }

        public static implicit operator DiskNum(string text)
        {
            return new DiskNum(text);
        }

        public static explicit operator String(DiskNum track)
        {
            return track.ToString();
        }
    }
}
