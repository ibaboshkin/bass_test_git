﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TagReader
{
    public enum FieldType : byte
    {
        Text = 0,
        Integer = 1,
        Float = 2,
        Char = 3,
        Byte = 4,
        DateTime = 5,
        Bin
    }

    public class CustomFieldData
    {
        private object mData = null;
        private readonly FieldType mType;

        public string Name { get; private set; }

        public FieldType Type
        {
            get
            {
                return mType;
            }
        }

        public object Data
        {
            get
            {
                return mData;
            }
            set
            {
                CheckData(value);

                mData = value;
            }
        }

        private void CheckData(object value)
        {
            if(value == null)
            {
                return;
            }

            try
            {
                switch (Type)
                {
                    case FieldType.Byte:
                        Convert.ToByte(value);
                        break;
                    case FieldType.Integer:
                        Convert.ToInt32(value);
                        break;
                    case FieldType.Float:
                        Convert.ToDouble(value);
                        break;
                    case FieldType.Char:
                        Convert.ToChar(value);
                        break;
                    case FieldType.DateTime:
                        bool ok1 = false;
                        bool ok2 = false;

                        try
                        {
                            Convert.ToDateTime(value);
                            ok1 = true;
                        }
                        catch { }

                        try
                        {
                            Convert.ToInt32(value);
                            ok2 = true;
                        }
                        catch { }

                        if (!ok1 && !ok2)
                        {
                            throw new Exception();
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(string.Format("The value must be of type \"{0}\"", Type));
            }
        }

        public CustomFieldData(string name, FieldType type)
        {
            Name = name;
            mType = type;
        }
    }

    public class TagsBase
    {
        private Dictionary<string, CustomFieldData> mFields = new Dictionary<string, CustomFieldData>();

        #region Properties
        public virtual string Title
        {
            get;
            set;
        }

        public virtual string Artist
        {
            get;
            set;
        }

        public virtual string Composer
        {
            get;
            set;
        }

        public virtual string Album
        {
            get;
            set;
        }

        public virtual short? Year
        {
            get;
            set;
        }

        public virtual string Comment
        {
            get;
            set;
        }

        public virtual string TrackNumber
        {
            get;
            set;
        }

        public virtual string TrackTotal
        {
            get;
            set;
        }

        public virtual string DiscNumber
        {
            get;
            set;
        }

        public virtual string DiscTotal
        {
            get;
            set;
        }

        public virtual string Genre
        {
            get;
            set;
        }

        public virtual string Copyright
        {
            get;
            set;
        }

        public virtual string Date
        {
            get;
            set;
        }

        public virtual string Publisher { get; set; }

        public virtual List<PictureInfo> Picture
        {
            get;
            set;
        }

        public object this[string TagName]
        {
            get
            {
                CustomFieldData value;
                mFields.TryGetValue(TagName, out value);

                return value.Data;
            }
            set
            {
                if (mFields.ContainsKey(TagName))
                {
                    mFields[TagName].Data = value;
                }
                else
                {
                    throw new InvalidOperationException(string.Format("There is no \"{0}\" field", TagName));
                }
            }
        } 
        #endregion

        public TagsBase()
        {
            Picture = new List<PictureInfo>();
        }

        protected void AddField(string fieldName, FieldType fieldType)
        {
            if (mFields.ContainsKey(fieldName))
            {
                if (mFields[fieldName].Type != fieldType)
                {
                    throw new InvalidOperationException(string.Format("Cannot change type of field \"{0}\"", fieldName));
                }
            }
            else
            {
                mFields.Add(fieldName, new CustomFieldData(fieldName, fieldType));
            }
        }

        public void AddCustomField(string fieldName, FieldType fieldType)
        {
            if (mFields.ContainsKey(fieldName))
            {
                throw new InvalidOperationException(string.Format("Tag already contains field \"{0}\"", fieldName));
            }
            else
            {
                mFields.Add(fieldName, new CustomFieldData(fieldName, fieldType));
            }
        }

        public object GetCustomField(string fieldName)
        {
            if (!mFields.ContainsKey(fieldName))
            {
                throw new InvalidOperationException(string.Format("There is no \"{0}\" field", fieldName));
            }
            else
            {
                return mFields[fieldName].Data;
            }
        }

        public void SetCustomField(string fieldName, object value)
        {
            if (!mFields.ContainsKey(fieldName))
            {
                throw new InvalidOperationException(string.Format("There is no \"{0}\" field", fieldName));
            }
            else
            {
                mFields[fieldName].Data = value;
            }
        }

        public Dictionary<string, CustomFieldData> GetCustomFields()
        {
            return mFields;
        }
    }
}
