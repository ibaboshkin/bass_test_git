﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TagReader;

namespace APEv2Reader
{
    public class APEv2 : TagsBase
    {
        public APEv2()
        {
            AddField("Title", FieldType.Text);
            AddField("Artist", FieldType.Text);
            AddField("Album", FieldType.Text);
            //AddField("performer", FieldType.Text);
            AddField("Year", FieldType.DateTime);
            AddField("Track", FieldType.Bin);
            AddField("Genre", FieldType.Text);
            AddField("Copyright", FieldType.Text);
            AddField("Comment", FieldType.Text);
            AddField("Composer", FieldType.Text);
            AddField("APIC", FieldType.Bin);
            AddField("TPOS", FieldType.Bin);
        }

        /// <summary>
        /// TODO: ???
        /// </summary>
        public object Picture
        {
            get
            {
                return this["APIC"];
            }
            set
            {
                this["APIC"] = value;
            }
        }

        internal void SetNonCustomField(string key, object value)
        {
            this[key] = value;
        }
    }
}
