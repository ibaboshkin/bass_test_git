﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TagReader;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Tags;

namespace APEv2Reader
{
    public class APEv2Reader : ITagReader
    {
        private enum InformationType
        {
            String,
            Binary,
            External,
            Reserved
        }

        private class APETagsHeader
        {
            public const int SIZE = 32;

            public Version Version { get; set; }

            public int Size { get; set; }

            public int ItemsCount { get; set; }

            public bool HasHeader { get; set; }

            public bool HasFooter { get; set; }

            public bool IsHeader { get; set; }

            public bool IsFooter { get; set; }

            public InformationType InfoType { get; set; }

            public bool Readonly { get; set; }
        }

        public TagsBase GetTags(string fileName)
        {
            Bass.BASS_Init(-1, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            var res = Bass.BASS_PluginLoadDirectory(Path.GetDirectoryName(this.GetType().Assembly.Location));
            TAG_INFO bassTags = BassTags.BASS_TAG_GetFromFile(fileName, true, true);

            if (res != null)
            {
                foreach (var item in res)
                {
                    bool ok = Bass.BASS_PluginFree(item.Key);
                }
            }

            Bass.BASS_Free();

            APEv2 result = new APEv2();

            if (bassTags.NativeTags != null)
            {
                foreach (var tag in bassTags.NativeTags)
                {
                    var parts = tag.Split(new[] { '=' });

                    AddTag(result, parts[0].ToLower(), parts[1]);
                }
            }

            return result;

            //using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            //{
            //    return GetTags(stream);
            //}
        }

        public TagsBase GetTags(System.IO.Stream stream)
        {
            APEv2 result = null;
            APETagsHeader header = ReadHeader(stream);

            if (header == null)
            {
                stream.Position = stream.Seek(0, SeekOrigin.End);
                header = ReadHeader(stream, true);

                if (header == null)
                {
                    return null;
                }

                stream.Seek(-header.Size, SeekOrigin.End);
            }

            result = new APEv2();

            for (int i = 0; i < header.ItemsCount; i++)
            {
                ReadItem(stream, result, header.Size);
            }

            return result;
        }

        private void ReadItem(Stream stream, APEv2 tags, int maxBufferLength)
        {
            int valueLength = stream.ReadLength();
            stream.Position += 4;
            byte[] buffer = new byte[maxBufferLength];
            int keyLength = -1;

            do
            {
                keyLength++;
                buffer[keyLength] = (byte)stream.ReadByte();
            } while (buffer[keyLength] != 0);

            if (keyLength == 0)
            {
                return;
            }

            string key = Encoding.ASCII.GetString(buffer, 0, keyLength).ToLower();
            buffer = new byte[valueLength];
            stream.Read(buffer, 0, valueLength);
            string value = Encoding.UTF8.GetString(buffer);

            AddTag(tags, key, value);
        }

        private static void AddTag(APEv2 tags, string key, string value)
        {
            if (key != "APIC")
            {
                switch (key)
                {
                    case "title":
                        tags.Title = value;
                        break;
                    case "artist":
                        tags.Artist = value;
                        break;
                    case "album":
                        tags.Album = value;
                        break;
                    case "year":
                        if (value == null)
                        {
                            tags.Year = null;
                        }
                        else
                        {
                            tags.Year = short.Parse(value);
                        }
                        break;
                    case "track":
                        tags.TrackNumber = value;
                        break;
                    case "genre":
                        tags.Genre = value;
                        break;
                    case "copyright":
                        tags.Copyright = value;
                        break;
                    case "date":
                        tags.Date = value;
                        //if (value == null)
                        //{
                        //    tags.Date = null;
                        //}

                        //try
                        //{
                        //    tags.Date = Convert.ToDateTime(value);
                        //}
                        //catch (FormatException)
                        //{
                        //    try
                        //    {
                        //        tags.Date = new DateTime(Convert.ToInt32(value), 1, 1);
                        //    }
                        //    catch
                        //    {
                        //        tags.Date = null;
                        //    }
                        //}
                        break;
                    case "comment":
                        tags.Comment = value;
                        break;
                    case "composer":
                        tags.Composer = value;
                        break;
                    case "discnumber":
                        tags.DiscNumber = value;
                        break;
                    case "organization":
                        tags.Publisher = value;
                        break;
                    default:
                        tags.AddCustomField(key, FieldType.Text);
                        tags.SetCustomField(key, value);
                        break;
                }
            }
            else
            {
                //tags.Picture = frame.Data;
            }
        }

        private APETagsHeader ReadHeader(Stream stream, bool fromEnd = false)
        {
            APETagsHeader result = null;

            if (fromEnd)
            {
                stream.Seek(0, SeekOrigin.End);
                stream.Position -= APETagsHeader.SIZE;
            }

            byte[] buffer = new byte[8];
            stream.Read(buffer, 0, 8);
            string preamble = Encoding.ASCII.GetString(buffer, 0, 8);

            if (preamble != "APETAGEX")
            {
                return result;
            }

            result = new APETagsHeader();
            //stream.Read(buffer, 0, 4);
            int version = stream.ReadLength();
            result.Version = new Version(version / 1000, version % 1000);
            result.Size = stream.ReadLength();
            result.ItemsCount = stream.ReadLength();
            stream.Read(buffer, 0, 4);

            result.Readonly = (buffer[0] & 0x01) != 0;
            result.InfoType = (InformationType)((buffer[0] >> 1) & 0x3);
            result.HasHeader = (buffer[3] & 0x80) != 0;
            result.HasFooter = (buffer[3] & 0x40) == 0;
            result.IsHeader = (buffer[3] & 0x20) != 0;
            result.IsFooter = !result.IsHeader;
            buffer[3] = (byte)(buffer[3] & 0x1F);
            buffer[0] = (byte)(buffer[3] & 0xF8);

            for (int i = 0; i < 4; i++)
            {
                if (buffer[i] != 0)
                {
                    return null;
                }
            }

            stream.Read(buffer, 0, 8);

            foreach (byte b in buffer)
            {
                if (b != 0)
                {
                    return null;
                }
            }

            return result;
        }

        public string Extension
        {
            get { return "ape"; }
        }
    }
}
