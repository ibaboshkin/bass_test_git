﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TagReader;

namespace APEv2Reader
{
    public class APEv2 : TagsBase
    {
        public APEv2()
        {
            AddField("Title", FieldType.Text);
            AddField("Artist", FieldType.Text);
            AddField("Album", FieldType.Text);
            //AddField("performer", FieldType.Text);
            AddField("Year", FieldType.DateTime);
            AddField("Track", FieldType.Bin);
            AddField("Genre", FieldType.Text);
            AddField("Copyright", FieldType.Text);
            AddField("Comment", FieldType.Text);
            AddField("Composer", FieldType.Text);
            AddField("APIC", FieldType.Bin);
            AddField("TPOS", FieldType.Bin);
        }

        public string Title
        {
            get
            {
                return (string)this["Title"];
            }
            set
            {
                this["Title"] = value;
            }
        }

        public string Artist
        {
            get
            {
                return (string)this["Artist"];
            }
            set
            {
                this["Artist"] = value;
            }
        }

        public string Composer
        {
            get
            {
                return (string)this["Composer"];
            }
            set
            {
                this["Composer"] = value;
            }
        }

        public string Album
        {
            get
            {
                return (string)this["Album"];
            }
            set
            {
                this["Album"] = value;
            }
        }

        //public string Performer
        //{
        //    get
        //    {
        //        return (string)this["performer"];
        //    }
        //    set
        //    {
        //        this["performer"] = value;
        //    }
        //}

        public string Comment
        {
            get
            {
                return (string)this["Comment"];
            }
            set
            {
                this["Comment"] = value;
            }
        }

        public TrackNum? TrackNumber
        {
            get
            {
                if (this["Track"] == null/* || !(this["Track"] is TrackNum)*/)
                {
                    return null;
                }

                if (this["Track"] is string)
                {
                    return (TrackNum)(string)this["Track"];
                }
                else
                    if (this["Track"] is TrackNum)
                    {
                        return (TrackNum)this["Track"];
                    }
                    else
                    {
                        return null;
                    }
            }
            set
            {
                if (!value.HasValue)
                {
                    this["Track"] = null;
                }
                else
                {
                    this["Track"] = value;
                }
            }
        }

        /// <summary>
        /// TODO: ???
        /// </summary>
        public DiskNum? DiskNumber
        {
            get
            {
                if (this["TPOS"] == null/* || !(this["TPOS"] is DiscNum)*/)
                {
                    return null;
                }

                if (this["TPOS"] is string)
                {
                    return (DiskNum)(string)this["TPOS"];
                }
                else
                    if (this["Track"] is TrackNum)
                    {
                        return (DiskNum)this["TPOS"];
                    }
                    else
                    {
                        return null;
                    }
            }
            set
            {
                if (!value.HasValue)
                {
                    this["TPOS"] = null;
                }
                else
                {
                    this["TPOS"] = value;
                }
            }
        }

        public string Genre
        {
            get
            {
                return (string)this["Genre"];
            }
            set
            {
                this["Genre"] = value;
            }
        }

        public string Copyright
        {
            get
            {
                return (string)this["Copyright"];
            }
            set
            {
                this["Copyright"] = value;
            }
        }

        public DateTime? Year
        {
            get
            {
                if (this["Year"] == null)
                {
                    return null;
                }

                DateTime? result = null;

                try
                {
                    result = Convert.ToDateTime(this["Year"]);
                }
                catch (FormatException)
                {
                    result = new DateTime(Convert.ToInt32(this["Year"]), 1, 1);
                }

                return result;
            }
            set
            {
                if (!value.HasValue)
                {
                    this["Year"] = null;
                }
                else
                {
                    this["Year"] = value;
                }
            }
        }

        /// <summary>
        /// TODO: ???
        /// </summary>
        public object Picture
        {
            get
            {
                return this["APIC"];
            }
            set
            {
                this["APIC"] = value;
            }
        }

        internal void SetNonCustomField(string key, object value)
        {
            this[key] = value;
        }
    }
}
