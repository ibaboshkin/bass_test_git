﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class CoverArt : IDisposable
    {
        #region External procedures
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr CoverArt_get_Name(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_get_Stream(IntPtr coverArt, out IntPtr buffer, out Int64 size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr CoverArt_get_Description(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_CoverType(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr CoverArt_get_MIMEType(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern TagPictureFormat CoverArt_get_PictureFormat(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_Width(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_Height(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_ColorDepth(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_NoOfColors(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr CoverArt_get_Parent(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int CoverArt_get_Index(IntPtr coverArt);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Name(IntPtr coverArt, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Stream(IntPtr coverArt, ref byte[] buffer, int size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Description(IntPtr coverArt, string Description);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_CoverType(IntPtr coverArt, int CoverType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_MIMEType(IntPtr coverArt, string MIMEType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_PictureFormat(IntPtr coverArt, TagPictureFormat PictureFormat);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Width(IntPtr coverArt, int Width);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Height(IntPtr coverArt, int Height);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_ColorDepth(IntPtr coverArt, int ColorDepth);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_NoOfColors(IntPtr coverArt, int NoOfColors);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Parent(IntPtr coverArt, IntPtr Parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_set_Index(IntPtr coverArt, int Index);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr CoverArt_Create(IntPtr Parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_Destroy(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_Clear(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void CoverArt_Delete(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern bool CoverArt_Assign(IntPtr thisCover, IntPtr coverArt);
        #endregion

        #region Fields
        private IntPtr _ptr;
        private bool _memoryOwn;
        private bool _disposed; 
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return Marshal.PtrToStringUni(CoverArt_get_Name(_ptr));
            }
            set
            {
                CoverArt_set_Name(_ptr, value);
            }
        }

        public byte[] Data
        {
            get
            {
                IntPtr bufferPtr;
                Int64 size;
                CoverArt_get_Stream(_ptr, out bufferPtr, out size);

                byte[] buffer = new byte[size];
                Marshal.Copy(bufferPtr, buffer, 0, (int)size);
                return buffer;
            }
            set
            {
                if(value == null || value.Length == 0)
                {
                    throw new ArgumentNullException("Data");
                }

                CoverArt_set_Stream(_ptr, ref value, value.Length);
            }
        }

        public string Description
        {
            get
            {
                return Marshal.PtrToStringUni(CoverArt_get_Description(_ptr));
            }
            set
            {
                CoverArt_set_Description(_ptr, value);
            }
        }

        public int CoverType
        {
            get
            {
                return CoverArt_get_CoverType(_ptr);
            }
            set
            {
                CoverArt_set_CoverType(_ptr, value);
            }
        }

        public string MIMEType
        {
            get
            {
                return Marshal.PtrToStringUni(CoverArt_get_MIMEType(_ptr));
            }
            set
            {
                CoverArt_set_MIMEType(_ptr, value);
            }
        }

        public TagPictureFormat PictureFormat
        {
            get
            {
                return CoverArt_get_PictureFormat(_ptr);
            }
            set
            {
                CoverArt_set_PictureFormat(_ptr, value);
            }
        }

        public int Width
        {
            get
            {
                return CoverArt_get_Width(_ptr);
            }
            set
            {
                CoverArt_set_Width(_ptr, value);
            }
        }

        public int Height
        {
            get
            {
                return CoverArt_get_Height(_ptr);
            }
            set
            {
                CoverArt_set_Height(_ptr, value);
            }
        }

        public int ColorDepth
        {
            get
            {
                return CoverArt_get_ColorDepth(_ptr);
            }
            set
            {
                CoverArt_set_ColorDepth(_ptr, value);
            }
        }

        public int NoOfColors
        {
            get
            {
                return CoverArt_get_NoOfColors(_ptr);
            }
            set
            {
                CoverArt_set_NoOfColors(_ptr, value);
            }
        }

        public Tags Parent
        {
            get
            {
                IntPtr parent = CoverArt_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                return new Tags(parent);
            }
            set
            {
                if (value == null)
                {
                    CoverArt_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    CoverArt_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        public int Index
        {
            get
            {
                return CoverArt_get_Index(_ptr);
            }
            set
            {
                CoverArt_set_Index(_ptr, value);
            }
        }
        #endregion

        #region Constructors & Finalizer
        public CoverArt()
            : this(null)
        {
        }

        public CoverArt(Tags parent)
        {
            if (parent == null)
            {
                _ptr = CoverArt_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = CoverArt_Create(parent.Ptr);
            }

            _memoryOwn = true;
        }

        internal CoverArt(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;
        }

        ~CoverArt()
        {
            Dispose();
        } 
        #endregion

        #region Methods
        public void Clear()
        {
            CoverArt_Clear(_ptr);
        }

        public void Delete()
        {
            CoverArt_Delete(_ptr);
        }

        public void Assign(CoverArt coverArt)
        {
            if (coverArt == null)
            {
                throw new ArgumentNullException("coverArt");
            }

            CoverArt_Assign(_ptr, coverArt._ptr);
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}x{2})\nDescription: {3}\nMIME Type: {4}", Name, Width, Height, Description, MIMEType);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        CoverArt_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
        #endregion
    }
}
