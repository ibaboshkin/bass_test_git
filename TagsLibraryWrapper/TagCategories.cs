﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    internal static class TagCategories
    {
        public const string System = "System";
        public const string Album = "Album";
        public const string Track = "Track";
        public const string Common = "Common";

        public static string Default
        {
            get
            {
                return Common;
            }
        }
    }
}
