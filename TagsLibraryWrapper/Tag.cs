﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class Tag : IDisposable
    {
        #region External procedures
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr Tag_Create(IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern void Tag_Destroy(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_Clear(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern bool Tag_Delete(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_Remove(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern bool Tag_Assign(IntPtr thisTag, IntPtr tag);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr Tag_get_Name(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Name(IntPtr tag, string name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr Tag_get_Value(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr Tag_get_Language(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr Tag_get_Description(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern ExtTagType Tag_get_ExtTagType(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr Tag_get_Parent(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int Tag_get_Index(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Value(IntPtr tag, string value);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Language(IntPtr tag, string language);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Description(IntPtr tag, string description);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_ExtTagType(IntPtr tag, ExtTagType extTagType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Parent(IntPtr tag, IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void Tag_set_Index(IntPtr tag, int index);
        #endregion

        #region Fields
        private IntPtr _ptr;
        private bool _memoryOwn;
        private static Logger _logger = LogManager.GetLogger("TagUtilities.TagsLibraryWrapper");
        private bool _disposed = false; 
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return Marshal.PtrToStringUni(Tag_get_Name(_ptr));
            }
            set
            {
                Tag_set_Name(_ptr, value);
            }
        }

        public string Value
        {
            get
            {
                return Marshal.PtrToStringUni(Tag_get_Value(_ptr));
            }
            set
            {
                Tag_set_Value(_ptr, value);
            }
        }

        public string Language
        {
            get
            {
                return Marshal.PtrToStringUni(Tag_get_Language(_ptr));
            }
            set
            {
                Tag_set_Language(_ptr, value);
            }
        }

        public string Description
        {
            get
            {
                return Marshal.PtrToStringUni(Tag_get_Description(_ptr));
            }
            set
            {
                Tag_set_Description(_ptr, value);
            }
        }

        public ExtTagType ExtTagType
        {
            get
            {
                return Tag_get_ExtTagType(_ptr);
            }
            set
            {
                Tag_set_ExtTagType(_ptr, value);
            }
        }

        public Tags Parent
        {
            get
            {
                IntPtr parent = Tag_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                return new Tags(parent);
            }
            set
            {
                if (value == null)
                {
                    Tag_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    Tag_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        public int Index
        {
            get
            {
                return Tag_get_Index(_ptr);
            }
            set
            {
                Tag_set_Index(_ptr, value);
            }
        }
        #endregion
    
        public Tag()
            : this(null)
        {
        }

        public Tag(Tags parent)
        {
            if (parent == null)
            {
                _ptr = Tag_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = Tag_Create(parent.Ptr);
            }

            _memoryOwn = true;

            _logger.Trace("Created Tag with new handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
            _logger.Trace(Environment.StackTrace);
        }

        internal Tag(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;

            _logger.Trace("Created Tag with existing handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
            _logger.Trace(Environment.StackTrace);
        }

        ~Tag()
        {
            Dispose();
        }

        #region Methods
        public void Clear()
        {
            Tag_Clear(_ptr);
        }

        public bool Delete()
        {
            return Tag_Delete(_ptr);
        }

        public void Remove()
        {
            Tag_Remove(_ptr);
        }

        public bool Assign(Tag tag)
        {
            if (tag == null)
            {
                throw new ArgumentNullException("tag");
            }

            return Tag_Assign(_ptr, tag._ptr);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        _logger.Trace("Try to destroy Tag with handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
                        _logger.Trace(Environment.StackTrace);
                        
                        Tag_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
        #endregion
    }
}
