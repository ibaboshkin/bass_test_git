﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class SourceAudioAttributes : IDisposable
    {
        #region External procedures
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr SourceAudioAttributes_Create(IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void SourceAudioAttributes_Destroy(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern short SourceAudioAttributes_get_Channels(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int SourceAudioAttributes_get_SamplesPerSec(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern short SourceAudioAttributes_get_BitsPerSample(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern double SourceAudioAttributes_get_PlayTime(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern long SourceAudioAttributes_get_SampleCount(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern int SourceAudioAttributes_get_BitRate(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr SourceAudioAttributes_get_Parent(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern void SourceAudioAttributes_set_Parent(IntPtr sourceAudioAttributes, IntPtr parent);
        #endregion

        #region Fields
        private IntPtr _ptr;
        private bool _disposed;
        private bool _memoryOwn; 
        #endregion

        #region Properties
        public short Channels
        {
            get
            {
                return SourceAudioAttributes_get_Channels(_ptr);
            }
        }

        public int SamplesPerSec
        {
            get
            {
                return SourceAudioAttributes_get_SamplesPerSec(_ptr);
            }
        }

        public short BitsPerSample
        {
            get
            {
                return SourceAudioAttributes_get_BitsPerSample(_ptr);
            }
        }

        public double PlayTime
        {
            get
            {
                return SourceAudioAttributes_get_PlayTime(_ptr);
            }
        }

        public long SampleCount
        {
            get
            {
                return SourceAudioAttributes_get_SampleCount(_ptr);
            }
        }

        public int BitRate
        {
            get
            {
                return SourceAudioAttributes_get_BitRate(_ptr);
            }
        }

        public Tags Parent
        {
            get
            {
                IntPtr parent = SourceAudioAttributes_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                return new Tags(parent);
            }
            set
            {
                if (value == null)
                {
                    SourceAudioAttributes_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    SourceAudioAttributes_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        internal IntPtr Ptr
        {
            get
            {
                return _ptr;
            }
        }
        #endregion

        #region Constructors & Finalizer
        public SourceAudioAttributes()
            : this(null)
        {
        }

        public SourceAudioAttributes(Tags parent)
        {
            if (parent == null)
            {
                _ptr = SourceAudioAttributes_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = SourceAudioAttributes_Create(parent.Ptr);
            }

            _memoryOwn = true;
        }

        internal SourceAudioAttributes(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;
        }

        ~SourceAudioAttributes()
        {
            Dispose();
        }
        #endregion

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        SourceAudioAttributes_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
    }
}
