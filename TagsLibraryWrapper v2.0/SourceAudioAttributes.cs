﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class SourceAudioAttributes : IDisposable
    {
        #region Fields
        private IntPtr _ptr;
        private bool _disposed;
        private bool _memoryOwn; 
        #endregion

        #region Properties
        public short Channels
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_Channels(_ptr);
            }
        }

        public int SamplesPerSec
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_SamplesPerSec(_ptr);
            }
        }

        public short BitsPerSample
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_BitsPerSample(_ptr);
            }
        }

        public double PlayTime
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_PlayTime(_ptr);
            }
        }

        public long SampleCount
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_SampleCount(_ptr);
            }
        }

        public int BitRate
        {
            get
            {
                return TagsLibraryPInvoke.SourceAudioAttributes_get_BitRate(_ptr);
            }
        }

        public TagsBase Parent
        {
            get
            {
                IntPtr parent = TagsLibraryPInvoke.SourceAudioAttributes_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                var tags = new TagsBase(parent);
                return (TagsBase)TypeConversionService.Convert(tags.TypeString, tags);
            }
            set
            {
                if (value == null)
                {
                    TagsLibraryPInvoke.SourceAudioAttributes_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    TagsLibraryPInvoke.SourceAudioAttributes_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        internal IntPtr Ptr
        {
            get
            {
                return _ptr;
            }
        }
        #endregion

        #region Constructors & Finalizer
        public SourceAudioAttributes()
            : this(null)
        {
        }

        public SourceAudioAttributes(TagsBase parent)
        {
            if (parent == null)
            {
                _ptr = TagsLibraryPInvoke.SourceAudioAttributes_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = TagsLibraryPInvoke.SourceAudioAttributes_Create(parent.Ptr);
            }

            _memoryOwn = true;
        }

        internal SourceAudioAttributes(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;
        }

        ~SourceAudioAttributes()
        {
            Dispose();
        }
        #endregion

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        TagsLibraryPInvoke.SourceAudioAttributes_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
    }
}
