﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    #region Enums
    public enum TagType
    {
        [EnumMember]
        ttNone = 0,
        [EnumMember]
        ttAutomatic = 1,
        [EnumMember]
        ttAPEv2 = 2,
        [EnumMember]
        ttFlac = 3,
        [EnumMember]
        ttID3v1 = 4,
        [EnumMember]
        ttID3v2 = 5,
        [EnumMember]
        ttMP4 = 6,
        [EnumMember]
        ttOpusVorbis = 7,
        [EnumMember]
        ttWAV = 8,
        [EnumMember]
        ttWMA = 9
    };

    public enum TagPictureFormat
    {
        [EnumMember]
        tpfUnknown = 0,
        [EnumMember]
        tpfJPEG = 1,
        [EnumMember]
        tpfPNG = 2,
        [EnumMember]
        tpfBMP = 3,
        [EnumMember]
        tpfGIF = 4
    };

    public enum ExtTagType
    {
        [EnumMember]
        ettUnknown = 0,
        [EnumMember]
        ettTXXX = 1,
        [EnumMember]
        ettWXXX = 2
    };

    public enum AudioType
    {
        [EnumMember]
        atAutomatic = 0,
        [EnumMember]
        atFlac = 1,
        [EnumMember]
        atMPEG = 2,
        [EnumMember]
        atDSDDSF = 3,
        [EnumMember]
        atWAV = 4,
        [EnumMember]
        atAIFF = 5,
        [EnumMember]
        atMP4 = 6,
        [EnumMember]
        atOpus = 7,
        [EnumMember]
        atVorbis = 8,
        [EnumMember]
        atWMA = 9
    };

    public enum MPEGVersion
    {
        [EnumMember]
        tmpegvUnknown = 0,
        [EnumMember]
        tmpegv1 = 1,
        [EnumMember]
        tmpegv2 = 2,
        [EnumMember]
        tmpegv25 = 3
    };

    public enum MPEGLayer
    {
        [EnumMember]
        tmpeglUnknown = 0,
        [EnumMember]
        tmpegl1 = 1,
        [EnumMember]
        tmpegl2 = 2,
        [EnumMember]
        tmpegl3 = 3
    };

    public enum MPEGChannelMode
    {
        [EnumMember]
        tmpegcmUnknown = 0,
        [EnumMember]
        tmpegcmMono = 1,
        [EnumMember]
        tmpegcmDualChannel = 2,
        [EnumMember]
        tmpegcmJointStereo = 3,
        [EnumMember]
        tmpegcmStereo = 4
    };

    public enum MPEGModeExtension
    {
        [EnumMember]
        tmpegmeUnknown = 0,
        [EnumMember]
        tmpegmeNone = 1,
        [EnumMember]
        tmpegmeIntensity = 2,
        [EnumMember]
        tmpegmeMS = 3,
        [EnumMember]
        tmpegmeIntensityMS = 4
    };

    public enum MPEGEmphasis
    {
        [EnumMember]
        tmpegeUnknown = 0,
        [EnumMember]
        tmpegeNo = 1,
        [EnumMember]
        tmpege5015 = 2,
        [EnumMember]
        tmpegeCCITJ17 = 3
    };

    public enum DSFChannelType
    {
        [EnumMember]
        dsfctUnknown = 0,
        [EnumMember]
        dsfctMono = 1,
        [EnumMember]
        dsfctStereo = 2,
        [EnumMember]
        dsfct3Channels = 3,
        [EnumMember]
        dsfctQuad = 4,
        [EnumMember]
        dsfct4Channels = 5,
        [EnumMember]
        dsfct5Channels = 6,
        [EnumMember]
        dsfct51Channels = 7
    };
    #endregion

    #region Structures
    public struct ExtTag
    {
        string Name;
        string Value;
        int ValueSize;
        string Language;
        string Description;
        ExtTagType ExtTagType;
        int Index;
    };

    public struct CoverArtData
    {
        string Name;
        IntPtr Data;
        long DataSize;
        string Description;
        int CoverType;
        string MIMEType;
        TagPictureFormat PictureFormat;
        int Width;
        int Height;
        int ColorDepth;
        int NoOfColors;
        int ID3v2TextEncoding;
        int Index;
    };

    public struct TagData
    {
        string Name;
        IntPtr Data;
        long DataSize;
        int DataType;
    };

    public struct CARTPostTimer
    {
        string Usage;
        int Value;
    };

    public struct MPEGAudioAttributes
    {
        long Position;                		//* Position of header in bytes
        int Header;                  		//* The Headers bytes
        int FrameSize;             			//* Frame's length
        MPEGVersion Version;          		//* MPEG Version
        MPEGLayer Layer;              		//* MPEG Layer
        bool CRC;                  			//* Frame has CRC
        int BitRate;                 		//* Frame's bitrate
        int SampleRate;                   //* Frame's sample rate
        bool Padding;              			//* Frame is padded
        bool _Private;             			//* Frame's private bit is set
        MPEGChannelMode ChannelMode;  		//* Frame's channel mode
        MPEGModeExtension ModeExtension; 	//* Joint stereo only
        bool Copyrighted;          			//* Frame's Copyright bit is set
        bool Original;             			//* Frame's Original bit is set
        MPEGEmphasis Emphasis;        		//* Frame's emphasis mode
        bool VBR;                  			//* Stream is probably VBR
        long FrameCount;              		//* Total number of MPEG frames (by header)
        int Quality;               			//* MPEG quality
        long Bytes;                   		//* Total bytes
    };

    public struct FlacAudioAttributes
    {
        int Channels;
        int SampleRate;
        int BitsPerSample;
        long SampleCount;
        double Playtime;       // Duration (seconds)
        double Ratio;          // Compression ratio (%)
        string ChannelMode;
        int Bitrate;
    };

    public struct DSFAudioAttributes
    {
        int FormatVersion;
        int FormatID;
        DSFChannelType ChannelType;
        int ChannelNumber;
        int SamplingFrequency;
        int BitsPerSample;
        long SampleCount;
        int BlockSizePerChannel;
        double PlayTime;
        int Bitrate;
    };

    public struct OpusAudioAttributes
    {
        int BitstreamVersion;                       	//{ Bitstream version number }
        int ChannelCount;                           	//      { Number of channels }
        int PreSkip;
        int SampleRate;                             	//        { Sample rate (hz) }
        int OutputGain;
        int MappingFamily;                          	//                 { 0,1,255 }
        double PlayTime;
        long SampleCount;
        int Bitrate;
    };

    [StructLayout(layoutKind: LayoutKind.Sequential, Size = 48)]
    public unsafe struct VorbisAudioAttributes
    {
        fixed byte BitstreamVersion[4];        				//{ Bitstream version number }
        int ChannelMode;                          	//      { Number of channels }
        int SampleRate;                           		//        { Sample rate (hz) }
        int BitRateMaximal;                       		//    { Bit rate upper limit }
        int BitRateNominal;                       		//        { Nominal bit rate }
        int BitRateMinimal;                       		//    { Bit rate lower limit }
        int BlockSize;                    			//{ Coded size for small and long blocks }
        double PlayTime;
        long SampleCount;
        int Bitrate;
    };

    [StructLayout(layoutKind: LayoutKind.Sequential, Size = 72)]
    public unsafe struct WAVEAudioAttributes
    {
        int FormatTag;                   	// format type
        int Channels;                    	// number of channels (i.e. mono, stereo, etc.)
        int SamplesPerSec;              	// sample rate
        int AvgBytesPerSec;             	// for buffer estimation
        int BlockAlign;                  	// block size of data
        int BitsPerSample;               	// number of bits per sample of mono data
        double PlayTime;
        long SampleCount;
        int cbSize;	                    // Size of the extension: 22
        int ValidBitsPerSample;	        // at most 8 *  M
        int ChannelMask;	                // Speaker position mask: 0
        fixed byte SubFormat[16];				// TODO: must by byte[16]
        int Bitrate;
    };

    [StructLayout(layoutKind: LayoutKind.Sequential, Size = 40)]
    public unsafe struct AIFFAttributes
    {
        int Channels;
        int SampleCount;
        int SampleSize;
        double SampleRate;
        fixed byte CompressionID[4];  // TODO: must be byte[4] http://en.wikipedia.org/wiki/Audio_Interchange_File_Format
        string Compression;
        double PlayTime;
        int BitRate;
    };

    public struct WMAAttributes
    {
        double PlayTime;
        int BitRate;
    };

    public struct AudioAttributes
    {
        int Channels;                     	// number of channels (i.e. mono, stereo, etc.)
        int SamplesPerSec;               		// sample rate
        int BitsPerSample;                	// number of bits per sample of mono data
        double PlayTime;                        // duration in seconds
        long SampleCount;                   	// number of total samples
        int Bitrate;
    };
    #endregion
}
