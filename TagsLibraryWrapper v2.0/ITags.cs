﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public interface ITags
    {
        string TypeString { get; }
        Tag Add(string name);
        CoverArt AddCoverArt(string name);
        CoverArt AddCoverArt(string name, byte[] data, string MIMEType);
        CoverArt AddCoverArt(string name, string fileName);
        bool Assign(TagsBase source);
        void Clear();
        bool Delete(int index);
        void LoadFromBASS(int channel);
        void LoadFromFile(string fileName);
        void SaveToFile(string fileName);
    }
}
