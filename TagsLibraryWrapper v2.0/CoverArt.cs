﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class CoverArt : IDisposable
    {
        #region Fields
        private IntPtr _ptr;
        private bool _memoryOwn;
        private bool _disposed; 
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.CoverArt_get_Name(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_Name(_ptr, value);
            }
        }

        public byte[] Data
        {
            get
            {
                IntPtr bufferPtr;
                Int64 size;
                TagsLibraryPInvoke.CoverArt_get_Stream(_ptr, out bufferPtr, out size);

                byte[] buffer = new byte[size];
                Marshal.Copy(bufferPtr, buffer, 0, (int)size);
                return buffer;
            }
            set
            {
                if(value == null || value.Length == 0)
                {
                    throw new ArgumentNullException("Data");
                }

                TagsLibraryPInvoke.CoverArt_set_Stream(_ptr, ref value, value.Length);
            }
        }

        public string Description
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.CoverArt_get_Description(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_Description(_ptr, value);
            }
        }

        public int CoverType
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_CoverType(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_CoverType(_ptr, value);
            }
        }

        public string MIMEType
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.CoverArt_get_MIMEType(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_MIMEType(_ptr, value);
            }
        }

        public TagPictureFormat PictureFormat
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_PictureFormat(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_PictureFormat(_ptr, value);
            }
        }

        public int Width
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_Width(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_Width(_ptr, value);
            }
        }

        public int Height
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_Height(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_Height(_ptr, value);
            }
        }

        public int ColorDepth
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_ColorDepth(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_ColorDepth(_ptr, value);
            }
        }

        public int NoOfColors
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_NoOfColors(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_NoOfColors(_ptr, value);
            }
        }

        public TagsBase Parent
        {
            get
            {
                IntPtr parent = TagsLibraryPInvoke.CoverArt_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                var tags = new TagsBase(parent);
                return (TagsBase)TypeConversionService.Convert(tags.TypeString, tags);
            }
            set
            {
                if (value == null)
                {
                    TagsLibraryPInvoke.CoverArt_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    TagsLibraryPInvoke.CoverArt_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        public int Index
        {
            get
            {
                return TagsLibraryPInvoke.CoverArt_get_Index(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.CoverArt_set_Index(_ptr, value);
            }
        }
        #endregion

        #region Constructors & Finalizer
        public CoverArt()
            : this(null)
        {
        }

        public CoverArt(TagsBase parent)
        {
            if (parent == null)
            {
                _ptr = TagsLibraryPInvoke.CoverArt_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = TagsLibraryPInvoke.CoverArt_Create(parent.Ptr);
            }

            _memoryOwn = true;
        }

        internal CoverArt(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;
        }

        ~CoverArt()
        {
            Dispose();
        } 
        #endregion

        #region Methods
        public void Clear()
        {
            TagsLibraryPInvoke.CoverArt_Clear(_ptr);
        }

        public void Delete()
        {
            TagsLibraryPInvoke.CoverArt_Delete(_ptr);
        }

        public void Assign(CoverArt coverArt)
        {
            if (coverArt == null)
            {
                throw new ArgumentNullException("coverArt");
            }

            TagsLibraryPInvoke.CoverArt_Assign(_ptr, coverArt._ptr);
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}x{2})\nDescription: {3}\nMIME Type: {4}", Name, Width, Height, Description, MIMEType);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        TagsLibraryPInvoke.CoverArt_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
        #endregion
    }
}
