﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TagsLibrary.Converters
{
    class ID3v1TagConverter
    {
        public static void registerTypeConversion()
        {
            TypeConversionService.RegisterType("TagsLibrary.ID3v1Tag", GetTagsBase);
        }

        public static object GetTagsBase(TagsBase tags)
        {
            return new ID3v1Tag(tags.Ptr);
        }
    }
}
