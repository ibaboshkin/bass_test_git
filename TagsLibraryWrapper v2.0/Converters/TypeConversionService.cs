﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public static class TypeConversionService
    {
        private static Dictionary<string, Func<TagsBase, object>> _convertDictionary = new Dictionary<string, Func<TagsBase, object>>();

        private static bool HasType(string typeString)
        {
            Func<TagsBase, object> value;
            return _convertDictionary.TryGetValue(typeString, out value);
        }

        public static void RegisterType(string typeString, Func<TagsBase, object> convertFunction)
        {
            _convertDictionary[typeString] = convertFunction;
        }

        public static object Convert(string typeString, TagsBase tags)
        {
            if (!HasType(typeString))
            {
                return (Object)tags;
            }

            Func<TagsBase, object> convertFunction = _convertDictionary[typeString];

            if (convertFunction == null)
            {
                return null;
            }

            return convertFunction(tags);
        }

        public static string ConvertFromNativeString(IntPtr text)
        {
            return Marshal.PtrToStringUni(text);
        }
    }
}
