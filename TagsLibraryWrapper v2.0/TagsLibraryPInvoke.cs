﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    internal static class TagsLibraryPInvoke
    {
        #region CoverArt
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr CoverArt_get_Name(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_get_Stream(IntPtr coverArt, out IntPtr buffer, out Int64 size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr CoverArt_get_Description(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_CoverType(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr CoverArt_get_MIMEType(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern TagPictureFormat CoverArt_get_PictureFormat(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_Width(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_Height(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_ColorDepth(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_NoOfColors(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr CoverArt_get_Parent(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int CoverArt_get_Index(IntPtr coverArt);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Name(IntPtr coverArt, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Stream(IntPtr coverArt, ref byte[] buffer, int size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Description(IntPtr coverArt, string Description);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_CoverType(IntPtr coverArt, int CoverType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_MIMEType(IntPtr coverArt, string MIMEType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_PictureFormat(IntPtr coverArt, TagPictureFormat PictureFormat);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Width(IntPtr coverArt, int Width);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Height(IntPtr coverArt, int Height);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_ColorDepth(IntPtr coverArt, int ColorDepth);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_NoOfColors(IntPtr coverArt, int NoOfColors);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Parent(IntPtr coverArt, IntPtr Parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_set_Index(IntPtr coverArt, int Index);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr CoverArt_Create(IntPtr Parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_Destroy(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_Clear(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void CoverArt_Delete(IntPtr coverArt);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool CoverArt_Assign(IntPtr thisCover, IntPtr coverArt);
        #endregion

        #region SourceAudioAttributes
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr SourceAudioAttributes_Create(IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void SourceAudioAttributes_Destroy(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern short SourceAudioAttributes_get_Channels(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int SourceAudioAttributes_get_SamplesPerSec(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern short SourceAudioAttributes_get_BitsPerSample(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern double SourceAudioAttributes_get_PlayTime(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern long SourceAudioAttributes_get_SampleCount(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int SourceAudioAttributes_get_BitRate(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr SourceAudioAttributes_get_Parent(IntPtr sourceAudioAttributes);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void SourceAudioAttributes_set_Parent(IntPtr sourceAudioAttributes, IntPtr parent);
        #endregion

        #region Tag
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Tag_Create(IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void Tag_Destroy(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_Clear(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool Tag_Delete(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_Remove(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool Tag_Assign(IntPtr thisTag, IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr Tag_get_Name(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Name(IntPtr tag, string name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr Tag_get_Value(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr Tag_get_Language(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr Tag_get_Description(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern ExtTagType Tag_get_ExtTagType(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr Tag_get_Parent(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int Tag_get_Index(IntPtr tag);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Value(IntPtr tag, string value);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Language(IntPtr tag, string language);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Description(IntPtr tag, string description);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_ExtTagType(IntPtr tag, ExtTagType extTagType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Parent(IntPtr tag, IntPtr parent);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void Tag_set_Index(IntPtr tag, int index);
        #endregion

        #region TagsBase
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_get_Loaded(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_get_Tags(IntPtr tags, out IntPtr array, out int size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_get_CoverArts(IntPtr tags, out IntPtr array, out int size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern long TagsBase_get_Size(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_set_Loaded(IntPtr tags, bool Loaded);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_set_Tags(IntPtr thisTags, IntPtr Tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_set_CoverArts(IntPtr tags, IntPtr CoverArts);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_Create();
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_Destroy(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_LoadFromFile(IntPtr tags, string FileName);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_LoadFromBASS(IntPtr tags, int Channel);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_SaveToFile1(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_SaveToFile2(IntPtr tags, string FileName);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_Add(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_Tag(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_GetTag(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_SetTag(IntPtr tags, string Name, string Text);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_Delete(IntPtr tags, int Index);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_Delete2(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_Remove(IntPtr tags, int Index);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_DeleteAllTags(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_Clear(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_Count(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_Exists(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_TypeCount(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_RemoveEmptyTags(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int TagsBase_CoverArtCount(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_CoverArt(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_AddCoverArt(IntPtr tags, string Name);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_AddCoverArt2(IntPtr tags, string Name, ref byte[] data, long size, string MIMEType);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_AddCoverArt3(IntPtr tags, string Name, string FileName);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_DeleteCoverArt(IntPtr tags, int Index);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void TagsBase_DeleteAllCoverArts(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool TagsBase_Assign(IntPtr tags, IntPtr Sources);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBase_GetTypeString(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern TagPictureFormat TagsBase_PictureStreamType(IntPtr tags, byte[] data, long size);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern TagPictureFormat TagsBase_PicturePointerType(IntPtr tags, IntPtr Picture);
        #endregion

        #region ID3v1Tag
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_Create();

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_Destroy(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetTitle(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetArtist(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetAlbum(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetYear(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetComment(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetTrack(IntPtr tags, byte value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_SetGenre(IntPtr tags, string value);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetTite(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetArtist(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetAlbum(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetYear(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetComment(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern byte ID3v1Tag_GetTrack(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetGenre(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool ID3v1Tag_Assign1(IntPtr tags, IntPtr source);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern bool ID3v1Tag_Assign2(IntPtr tags, IntPtr source);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern void ID3v1Tag_Clear(IntPtr tags);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v1Tag_LoadFromFile(IntPtr tags, string fileName);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v1Tag_LoadFromBASS(IntPtr tags, int channel);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v1Tag_SaveToFile1(IntPtr tags, string fileName);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v1Tag_SaveToFile2(IntPtr tags, string fileName, bool writeLyricsTag);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1Tag_GetTypeString(IntPtr tags);
        #endregion

        #region Converters
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr ID3v1TagToTagsBase(IntPtr tags);

        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern IntPtr TagsBaseToID3v1Tag(IntPtr tags);
        #endregion
    }
}
