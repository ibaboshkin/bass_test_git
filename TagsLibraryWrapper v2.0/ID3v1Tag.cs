﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class ID3v1Tag : TagsBase, ITags, IDisposable
    {
        #region Fields
        private bool _disposed;
        private bool _memoryOwn;
        private IntPtr _ptr;
        private TimeSpan _length;
        private static Logger _logger = LogManager.GetLogger("TagUtilities.TagsLibraryWrapper");
        #endregion

        #region Properties
        public override string TypeString
        {
            get 
            { 
                return TypeConversionService.ConvertFromNativeString(TagsLibraryPInvoke.ID3v1Tag_GetTypeString(_ptr));
            }
        }
        #endregion

        #region Constructor & Destructor
        public ID3v1Tag()
            : this(TagsLibraryPInvoke.ID3v1Tag_Create(), true)
        {
        }

        internal ID3v1Tag(IntPtr ptr)
            : this(ptr, false)
        {
        }

        internal ID3v1Tag(IntPtr ptr, bool memoryOwn)
            : base(TagsLibraryPInvoke.ID3v1TagToTagsBase(ptr), memoryOwn)
        {
            _ptr = ptr;
            _memoryOwn = memoryOwn;
            _logger.Trace("Created ID3v1Tag instance with existing handler 0x{0:x} ({0})", _ptr);
            //_logger.Trace(Environment.StackTrace);
        }

        ~ID3v1Tag()
        {
            Dispose();
        }

        public override void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _logger.Trace("Try to dispose Tags instance with handler 0x{0:x} ({0})", _ptr);
                    //_logger.Trace(Environment.StackTrace);
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        TagsLibraryPInvoke.ID3v1Tag_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                    }

                    GC.SuppressFinalize(this);
                    base.Dispose();
                }
            }
        }
        #endregion

        #region Methods
        public override bool Assign(TagsBase source)
        {
            return TagsLibraryPInvoke.ID3v1Tag_Assign1(_ptr, source.Ptr);
        }

        public bool Assign(ID3v1Tag source)
        {
            return TagsLibraryPInvoke.ID3v1Tag_Assign2(_ptr, source.Ptr);
        }

        public override void Clear()
        {
            TagsLibraryPInvoke.ID3v1Tag_Clear(_ptr);
        }

        public void LoadFromBASS(int channel)
        {
            TagsLibraryPInvoke.ID3v1Tag_LoadFromBASS(_ptr, channel);
        }

        public void LoadFromFile(string fileName)
        {
            TagsLibraryPInvoke.ID3v1Tag_LoadFromFile(_ptr, fileName);
        }

        public void SaveToFile(string fileName)
        {
            TagsLibraryPInvoke.ID3v1Tag_SaveToFile1(_ptr, fileName);
        }

        public void SaveToFile(string fileName, bool writeLyricsTag)
        {
            TagsLibraryPInvoke.ID3v1Tag_SaveToFile2(_ptr, fileName, writeLyricsTag);
        } 
        #endregion
    }
}
