﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    [Serializable]
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
    public class TagNameAttribute : Attribute
    {
        private string _description;
        private string _friendlyName;
        private string _category;
        private string _systemName;

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public string FriendlyName
        {
            get
            {
                return _friendlyName;
            }
            set
            {
                _friendlyName = value;
            }
        }

        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }


        public string SystemName
        {
            get
            {
                return _systemName;
            }
            set
            {
                _systemName = value;
            }
        }


        public TagNameAttribute()
            : this(string.Empty)
        {
        }

        public TagNameAttribute(string description)
            :this(string.Empty, description)
        {
        }

        public TagNameAttribute(string friendlyName, string description, string category = TagCategories.Common)
        {
            Description = description;
            FriendlyName = friendlyName;
            Category = category;
        }
    }
}
