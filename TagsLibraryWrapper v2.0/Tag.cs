﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public class Tag : IDisposable
    {
        #region Fields
        private IntPtr _ptr;
        private bool _memoryOwn;
        private static Logger _logger = LogManager.GetLogger("TagUtilities.TagsLibraryWrapper");
        private bool _disposed = false; 
        #endregion

        #region Properties
        public string Name
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.Tag_get_Name(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_Name(_ptr, value);
            }
        }

        public string Value
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.Tag_get_Value(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_Value(_ptr, value);
            }
        }

        public string Language
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.Tag_get_Language(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_Language(_ptr, value);
            }
        }

        public string Description
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.Tag_get_Description(_ptr));
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_Description(_ptr, value);
            }
        }

        public ExtTagType ExtTagType
        {
            get
            {
                return TagsLibraryPInvoke.Tag_get_ExtTagType(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_ExtTagType(_ptr, value);
            }
        }

        public TagsBase Parent
        {
            get
            {
                IntPtr parent = TagsLibraryPInvoke.Tag_get_Parent(_ptr);

                if (parent == IntPtr.Zero)
                {
                    return null;
                }

                var tags = new TagsBase(parent);
                return (TagsBase)TypeConversionService.Convert(tags.TypeString, tags);
            }
            set
            {
                if (value == null)
                {
                    TagsLibraryPInvoke.Tag_set_Parent(_ptr, IntPtr.Zero);
                }
                else
                {
                    TagsLibraryPInvoke.Tag_set_Parent(_ptr, value.Ptr);
                }
            }
        }

        public int Index
        {
            get
            {
                return TagsLibraryPInvoke.Tag_get_Index(_ptr);
            }
            set
            {
                TagsLibraryPInvoke.Tag_set_Index(_ptr, value);
            }
        }
        #endregion
    
        public Tag()
            : this(null)
        {
        }

        public Tag(TagsBase parent)
        {
            if (parent == null)
            {
                _ptr = TagsLibraryPInvoke.Tag_Create(IntPtr.Zero);
            }
            else
            {
                _ptr = TagsLibraryPInvoke.Tag_Create(parent.Ptr);
            }

            _memoryOwn = true;

            _logger.Trace("Created Tag with new handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
            _logger.Trace(Environment.StackTrace);
        }

        internal Tag(IntPtr ptr)
        {
            _ptr = ptr;
            _memoryOwn = false;

            _logger.Trace("Created Tag with existing handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
            _logger.Trace(Environment.StackTrace);
        }

        ~Tag()
        {
            Dispose();
        }

        #region Methods
        public void Clear()
        {
            TagsLibraryPInvoke.Tag_Clear(_ptr);
        }

        public bool Delete()
        {
            return TagsLibraryPInvoke.Tag_Delete(_ptr);
        }

        public void Remove()
        {
            TagsLibraryPInvoke.Tag_Remove(_ptr);
        }

        public bool Assign(Tag tag)
        {
            if (tag == null)
            {
                throw new ArgumentNullException("tag");
            }

            return TagsLibraryPInvoke.Tag_Assign(_ptr, tag._ptr);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        _logger.Trace("Try to destroy Tag with handler 0x{0:x} ({0}). Name = \"{1}\", Value = \"{2}\"", (ulong)_ptr, this.Name, this.Value);
                        _logger.Trace(Environment.StackTrace);

                        TagsLibraryPInvoke.Tag_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                        GC.SuppressFinalize(this);
                    }
                }
            }
        }
        #endregion
    }
}
