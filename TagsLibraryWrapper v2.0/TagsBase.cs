﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text;
using Un4seen.Bass;

namespace TagUtilities.TagsLibrary
{
    [Serializable]
    public class TagsBase : IDisposable, ITags
    {
        #region Fields
        private bool _disposed;
        private bool _memoryOwn;
        private IntPtr _ptr;
        private TimeSpan _length;
        private int _bitrate;
        private byte _channels;
        private int _frequency;
        private static Logger _logger = LogManager.GetLogger("TagUtilities.TagsLibraryWrapper");
        #endregion

        #region Properties
        public bool Loaded
        {
            get
            {
                try
                {
                    return TagsLibraryPInvoke.TagsBase_get_Loaded(_ptr);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }
            }
            set
            {
                try
                {
                    TagsLibraryPInvoke.TagsBase_set_Loaded(_ptr, value);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }
            }
        }

        public Tag[] TagsList
        {
            get
            {
                _logger.Trace("Enter Tags.get_TagsList");

                IntPtr ptrArray;
                int size;

                try
                {
                    TagsLibraryPInvoke.TagsBase_get_Tags(_ptr, out ptrArray, out size);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }

                if (ptrArray == IntPtr.Zero || size == 0)
                {
                    return null;
                }

                IntPtr[] pointers = new IntPtr[size];
                Marshal.Copy(ptrArray, pointers, 0, size);
                Tag[] tags = new Tag[size];

                for (int i = 0; i < size; i++)
                {
                    tags[i] = new Tag(pointers[i]);
                }

                _logger.Trace("Exit Tags.get_TagsList");
                return tags;
            }
        }

        public CoverArt[] CoverArts
        {
            get
            {
                _logger.Trace("Enter Tags.get_CoverArts");

                IntPtr ptrArray;
                int size;

                try
                {
                    TagsLibraryPInvoke.TagsBase_get_CoverArts(_ptr, out ptrArray, out size);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }

                if (ptrArray == IntPtr.Zero || size == 0)
                {
                    return null;
                }

                IntPtr[] pointers = new IntPtr[size];
                Marshal.Copy(ptrArray, pointers, 0, size);
                CoverArt[] coverArts = new CoverArt[size];

                for (int i = 0; i < size; i++)
                {
                    coverArts[i] = new CoverArt(pointers[i]);
                }

                _logger.Trace("Exit Tags.get_CoverArts");
                return coverArts;
            }
        }

        public int Count
        {
            get
            {
                try
                {
                    return TagsLibraryPInvoke.TagsBase_Count(_ptr);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }
            }
        }

        public int CoverArtCount
        {
            get
            {
                try
                {
                    return TagsLibraryPInvoke.TagsBase_CoverArtCount(_ptr);
                }
                catch (Exception ex)
                {
                    _logger.Error<Exception>(ex);
                    throw;
                }
            }
        }

        public long Size
        {
            get
            {
                return TagsLibraryPInvoke.TagsBase_get_Size(_ptr);
            }
        }

        public string this[string name]
        {
            get
            {
                return Marshal.PtrToStringUni(TagsLibraryPInvoke.TagsBase_GetTag(_ptr, name));
            }
            set
            {
                if (Exists(name) != -1)
                {
                    int result = TagsLibraryPInvoke.TagsBase_SetTag(_ptr, name, value);
                    // TagsUtilities.CheckError(result);
                }
                else
                {
                    Tag tag = Add(name);
                    tag.Value = value;
                }
            }
        }

        internal IntPtr Ptr
        {
            get
            {
                return _ptr;
            }
        }

        public virtual string TypeString
        {
            get
            {
                return TypeConversionService.ConvertFromNativeString(TagsLibraryPInvoke.TagsBase_GetTypeString(_ptr));
            }
        }
        #region Tags Properties
        [TagName(Category = TagCategories.System)]
        public TimeSpan Duration
        {
            get { return _length; }
            private set { _length = value; }
        }

        [TagName(Category = TagCategories.System)]
        public int Bitrate
        {
            get { return _bitrate; }
            private set { _bitrate = value; }
        }

        [TagName(Category = TagCategories.System)]
        public byte Channels
        {
            get { return _channels; }
            private set { _channels = value; }
        }

        [TagName(Category = TagCategories.System)]
        public int Frequency
        {
            get { return _frequency; }
            private set { _frequency = value; }
        }

        [TagName(Category = TagCategories.Track, SystemName = "ARTIST")]
        [TagName(Category = TagCategories.Album, SystemName = "ARTIST")]
        public string Artist
        {
            get
            {
                return this["ARTIST"];
            }
            set
            {
                this["ARTIST"] = value;
            }
        }

        [TagName(Category = TagCategories.Album, SystemName = "ALBUMARTIST")]
        public string AlbumArtist
        {
            get
            {
                return this["ALBUMARTIST"];
            }
            set
            {
                this["ALBUMARTIST"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "TITLE")]
        public string Title
        {
            get
            {
                return this["TITLE"];
            }
            set
            {
                this["TITLE"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "COMPOSER")]
        public string Composer
        {
            get
            {
                return this["COMPOSER"];
            }
            set
            {
                this["COMPOSER"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "SUBTITLE")]
        public string Subtitle
        {
            get
            {
                return this["SUBTITLE"];
            }
            set
            {
                this["SUBTITLE"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "MIXARTIST")]
        public string MixArtist
        {
            get
            {
                return this["MIXARTIST"];
            }
            set
            {
                this["MIXARTIST"] = value;
            }
        }

        [TagName(Category = TagCategories.Album, SystemName = "ALBUM")]
        public string Album
        {
            get
            {
                return this["ALBUM"];
            }
            set
            {
                this["ALBUM"] = value;
            }
        }

        [TagName(SystemName = "COPYRIGHT")]
        public string Copyright
        {
            get
            {
                return this["COPYRIGHT"];
            }
            set
            {
                this["COPYRIGHT"] = value;
            }
        }

        [TagName(SystemName = "GROUPING")]
        public string Grouping
        {
            get
            {
                return this["GROUPING"];
            }
            set
            {
                this["GROUPING"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "GENRE")]
        [TagName(Category = TagCategories.Album, SystemName = "GENRE")]
        public string Genre
        {
            get
            {
                return this["GENRE"];
            }
            set
            {
                this["GENRE"] = value;
            }
        }

        [TagName(SystemName = "RELEASEDATE")]
        public string ReleaseDate
        {
            get
            {
                return this["RELEASEDATE"];
            }
            set
            {
                this["RELEASEDATE"] = value;
            }
        }

        [TagName(SystemName = "RECORDINGDATE")]
        public string RecordingDate
        {
            get
            {
                return this["RECORDINGDATE"];
            }
            set
            {
                this["RECORDINGDATE"] = value;
            }
        }

        [TagName(Category = TagCategories.Album, SystemName = "YEAR")]
        [TagName(Category = TagCategories.Track, SystemName = "YEAR")]
        public string Year
        {
            get
            {
                return this["YEAR"];
            }
            set
            {
                this["YEAR"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "TRACKNUMBER")]
        public string TrackNumber
        {
            get
            {
                return this["TRACKNUMBER"];
            }
            set
            {
                this["TRACKNUMBER"] = value;
            }
        }

        [TagName(SystemName = "DISCNUMBER")]
        public string DiscNumber
        {
            get
            {
                return this["DISCNUMBER"];
            }
            set
            {
                this["DISCNUMBER"] = value;
            }
        }

        [TagName(SystemName = "BPM")]
        public string Bpm
        {
            get
            {
                return this["BPM"];
            }
            set
            {
                this["BPM"] = value;
            }
        }

        [TagName(SystemName = "KEY")]
        public string Key
        {
            get
            {
                return this["KEY"];
            }
            set
            {
                this["KEY"] = value;
            }
        }

        [TagName(SystemName = "CONDUCTOR")]
        public string Conductor
        {
            get
            {
                return this["CONDUCTOR"];
            }
            set
            {
                this["CONDUCTOR"] = value;
            }
        }

        [TagName(SystemName = "LYRICIST")]
        public string Lyricist
        {
            get
            {
                return this["LYRICIST"];
            }
            set
            {
                this["LYRICIST"] = value;
            }
        }

        [TagName(SystemName = "PRODUCED")]
        public string Produced
        {
            get
            {
                return this["PRODUCED"];
            }
            set
            {
                this["PRODUCED"] = value;
            }
        }

        [TagName(SystemName = "STUDIO")]
        public string RecordLocation
        {
            get
            {
                return this["STUDIO"];
            }
            set
            {
                this["STUDIO"] = value;
            }
        }

        [TagName(SystemName = "PUBLISHER")]
        public string Label
        {
            get
            {
                return this["PUBLISHER"];
            }
            set
            {
                this["PUBLISHER"] = value;
            }
        }

        [TagName(SystemName = "ENCODEDBY")]
        public string EncodeBy
        {
            get
            {
                return this["ENCODEDBY"];
            }
            set
            {
                this["ENCODEDBY"] = value;
            }
        }

        [TagName(SystemName = "SOFTWARESETTINGS")]
        public string SoftwareSettings
        {
            get
            {
                return this["SOFTWARESETTINGS"];
            }
            set
            {
                this["SOFTWARESETTINGS"] = value;
            }
        }

        [TagName(SystemName = "ISRC")]
        public string ISRC
        {
            get
            {
                return this["ISRC"];
            }
            set
            {
                this["ISRC"] = value;
            }
        }

        [TagName(SystemName = "DISCSUBTITLE")]
        public string DiscSubtitle
        {
            get
            {
                return this["DISCSUBTITLE"];
            }
            set
            {
                this["DISCSUBTITLE"] = value;
            }
        }

        [TagName(SystemName = "LANGUAGE")]
        public string Language
        {
            get
            {
                return this["LANGUAGE"];
            }
            set
            {
                this["LANGUAGE"] = value;
            }
        }

        [TagName(SystemName = "MEDIA")]
        public string Media
        {
            get
            {
                return this["MEDIA"];
            }
            set
            {
                this["MEDIA"] = value;
            }
        }

        [TagName(SystemName = "MOOD")]
        public string Mood
        {
            get
            {
                return this["MOOD"];
            }
            set
            {
                this["MOOD"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "COMMENT")]
        public string Comment
        {
            get
            {
                return this["COMMENT"];
            }
            set
            {
                this["COMMENT"] = value;
            }
        }

        [TagName(Category = TagCategories.Track, SystemName = "LYRICS")]
        public string Lyrics
        {
            get
            {
                return this["LYRICS"];
            }
            set
            {
                this["LYRICS"] = value;
            }
        }

        [TagName(SystemName = "ARTISTSORT")]
        public string ArtistSort
        {
            get
            {
                return this["ARTISTSORT"];
            }
            set
            {
                this["ARTISTSORT"] = value;
            }
        }

        [TagName(SystemName = "TITLESORT")]
        public string TitleSort
        {
            get
            {
                return this["TITLESORT"];
            }
            set
            {
                this["TITLESORT"] = value;
            }
        }

        [TagName(SystemName = "ALBUMARTISTSORT")]
        public string AlbumArtistSort
        {
            get
            {
                return this["ALBUMARTISTSORT"];
            }
            set
            {
                this["ALBUMARTISTSORT"] = value;
            }
        }

        [TagName(SystemName = "ALBUMSORT")]
        public string AlbumSort
        {
            get
            {
                return this["ALBUMSORT"];
            }
            set
            {
                this["ALBUMSORT"] = value;
            }
        }

        [TagName(SystemName = "ORIGINALARTIST")]
        public string OriginalArtist
        {
            get
            {
                return this["ORIGINALARTIST"];
            }
            set
            {
                this["ORIGINALARTIST"] = value;
            }
        }

        [TagName(SystemName = "ORIGINALTITLE")]
        public string OriginalTitle
        {
            get
            {
                return this["ORIGINALTITLE"];
            }
            set
            {
                this["ORIGINALTITLE"] = value;
            }
        }

        [TagName(SystemName = "ORIGINALLYRICIST")]
        public string OriginalLyricist
        {
            get
            {
                return this["ORIGINALLYRICIST"];
            }
            set
            {
                this["ORIGINALLYRICIST"] = value;
            }
        }

        [TagName(SystemName = "ORIGINALFILENAME")]
        public string OriginalFileName
        {
            get
            {
                return this["ORIGINALFILENAME"];
            }
            set
            {
                this["ORIGINALFILENAME"] = value;
            }
        }

        [TagName(SystemName = "ORIGINALRELEASEDATE")]
        public string OriginalReleaseDate
        {
            get
            {
                return this["ORIGINALRELEASEDATE"];
            }
            set
            {
                this["ORIGINALRELEASEDATE"] = value;
            }
        }

        [TagName(SystemName = "ARTISTURL")]
        public string ArtistUrl
        {
            get
            {
                return this["ARTISTURL"];
            }
            set
            {
                this["ARTISTURL"] = value;
            }
        }

        [TagName(SystemName = "AUDIOFILEURL")]
        public string AudiofileUrl
        {
            get
            {
                return this["AUDIOFILEURL"];
            }
            set
            {
                this["AUDIOFILEURL"] = value;
            }
        }

        [TagName(SystemName = "BUYCDURL")]
        public string BuyCDUrl
        {
            get
            {
                return this["BUYCDURL"];
            }
            set
            {
                this["BUYCDURL"] = value;
            }
        }

        [TagName(SystemName = "PUBLISHERURL")]
        public string PublisherUrl
        {
            get
            {
                return this["PUBLISHERURL"];
            }
            set
            {
                this["PUBLISHERURL"] = value;
            }
        }

        [TagName(SystemName = "RADIOSTATIONURL")]
        public string RadioStationUrl
        {
            get
            {
                return this["RADIOSTATIONURL"];
            }
            set
            {
                this["RADIOSTATIONURL"] = value;
            }
        }

        [TagName(SystemName = "COPYRIGHTURL")]
        public string CopyrightUrl
        {
            get
            {
                return this["COPYRIGHTURL"];
            }
            set
            {
                this["COPYRIGHTURL"] = value;
            }
        }

        [TagName(SystemName = "OFFICIALAUDIOSOURCEURL")]
        public string OfficialAudioSourceUrl
        {
            get
            {
                return this["OFFICIALAUDIOSOURCEURL"];
            }
            set
            {
                this["OFFICIALAUDIOSOURCEURL"] = value;
            }
        }

        [TagName(SystemName = "PAYMENTURL")]
        public string PaymentUrl
        {
            get
            {
                return this["PAYMENTURL"];
            }
            set
            {
                this["PAYMENTURL"] = value;
            }
        }

        [TagName(SystemName = "FILEOWNER")]
        public string FileOwner
        {
            get
            {
                return this["FILEOWNER"];
            }
            set
            {
                this["FILEOWNER"] = value;
            }
        }

        [TagName(SystemName = "RADIOSTATIONNAME")]
        public string RadioStationName
        {
            get
            {
                return this["RADIOSTATIONNAME"];
            }
            set
            {
                this["RADIOSTATIONNAME"] = value;
            }
        }

        [TagName(SystemName = "RADIOSTATIONOWNER")]
        public string RadioStationOwner
        {
            get
            {
                return this["RADIOSTATIONOWNER"];
            }
            set
            {
                this["RADIOSTATIONOWNER"] = value;
            }
        }

        [TagName(SystemName = "GAPLESSPLAYBACK")]
        public string GaplessPlayback
        {
            get
            {
                return this["GAPLESSPLAYBACK"];
            }
            set
            {
                this["GAPLESSPLAYBACK"] = value;
            }
        }

        [TagName(SystemName = "PARTOFCOMPILATION")]
        public string PartOfCompilation
        {
            get
            {
                return this["PARTOFCOMPILATION"];
            }
            set
            {
                this["PARTOFCOMPILATION"] = value;
            }
        }

        [TagName(SystemName = "RATING")]
        public string Rating
        {
            get
            {
                return this["RATING"];
            }
            set
            {
                this["RATING"] = value;
            }
        }

        [TagName(SystemName = "TVNETWORK")]
        public string TVNetwork
        {
            get
            {
                return this["TVNETWORK"];
            }
            set
            {
                this["TVNETWORK"] = value;
            }
        }

        [TagName(SystemName = "SHOW")]
        public string Show
        {
            get
            {
                return this["SHOW"];
            }
            set
            {
                this["SHOW"] = value;
            }
        }

        [TagName(SystemName = "EPISODE")]
        public string Episode
        {
            get
            {
                return this["EPISODE"];
            }
            set
            {
                this["EPISODE"] = value;
            }
        }

        [TagName(SystemName = "SEASONNUMBER")]
        public string SeasonNumber
        {
            get
            {
                return this["SEASONNUMBER"];
            }
            set
            {
                this["SEASONNUMBER"] = value;
            }
        }

        [TagName(SystemName = "EPISODENUMBER")]
        public string EpisodeNumber
        {
            get
            {
                return this["EPISODENUMBER"];
            }
            set
            {
                this["EPISODENUMBER"] = value;
            }
        }

        [TagName(SystemName = "HDVIDEO")]
        public string HDVideo
        {
            get
            {
                return this["HDVIDEO"];
            }
            set
            {
                this["HDVIDEO"] = value;
            }
        }

        [TagName(SystemName = "DESCRIPTION")]
        public string Description
        {
            get
            {
                return this["DESCRIPTION"];
            }
            set
            {
                this["DESCRIPTION"] = value;
            }
        }

        [TagName(SystemName = "LONGDESCRIPTION")]
        public string LongDescription
        {
            get
            {
                return this["LONGDESCRIPTION"];
            }
            set
            {
                this["LONGDESCRIPTION"] = value;
            }
        }

        [TagName(SystemName = "ENCODERTOOL")]
        public string EncoderTool
        {
            get
            {
                return this["ENCODERTOOL"];
            }
            set
            {
                this["ENCODERTOOL"] = value;
            }
        }

        [TagName(SystemName = "SHOWSORT")]
        public string ShowSort
        {
            get
            {
                return this["SHOWSORT"];
            }
            set
            {
                this["SHOWSORT"] = value;
            }
        }

        [TagName(SystemName = "PODCAST")]
        public string Podcast
        {
            get
            {
                return this["PODCAST"];
            }
            set
            {
                this["PODCAST"] = value;
            }
        }

        [TagName(SystemName = "PODCASTURL")]
        public string PodcastUrl
        {
            get
            {
                return this["PODCASTURL"];
            }
            set
            {
                this["PODCASTURL"] = value;
            }
        }

        [TagName(SystemName = "PODCASTKEYWORDS")]
        public string PodcastKeyWords
        {
            get
            {
                return this["PODCASTKEYWORDS"];
            }
            set
            {
                this["PODCASTKEYWORDS"] = value;
            }
        }

        [TagName(SystemName = "PURCHASEACCOUNT")]
        public string PurchaseAccount
        {
            get
            {
                return this["PURCHASEACCOUNT"];
            }
            set
            {
                this["PURCHASEACCOUNT"] = value;
            }
        }

        [TagName(SystemName = "PURCHASEACCOUNTTYPE")]
        public string PurchaseAccountType
        {
            get
            {
                return this["PURCHASEACCOUNTTYPE"];
            }
            set
            {
                this["PURCHASEACCOUNTTYPE"] = value;
            }
        }

        [TagName(SystemName = "PURCHASEDATE")]
        public string PurchaseDate
        {
            get
            {
                return this["PURCHASEDATE"];
            }
            set
            {
                this["PURCHASEDATE"] = value;
            }
        }

        [TagName(SystemName = "PLAYCOUNT")]
        public string PlayCount
        {
            get
            {
                return this["PLAYCOUNT"];
            }
            set
            {
                this["PLAYCOUNT"] = value;
            }
        }

        [TagName(SystemName = "URL")]
        public string Url
        {
            get
            {
                return this["URL"];
            }
            set
            {
                this["URL"] = value;
            }
        }

        [TagName(SystemName = "TEMPO")]
        public string Tempo
        {
            get
            {
                return this["TEMPO"];
            }
            set
            {
                this["TEMPO"] = value;
            }
        }

        [TagName(SystemName = "KEYWORDS")]
        public string KeyWords
        {
            get
            {
                return this["KEYWORDS"];
            }
            set
            {
                this["KEYWORDS"] = value;
            }
        }

        [TagName(SystemName = "MUSICEND")]
        public string Musicend
        {
            get
            {
                return this["MUSICEND"];
            }
            set
            {
                this["MUSICEND"] = value;
            }
        }

        [TagName(SystemName = "MUSICENERGY")]
        public string MusicEnergy
        {
            get
            {
                return this["MUSICENERGY"];
            }
            set
            {
                this["MUSICENERGY"] = value;
            }
        }

        [TagName(SystemName = "MUSICTEXTURE")]
        public string MusicTexture
        {
            get
            {
                return this["MUSICTEXTURE"];
            }
            set
            {
                this["MUSICTEXTURE"] = value;
            }
        }

        [TagName(SystemName = "HOOKSTART")]
        public string HookStart
        {
            get
            {
                return this["HOOKSTART"];
            }
            set
            {
                this["HOOKSTART"] = value;
            }
        }

        [TagName(SystemName = "HOOKEND")]
        public string HookEnd
        {
            get
            {
                return this["HOOKEND"];
            }
            set
            {
                this["HOOKEND"] = value;
            }
        }

        [TagName(SystemName = "SUBJECT")]
        public string Subject
        {
            get
            {
                return this["SUBJECT"];
            }
            set
            {
                this["SUBJECT"] = value;
            }
        }

        [TagName(SystemName = "ARCHIVAL LOCATION")]
        public string ArchivalLocation
        {
            get
            {
                return this["ARCHIVAL LOCATION"];
            }
            set
            {
                this["ARCHIVAL LOCATION"] = value;
            }
        }

        [TagName(SystemName = "DISC")]
        public string Disc
        {
            get
            {
                return this["DISC"];
            }
            set
            {
                this["DISC"] = value;
            }
        }

        [TagName(SystemName = "CATALOGNUMBER")]
        public string CatalogNumber
        {
            get
            {
                return this["CATALOGNUMBER"];
            }
            set
            {
                this["CATALOGNUMBER"] = value;
            }
        }

        [TagName(SystemName = "PARTOFSERIES")]
        public string PartOfSeries
        {
            get
            {
                return this["PARTOFSERIES"];
            }
            set
            {
                this["PARTOFSERIES"] = value;
            }
        }

        [TagName(SystemName = "ARRANGER")]
        public string Arranger
        {
            get
            {
                return this["ARRANGER"];
            }
            set
            {
                this["ARRANGER"] = value;
            }
        }

        [TagName(SystemName = "ENGINEER")]
        public string Engineer
        {
            get
            {
                return this["ENGINEER"];
            }
            set
            {
                this["ENGINEER"] = value;
            }
        }

        [TagName(SystemName = "DJMIXER")]
        public string DjMixer
        {
            get
            {
                return this["DJMIXER"];
            }
            set
            {
                this["DJMIXER"] = value;
            }
        }

        [TagName(SystemName = "MIXER")]
        public string Mixer
        {
            get
            {
                return this["MIXER"];
            }
            set
            {
                this["MIXER"] = value;
            }
        }

        [TagName(SystemName = "POPULARITY")]
        public string Popularity
        {
            get
            {
                return this["POPULARITY"];
            }
            set
            {
                this["POPULARITY"] = value;
            }
        }

        [TagName(SystemName = "QUALITY")]
        public string Quality
        {
            get
            {
                return this["QUALITY"];
            }
            set
            {
                this["QUALITY"] = value;
            }
        }

        [TagName(SystemName = "SITUATION")]
        public string Situation
        {
            get
            {
                return this["SITUATION"];
            }
            set
            {
                this["SITUATION"] = value;
            }
        }

        [TagName(SystemName = "PREFERENCE")]
        public string Preference
        {
            get
            {
                return this["PREFERENCE"];
            }
            set
            {
                this["PREFERENCE"] = value;
            }
        }
        #endregion
        #endregion

        #region Constructor & Destructor
        /// <summary>
        /// TODO: Probably it should be removed at all.
        /// </summary>
        internal TagsBase()
            : this(TagsLibraryPInvoke.TagsBase_Create(), true)
        {
        }

        internal TagsBase(IntPtr ptr)
            : this(ptr, false)
        {
        }

        internal TagsBase(IntPtr ptr, bool memoryOwn)
        {
            _ptr = ptr;
            _memoryOwn = memoryOwn;
            _logger.Trace("Created Tags instance with existing handler 0x{0:x} ({0})", _ptr);
            //_logger.Trace(Environment.StackTrace);
        }

        ~TagsBase()
        {
            Dispose();
        }
        #endregion

        #region Methods
        public static List<TagInfo> GetTagNames()
        {
            Type tagsType = typeof(TagsBase);

            var properties = tagsType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.GetCustomAttributes(typeof(TagNameAttribute), false).Count() > 0);

            List<TagInfo> result = new List<TagInfo>(properties.Count());

            foreach (var property in properties)
            {
                var attributes = property.GetCustomAttributes(typeof(TagNameAttribute), false).Cast<TagNameAttribute>();

                if (attributes == null)
                {
                    continue;
                }

                foreach (var attribute in attributes)
                {
                    TagInfo info = new TagInfo();
                    info.ProperyName = property.Name;
                    info.FriendlyName = attribute.FriendlyName;
                    info.Description = attribute.Description;
                    info.Category = attribute.Category;
                    info.SystemName = attribute.SystemName ?? string.Empty;

                    if (string.IsNullOrEmpty(info.Description))
                    {
                        info.Description = property.Name;
                    }

                    if (string.IsNullOrEmpty(info.FriendlyName))
                    {
                        info.FriendlyName = property.Name;
                    }

                    if (string.IsNullOrEmpty(info.Category))
                    {
                        info.Category = "Common";
                    }

                    result.Add(info);
                }
            }

            return result;
        }

        public virtual Tag Add(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            IntPtr ptr = TagsLibraryPInvoke.TagsBase_Add(_ptr, name);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new Tag(ptr);
        }

        public virtual CoverArt AddCoverArt(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            IntPtr ptr = TagsLibraryPInvoke.TagsBase_AddCoverArt(_ptr, name);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new CoverArt(ptr);
        }

        public virtual CoverArt AddCoverArt(string name, byte[] data, string MIMEType)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (data == null || data.Length == 0)
            {
                throw new ArgumentNullException("data");
            }

            IntPtr ptr = TagsLibraryPInvoke.TagsBase_AddCoverArt2(_ptr, name, ref data, data.Length, MIMEType);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new CoverArt(ptr);
        }

        public virtual CoverArt AddCoverArt(string name, string fileName)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            IntPtr ptr = TagsLibraryPInvoke.TagsBase_AddCoverArt3(_ptr, name, fileName);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new CoverArt(ptr);
        }

        public virtual bool Assign(TagsBase source)
        {
            return TagsLibraryPInvoke.TagsBase_Assign(_ptr, source._ptr);
        }

        public virtual void Clear()
        {
            TagsLibraryPInvoke.TagsBase_Clear(_ptr);
        }

        public virtual bool Delete(int index)
        {
            return TagsLibraryPInvoke.TagsBase_Delete(_ptr, index);
        }

        public bool Delete(string name)
        {
            return TagsLibraryPInvoke.TagsBase_Delete2(_ptr, name);
        }

        public void DeleteAllCoverArts()
        {
            TagsLibraryPInvoke.TagsBase_DeleteAllCoverArts(_ptr);
        }

        public void DeleteAllTags()
        {
            TagsLibraryPInvoke.TagsBase_DeleteAllTags(_ptr);
        }

        public bool DeleteCoverArt(int index)
        {
            return TagsLibraryPInvoke.TagsBase_DeleteCoverArt(_ptr, index);
        }

        /// <summary>
        /// TODO: Why TagsBase_Destroy is failing?
        /// </summary>
        public virtual void Dispose()
        {
            if (!_disposed)
            {
                lock (this)
                {
                    _logger.Trace("Try to dispose Tags instance with handler 0x{0:x} ({0})", _ptr);
                    //_logger.Trace(Environment.StackTrace);
                    _disposed = true;

                    if (_memoryOwn)
                    {
                        //TagsBase_Destroy(_ptr);
                        _ptr = IntPtr.Zero;
                    }

                    GC.SuppressFinalize(this);
                }
            }
        }

        public int Exists(string name)
        {
            return TagsLibraryPInvoke.TagsBase_Exists(_ptr, name);
        }

        public CoverArt GetCoverArt(string name)
        {
            IntPtr ptr = TagsLibraryPInvoke.TagsBase_CoverArt(_ptr, name);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new CoverArt(ptr);
        }

        public Tag Tag(string name)
        {
            IntPtr ptr = TagsLibraryPInvoke.TagsBase_Tag(_ptr, name);

            if (ptr == IntPtr.Zero)
            {
                return null;
            }

            return new Tag(ptr);
        }

        public virtual void LoadFromBASS(int channel)
        {
            TagsLibraryPInvoke.TagsBase_LoadFromBASS(_ptr, channel);
        }

        private void LoadFileInfo(int channel)
        {
            // length in bytes 
            long len = Bass.BASS_ChannelGetLength(channel, BASSMode.BASS_POS_BYTES);
            // the time length 
            double time = Bass.BASS_ChannelBytes2Seconds(channel, len);
            Duration = TimeSpan.FromSeconds(time);

            len = Bass.BASS_StreamGetFilePosition(channel, BASSStreamFilePosition.BASS_FILEPOS_END); // file length
            Bitrate = (int)(len / (125 * time) + 0.5); // bitrate (Kbps)

            var info = Bass.BASS_ChannelGetInfo(channel);
            Channels = (byte)info.chans;
            Frequency = info.freq;
        }

        public void LoadFromBASS(string fileName)
        {
            _logger.Trace("Enter Tags.LoadFromBASS(\"{0}\"). Handler 0x{1:x} ({1})", fileName, (ulong)_ptr);

            if (string.IsNullOrEmpty(fileName))
            {
                _logger.Error("Tags.LoadFromFile: 'fileName' is null or empty");
                throw new ArgumentNullException("fileName");
            }

            int stream = Bass.BASS_StreamCreateFile(fileName, 0, 0, BASSFlag.BASS_DEFAULT | BASSFlag.BASS_UNICODE);

            if (stream == 0)
            {
                _logger.Error("Could not open file \"{0}\"", fileName);
                throw new InvalidOperationException(string.Format("Could not open file \"{0}\"", fileName));
            }

            LoadFromBASS(stream);

            Bass.BASS_StreamFree(stream);
        }

        public virtual void LoadFromFile(string fileName)
        {
            TagsLibraryPInvoke.TagsBase_LoadFromFile(_ptr, fileName);
        }

        public TagPictureFormat PicturePointerType(IntPtr picturePointer)
        {
            return TagsLibraryPInvoke.TagsBase_PicturePointerType(_ptr, picturePointer);
        }

        public TagPictureFormat PictureStreamType(byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            return TagsLibraryPInvoke.TagsBase_PictureStreamType(_ptr, data, data.Length);
        }

        public bool Remove(int index)
        {
            return TagsLibraryPInvoke.TagsBase_Remove(_ptr, index);
        }

        public void RemoveEmptyTags()
        {
            TagsLibraryPInvoke.TagsBase_RemoveEmptyTags(_ptr);
        }

        public void SaveToFile()
        {
            TagsLibraryPInvoke.TagsBase_SaveToFile1(_ptr);
        }

        public virtual void SaveToFile(string fileName)
        {
            TagsLibraryPInvoke.TagsBase_SaveToFile2(_ptr, fileName);
        }

        public int TypeCount(string name)
        {
            return TagsLibraryPInvoke.TagsBase_TypeCount(_ptr, name);
        }

        public override string ToString()
        {
            return string.Format("{0}. {1} - {2} ({3}) [{4}]", TrackNumber, Artist, Title, Album, string.Empty);
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            TagsBase other = obj as TagsBase;

            if (other == null)
            {
                return false;
            }

            return this == other;
        }

        public static bool operator ==(TagsBase a, TagsBase b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            if (((Object)a == null) || ((Object)b == null))
            {
                return false;
            }

            IntPtr aRef = a.Ptr;

            IntPtr bRef = b.Ptr;

            return aRef == bRef;
        }

        public static bool operator !=(TagsBase a, TagsBase b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return _ptr.ToInt64().GetHashCode();
        }	
        #endregion
    }
}
