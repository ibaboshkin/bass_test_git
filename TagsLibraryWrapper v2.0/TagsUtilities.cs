﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace TagUtilities.TagsLibrary
{
    public static class TagsUtilities
    {
        #region External
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern IntPtr TagsLibraryErrorCode2String2(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v1TagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int ID3v2TagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int FlacTagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int MP4TagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int APEv2TagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int OpusVorbisTagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int WAVTagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        public static extern int WMATagLibraryErrorCodeToTagsLibraryErrorCode(int ErrorCode);
        [DllImport("TagsLib.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        private static extern uint TagsLibraryVersion();
        #endregion

        private static byte GetHexVersion(byte bt)
        {
            return (byte)(bt / 16 * 10 + bt % 16);
        }

        public static Version TagsLibVersion
        {
            get
            {
                uint version = TagsLibraryVersion();
                byte[] bytes = BitConverter.GetBytes(version);

                return new Version(
                    GetHexVersion(bytes[3]),
                    GetHexVersion(bytes[2]),
                    GetHexVersion(bytes[1]),
                    GetHexVersion(bytes[0])
                    );
            }
        }

        #region Methods
        public static void Initialize()
        {
            Assembly Assem = Assembly.GetExecutingAssembly();

            foreach (Type type in Assem.GetTypes())
            {
                MethodInfo methodInfo = type.GetMethod("registerTypeConversion");

                if (methodInfo != null)
                {
                    methodInfo.Invoke(null, null);
                }
            }
        }

        internal static string GetErrorString(int errorCode)
        {
            return Marshal.PtrToStringUni(TagsLibraryErrorCode2String2(errorCode));
        }

        internal static void CheckError(int resultCode)
        {
            if (resultCode != 0)
            {
                throw new InvalidOperationException(GetErrorString(resultCode));
            }
        }

        internal static void CheckError(int resultCode, Func<int, int> errorCodeConverter)
        {
            if (resultCode != 0)
            {
                if (errorCodeConverter == null)
                {
                    throw new ArgumentNullException("errorFunction");
                }

                throw new InvalidOperationException(GetErrorString(errorCodeConverter(resultCode)));
            }
        }
        #endregion
    }
}
