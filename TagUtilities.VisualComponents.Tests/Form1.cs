﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TagUtilities.TagsLibrary;
using TagUtilities.VisualComponents.Models;
using TupleS3 = System.Tuple<string, string, string>;

namespace TagUtilities.VisualComponents.Tests
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            valueSelector1.Fill(MapTagInfo(Tags.GetTagNames()));
        }

        private IEnumerable<DescriptedValue> MapTagInfo(IEnumerable<TagInfo> info)
        {
            List<DescriptedValue> result = new List<DescriptedValue>(info.Count());

            foreach (var item in info)
            {
                result.Add(new DescriptedValue("%" + item.ProperyName + "%", item.Category, item.Description));
            }

            return result;
        }

        private void valueSelector1_InsertMacro(object sender, Models.InsertMacroEventArgs e)
        {
            MessageBox.Show(e.Macro);
        }
    }
}
